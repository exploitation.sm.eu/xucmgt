package form

import play.api.data.format.Formats
import play.api.data.format.Formatter
import play.api.data.FormError

object MoreFormats {

    def myBigDecimalFormat(precision: Option[(Int, Int)]): Formatter[BigDecimal] = new Formatter[BigDecimal] {

        def bind(key: String, data: Map[String, String]) = {
          Formats.bigDecimalFormat(precision).bind(key, Map(key -> data(key).replaceAll(" ","")))
        }

        def unbind(key: String, value: BigDecimal) = Formats.bigDecimalFormat.unbind(key,value)
  }

}