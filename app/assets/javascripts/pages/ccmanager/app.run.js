(function(){
    'use strict';

    angular
    .module('ccManager')
    .run(run);

    run.$inject =['Preferences','$templateCache'];

    function run (preferences, $templateCache){
        preferences.init();
        $templateCache.put('ng-table/filters/dropdown-multi-select.html',
        '<div ng-dropdown-multiselect="" options="$column.data" selected-model="params.filter()[name]" translation-texts="mstranslation"></div>');
    }
})();
var globalSearch={};
var supervisionUsername = "";
var supervisionPassword = "";

var _initCti = function(username, password, hostAndPort, protocol) {
    supervisionUsername = username;
    supervisionPassword = password;
    var phoneNumber = 0;
    console.log("sign in " + supervisionUsername + " " + password);
    var wsurl = protocol + hostAndPort + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
                            phoneNumber + "&amp;password=" + encodeURIComponent(password);
    Cti.debugMsg = true;
    Cti.WebSocket.init(wsurl, supervisionUsername, 0);

};