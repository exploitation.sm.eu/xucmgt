
(function(){
    'use strict';
    angular.module('ccManager',[
        'xuc.services',
        'ngCookies',
        'ui.bootstrap',
        'LocalStorageModule',
        'ngTable',
        'pascalprecht.translate',
        'angularjs-dropdown-multiselect'
     ]);
})();