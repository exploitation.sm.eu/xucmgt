(function() {
    'use strict';

    angular.module('ccManager').config(config);

    config.$inject = [ 'localStorageServiceProvider', '$logProvider', '$translateProvider','$httpProvider' ];

    function config(localStorageServiceProvider, $logProvider, $translateProvider,$httpProvider ) {
    /*jshint -W087*/
        //debugger;
        localStorageServiceProvider.setPrefix('ccManager.default');
        localStorageServiceProvider.setNotify(true, true);
        $logProvider.debugEnabled(false);

        $translateProvider.translations('fr', {
            COL_AGENT_NUMBER : 'No',
            COL_AGENT_FIRSTNAME : 'Prénom',
            COL_AGENT_LASTNAME : 'Nom',
            COL_AGENT_LOGINDATETIME : 'Login',
            COL_AGENT_LOGOUTDATETIME : 'Logout',
            COL_AGENT_STATE : 'Etat',
            COL_AGENT_STATENAME : 'Etat',
            COL_AGENT_STATUS : 'Prés.',
            COL_AGENT_TIMEINSTATE : 'Depuis',
            COL_AGENT_PHONENB : 'Tél',
            COL_AGENT_PAUSEDTIME : 'Tot. Pause',
            COL_AGENT_WRAPUPTIME : 'Wrapup',
            COL_AGENT_INBCALLS : 'App. Prés.',
            COL_AGENT_INBANSCALLS : 'App. Rép.',
            COL_AGENT_INBCALLTIME : 'Total Com. Entr.',
            COL_AGENT_INBAVERCALLTIME : 'Entr. DMC',
            COL_AGENT_INBUNANSCALLS : 'Non Rép.',
            COL_AGENT_INBPERCUNANSCALLS : '% Non Rép.',
            COL_AGENT_OUTCALLS : 'App. Sortants',
            COL_AGENT_OUTCALLTIME : 'Total Com. Sort.',
            ACTION_LOGIN : 'Logguer',
            ACTION_LOGOUT : 'Delogguer',
            ACTION_PAUSE : 'Pause',
            ACTION_UNPAUSE : 'Annuler Pause',
            ACTION_LISTEN : 'Ecouter',
            SELECT_COLUMN : 'Sélectionner les colonnes à afficher',
            COL_NUMBER : 'No',
            COL_DISPLAYNAME : 'Nom',
            COL_WAITINGCALLS : 'Att.',
            COL_LONGESTWAITTIME : 'Tps Att.',
            COL_EWT : 'TAE',
            COL_AVAILABLEAGENTS : 'Disp.',
            COL_TALKINGAGENTS : 'Conv.',
            COL_TOTALNUMBERCALLSENTERED : 'Total',
            COL_TOTALNUMBERCALLSANSWERED : 'Rép.',
            COL_PERCENTAGEANSWEREDBEFORE15 : '%Rép. 15s',
            COL_TOTALNUMBERCALLSABANDONNED : 'Aband.',
            COL_PERCENTAGEABANDONNEDAFTER15 : '%Aband. 15s',
            COL_TOTALNUMBERCALLSCLOSED : 'Fermé',
            COL_TOTALNUMBERCALLSTIMEOUT : 'Diss.',
            MS_CHECKALL : 'Tout Selectionner',
            MS_UNCHECKALL : 'Tout Deselect.',
            MS_SELECTIONCOUNT : 'Select',
            MS_SELECTIONOF : '/',
            MS_SEARCHPLACEHOLDER : 'Chercher...',
            MS_BUTTONDEFAULTTEXT : 'Choisir',
            MS_DYNAMICBUTTONTEXTSUFFIX : 'selection.'
        });
        $translateProvider.translations('en', {
            COL_AGENT_NUMBER : 'Nb',
            COL_AGENT_FIRSTNAME : 'First Name',
            COL_AGENT_LASTNAME : 'Last Name',
            COL_AGENT_LOGINDATETIME : 'Login',
            COL_AGENT_LOGOUTDATETIME : 'Logout',
            COL_AGENT_STATE : 'State',
            COL_AGENT_STATENAME : 'State',
            COL_AGENT_STATUS : 'Status.',
            COL_AGENT_TIMEINSTATE : 'Since',
            COL_AGENT_PHONENB : 'Phone',
            COL_AGENT_PAUSEDTIME : 'Tot. Pause',
            COL_AGENT_WRAPUPTIME : 'Wrapup',
            COL_AGENT_INBCALLS : 'Inb. Calls',
            COL_AGENT_INBANSCALLS : 'Inb. Answ.',
            COL_AGENT_INBCALLTIME : 'Inb. Total Com.',
            COL_AGENT_INBAVERCALLTIME : 'Inb. Moy. Com.',
            COL_AGENT_INBUNANSCALLS : 'Inb. Unansw.',
            COL_AGENT_INBPERCUNANSCALLS : 'Inb % Unansw.',
            COL_AGENT_OUTCALLS : 'Out. Calls',
            COL_AGENT_OUTCALLTIME : 'Out. Total Com.',
            ACTION_LOGIN : 'Login',
            ACTION_LOGOUT : 'Logout',
            ACTION_PAUSE : 'Pause',
            ACTION_UNPAUSE : 'Un Pause',
            ACTION_LISTEN : 'Listen',
            SELECT_COLUMN : 'Select columns to display',
            COL_NUMBER : 'Nb',
            COL_DISPLAYNAME : 'Name',
            COL_WAITINGCALLS : 'Wait',
            COL_LONGESTWAITTIME : 'Time Wait',
            COL_EWT : 'EWT',
            COL_AVAILABLEAGENTS : 'Avail.',
            COL_TALKINGAGENTS : 'Talk.',
            COL_TOTALNUMBERCALLSENTERED : 'Total',
            COL_TOTALNUMBERCALLSANSWERED : 'Answ.',
            COL_PERCENTAGEANSWEREDBEFORE15 : '%Ans. 15s',
            COL_TOTALNUMBERCALLSABANDONNED : 'Aband.',
            COL_PERCENTAGEABANDONNEDAFTER15 : '%Aband. 15s',
            COL_TOTALNUMBERCALLSCLOSED : 'Closed',
            COL_TOTALNUMBERCALLSTIMEOUT : 'Divert.',
            MS_CHECKALL : 'Check All',
            MS_UNCHECKALL : 'Uncheck All',
            MS_SELECTIONCOUNT : 'checked',
            MS_SELECTIONOF : '/',
            MS_SEARCHPLACEHOLDER : 'Search...',
            MS_BUTTONDEFAULTTEXT : 'Select',
            MS_DYNAMICBUTTONTEXTSUFFIX : 'checked'
        });
        $translateProvider.determinePreferredLanguage();
        $translateProvider.fallbackLanguage([ 'en' ]);

        /*$urlRouterProvider.otherwise("/global");
        $stateProvider.state('global', {
          url : '/global',
          templateUrl : "mainViewGlobal.html"
        })
        .state('group', {
          url : '/group',
          templateUrl :"mainViewGroup.html"
        })
        .state('queue', {
            url : '/queue',
            templateUrl :"mainViewQueue.html"
        })
        .state('agent', {
            url : '/agent',
            templateUrl :"mainViewAgent.html"
        });
        */

    }
})();
