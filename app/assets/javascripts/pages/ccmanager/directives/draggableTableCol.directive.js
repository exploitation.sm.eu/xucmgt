(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('droppableTableCol',droppableTableCol);

    droppableTableCol.$inject= ['$rootScope'];

    function droppableTableCol($rootScope){
        return {
            replace:true,
            link: function(scope, element) {
                var el = element[0];
                el.addEventListener(
                    'dragover',
                    function(e) {
                        e.dataTransfer.dropEffect = 'move';
                        if (e.preventDefault) e.preventDefault();
                        this.style.backgroundColor = "grey";
                        return false;
                    },
                    false
                );
                el.addEventListener(
                    'dragleave',
                    function(e) {
                        e.dataTransfer.dropEffect = 'move';
                        // allows us to drop
                        if (e.preventDefault) e.preventDefault();
                        this.style.backgroundColor = "";
                        return false;
                    },
                    false
                );
                el.addEventListener(
                    'drop',
                    function(e) {
                        if (e.stopPropagation) e.stopPropagation();
                        this.style.backgroundColor = "";
                        $rootScope.$broadcast('moveTableCol', {'colRef' : e.dataTransfer.getData('colref'), 'beforeColRef': scope.$column.ref()});
                        return false;
                    },
                    false
                );
              }
          };
    }
})();