(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('d3Queue',d3Queue);

    d3Queue.$inject= ['$filter'];

    function d3Queue($filter){
        return {
            restrict : 'E' ,
            scope: {
                data: '='
            },
            link : function(scope, element) {
                var heightSvg = 165;
                var widthSvg = 165;
                var svg = d3.select(element[0]).append("svg").attr("height", heightSvg).attr("width", widthSvg);
                var styleRectangleBackground = "fill:#"+Math.floor(Math.random()*16777215).toString(16)+";opacity:0.8;";


                scope.render = function(data) {
                    function setIndicator(svgContainer, size, counter, label, fontSize, formatter) {
                        var position = {};
                        var className = 'queueIndicator';
                        var textStyle = "fill:white;text-anchor:middle; font-size:"+fontSize+"px;";
                        var roundCorner = 10;
                        var drawRect = function() {
                            svgContainer.append("rect").
                            attr("x", position.x).attr("y", position.y).
                            attr("width", size.w).attr("height", size.h).
                            attr("class",className).
                            attr("rx",roundCorner).attr("ry",roundCorner);
                        };
                        var drawCounter = function() {
                            var texteTpsMax = svgContainer.append("text").attr("style", textStyle);
                            texteTpsMax.append("tspan").attr("x", position.x + size.w/2).attr("y", position.y + size.h/2  -(fontSize/2)).text(label);
                            texteTpsMax.append("tspan").attr("x", position.x + size.w/2).attr("y", position.y + size.h/2  + fontSize).text(formatter(counter));
                        };
                        return {
                            drawOn : function(pos) {
                                position = pos;
                                drawRect();
                                drawCounter();
                            }
                        };
                    }
                    d3.select(element[0]).select("g").remove();
                    var svgContainer = svg.append("g");
                    var marge = 5;
                    var padding = 10;
                    var indicatorSize = {w : (widthSvg-marge-(2*padding))/2, h : (heightSvg-marge-(2*padding))/2};
                    var width = (widthSvg-marge-(2*padding))/2;
                    var height = (heightSvg-marge-(2*padding))/2;
                    var fontSize = 15;
                    function getPos() {
                        var Itop = padding, Ileft= padding, Iright = width+marge+padding, Ibottom = height+marge+padding;
                        return {
                            topLeft : {x:Ileft, y:Itop},
                            topRight : {x:Iright, y:Itop},
                            bottomLeft : {x:Ileft,y:Ibottom},
                            bottomRight : {x:Iright, y:Ibottom}
                        };
                    }
                    var pos = getPos();
                    var drawBackground = function(style) {
                        svgContainer.append("rect")
                            .attr("x", 2.5*padding)
                            .attr("y", 2.5*padding)
                            .attr("width", widthSvg-(5*padding))
                            .attr("height", heightSvg-(5*padding))
                            .attr("style", styleRectangleBackground)
                            .attr("transform", "rotate(45,"+(width+(marge/2)+padding)+","+(height+(marge/2)+padding)+")");
                    };
                    var percentFormatter = function(counter) {
                        if (counter) {
                            return $filter('number')(counter,1) + " %";
                        }
                        else {
                            return "- %";
                        }
                    };
                    var noFormat = function(counter){return (counter || "0");};

                    drawBackground(svgContainer,styleRectangleBackground);

                    setIndicator(svgContainer,indicatorSize,data.PercentageAnsweredBefore15,"R 15s",fontSize,percentFormatter).drawOn(pos.topLeft);
                    setIndicator(svgContainer,indicatorSize,data.AvailableAgents,"Dispo",fontSize,noFormat).drawOn(pos.topRight);
                    setIndicator(svgContainer,indicatorSize,data.WaitingCalls,"Att",fontSize,noFormat).drawOn(pos.bottomRight);
                    setIndicator(svgContainer,indicatorSize,data.PercentageAbandonnedAfter15,"A 15s",fontSize,percentFormatter).drawOn(pos.bottomLeft);
                };
                scope.$watch('data', function(){
                    scope.render(scope.data);
                }, true);
            }
        };
    }
})();