(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('droppableAgentTrash',droppableAgentTrash);

    droppableAgentTrash.$inject= ['$rootScope'];

    function droppableAgentTrash($rootScope){
          return {
              replace:true,
              link: function(scope, element) {
                  var el = element[0];
                  el.addEventListener(
                      'dragover',
                      function(e) {
                          e.dataTransfer.dropEffect = 'move';
                          if (e.preventDefault) e.preventDefault();
                          this.style.backgroundColor = "Red";
                          return false;
                      },
                      false
                  );
                  el.addEventListener(
                      'dragleave',
                      function(e) {
                          e.dataTransfer.dropEffect = 'move';
                          // allows us to drop
                          if (e.preventDefault) e.preventDefault();
                          this.style.backgroundColor = "";
                          return false;
                      },
                      false
                  );
                  el.addEventListener(
                      'drop',
                      function(e) {
                          if (e.stopPropagation) e.stopPropagation();
                          this.style.backgroundColor = "";
                          $rootScope.$broadcast('dropAgentTrash', {'agentId' : Number(e.dataTransfer.getData('idAgent')), 'queueId' : Number(e.dataTransfer.getData('idQueue'))});
                          return false;
                      },
                      false
                  );
                }
            };
      }
})();