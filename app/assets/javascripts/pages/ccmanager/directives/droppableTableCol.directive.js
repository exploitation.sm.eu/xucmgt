(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('draggableTableCol',draggableTableCol);

    draggableTableCol.$inject= [];

    function draggableTableCol (){
        return {
            replace:true,
            link : function(scope, element) {
                var el = element[0];
                el.draggable = true;
                el.addEventListener(
                    'dragstart',
                    function(e) {
                        e.dataTransfer.effectAllowed = 'move';
                        e.dataTransfer.setData('colref', scope.$column.ref());
                        this.style.opacity = '0.7';
                        return false;
                    },
                    false
                );
                el.addEventListener(
                    'dragend',
                    function(e) {
                        this.style.opacity = '1';
                        return false;
                    },
                    false
                );
            }
        };
    }
})();