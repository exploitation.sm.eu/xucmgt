(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('droppableGroup',droppableGroup);

    droppableGroup.$inject= ['$rootScope'];

    function droppableGroup($rootScope){
          return {
              replace:true,
              link: function(scope, element) {
                  var el = element[0];
                  el.addEventListener(
                      'dragover',
                      function(e) {
                          e.dataTransfer.dropEffect = 'move';
                          if (e.preventDefault) e.preventDefault();
                          this.style.backgroundColor = "Yellow";
                          return false;
                      },
                      false
                  );
                  el.addEventListener(
                      'dragleave',
                      function(e) {
                          e.dataTransfer.dropEffect = 'move';
                          if (e.preventDefault) e.preventDefault();
                          this.style.backgroundColor = "";
                          return false;
                      },
                      false
                  );
                  el.addEventListener(
                      'drop',
                      function(e) {
                          if (e.stopPropagation) e.stopPropagation();
                          this.style.backgroundColor = "";
                          scope.agentInGroup(Number(e.dataTransfer.getData('groupId')),
                            Number(e.dataTransfer.getData('queueId')),
                            Number(e.dataTransfer.getData('penalty')),
                            scope.queue.id,
                            scope.groupOfGroups.penalty,
                            (e.dataTransfer.getData('clone') === 'true')
                          );
                          return false;
                      },
                      false
                  );
                }
            };
      }
})();