(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('queuefield',queuefield);

    queuefield.$inject= ['$compile'];

    function queuefield ($compile){
        var queueFieldTemplate = '<span class="{{arg.fieldClass}} T{{item[arg.fieldDynClass]}}" ng-bind="item[arg.field] | editableFilter:arg.displayFilter"></span>';
        var linker = function(scope, element, attrs) {
            element.html(queueFieldTemplate);
            $compile(element.contents())(scope);
        };
        return {
            restrict: 'E',
            replace: true,
            scope: {
                item: '=',
                arg: '=',
                editableFilter: '=editableFilter',
                df: '='
            },
            link : linker
        };
    }
})();