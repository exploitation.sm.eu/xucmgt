(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('d3EmptyGroup',d3EmptyGroup);

    d3EmptyGroup.$inject= [];

    function d3EmptyGroup(){
        return {
            restrict : 'E',
            link : function(scope, element) {
                var heightSvg = 20;
                var widthSvg = 6;
                var svgContainer = d3.select(element[0]).append("svg").attr("height", heightSvg).attr("width", widthSvg);
                svgContainer.append("rect").attr("x",5).attr("y",5).attr("width",widthSvg -5).attr("height", heightSvg-5).attr("style", "fill : transparent");

            }
         };
    }
})();