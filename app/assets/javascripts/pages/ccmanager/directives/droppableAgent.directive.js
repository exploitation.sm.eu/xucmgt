(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('droppableAgent',droppableAgent);

    droppableAgent.$inject= ['$rootScope'];

    function droppableAgent($rootScope){
        return {
            replace:true,
            link: function(scope, element) {
                var el = element[0];
                el.addEventListener(
                    'dragover',
                    function(e) {
                        e.dataTransfer.dropEffect = 'move';
                        if (e.preventDefault) e.preventDefault();
                        this.style.backgroundColor = "Orange";
                        return false;
                    },
                    false
                );
                el.addEventListener(
                    'dragleave',
                    function(e) {
                        e.dataTransfer.dropEffect = 'move';
                        // allows us to drop
                        if (e.preventDefault) e.preventDefault();
                        this.style.backgroundColor = "";
                        return false;
                    },
                    false
                );
                el.addEventListener(
                    'drop',
                    function(e) {
                        if (e.stopPropagation) e.stopPropagation();
                        this.style.backgroundColor = "";
                        $rootScope.$broadcast('dropAgent', {'idAgent' : Number(e.dataTransfer.getData('idAgent')), 'oldIdQueue' : Number(e.dataTransfer.getData('idQueue')),
                                                'newIdQueue' : scope.queue.id, 'penalty' : scope.group.penalty, 'clone' : e.dataTransfer.getData('clone') === 'true'});
                        return false;
                    },
                    false
                );
            }
        };
    }
})();
