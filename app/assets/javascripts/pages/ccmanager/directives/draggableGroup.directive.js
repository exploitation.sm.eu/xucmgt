(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('draggableGroup',draggableGroup);

    draggableGroup.$inject= ['$rootScope'];

    function draggableGroup($rootScope){
      return {
          replace:true,
          link : function(scope, element) {
              var el = element[0];
              el.draggable = true;
              el.addEventListener(
                  'dragstart',
                  function(e) {
                      e.dataTransfer.effectAllowed = 'move';
                      e.dataTransfer.setData('groupId', scope.group.id);
                      e.dataTransfer.setData('queueId', scope.queue.id);
                      e.dataTransfer.setData('penalty', scope.groupOfGroups.penalty);
                      e.dataTransfer.setData('clone', e.ctrlKey);
                      this.style.opacity = '0.6';
                      return false;
                  },
                  false
              );
              el.addEventListener(
                  'dragend',
                  function(e) {
                      this.style.opacity = '1';
                      return false;
                  },
                  false
              );
            }
        };
    }
})();