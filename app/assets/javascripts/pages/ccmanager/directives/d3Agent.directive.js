(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('d3Agent',d3Agent);

    d3Agent.$inject= [];

    function d3Agent(){
        return {
            restrict : 'E' ,
            scope: {
                data: '='
            },
            link : function(scope, element) {
                var heightSvg = 50;
                var widthSvg = 65;
                var roundCorner = 5;
                var nameMaxLen = 7;
                var svg = d3.select(element[0]).append("svg").attr("height", heightSvg).attr("width", widthSvg);

                scope.render = function(data) {
                    data=data||{};
                    var fontSize = 12;
                    var formatText = function (str, maxLen) {
                        if (str.length > maxLen) {
                            return str.substring(0, maxLen-1) + '.';
                        }
                        else return str;
                    };
                    d3.select(element[0]).select("g").remove();
                    var svgContainer = svg.append("g");
                    var padding = 1;
                    var agentStyle = "font-size:"+fontSize+"px;";

                    svgContainer.append("rect").attr("x",padding).attr("y",3*padding).attr("rx",roundCorner).attr("ry",roundCorner)
                                               .attr("width", widthSvg-(2*padding)).attr("height", heightSvg-(6*padding))
                                               .attr("class", data.state + " AgentBox");
                    var labelPos = {};
                        labelPos.top = (2*padding) + fontSize*1.1;
                        labelPos.middle = (2*padding) + fontSize*2.5;
                        labelPos.bottom = (2*padding) + fontSize*3.5;
                    var drawAgentLabel = function(text, ypos) {
                        svgContainer.append("text").attr("style", agentStyle)
                                      .append("tspan").attr("x", widthSvg/2).attr("y",ypos)
                                      .attr("class","AgentText").text(text);
                    };
                    drawAgentLabel(data.phoneNb,labelPos.top);
                    var toDisplay = data.lastName ? formatText(data.lastName.toUpperCase(),nameMaxLen) : "";
                    drawAgentLabel(toDisplay,labelPos.middle);
                    toDisplay = data.lastName ? formatText(data.firstName,nameMaxLen) : "";
                    drawAgentLabel(toDisplay,labelPos.bottom);
                };

                scope.$watch('data.state', function(){
                    scope.render(scope.data);
                }, true);

            }
        };
     }
})();