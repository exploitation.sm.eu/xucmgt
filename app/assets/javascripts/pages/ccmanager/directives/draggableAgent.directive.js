(function(){
    'use strict';

    angular
    .module('ccManager')
    .directive('draggableAgent',droppableAgent);

    droppableAgent.$inject= ['$rootScope'];

    function droppableAgent($rootScope){
        return {
            replace:true,
            link : function(scope, element) {
                var el = element[0];
                el.draggable = true;
                el.addEventListener(
                    'dragstart',
                    function(e) {
                        e.dataTransfer.effectAllowed = 'move';
                        e.dataTransfer.setData('idAgent', scope.agent.id);
                        e.dataTransfer.setData('idQueue', scope.queue.id);
                        e.dataTransfer.setData('clone', e.ctrlKey);
                        this.style.opacity = '0.7';
                        return false;
                    },
                    false
                );
                el.addEventListener(
                    'dragend',
                    function(e) {
                        this.style.opacity = '1';
                        return false;
                    },
                    false
                );
            }
        };
    }
})();
