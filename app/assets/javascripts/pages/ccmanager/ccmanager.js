var ccManagerApp = angular.module('ccManager', ['xuc.services','ngCookies','ui.bootstrap','LocalStorageModule','ngTable', 'pascalprecht.translate', 'angularjs-dropdown-multiselect'])
                    .config(['localStorageServiceProvider','$logProvider', function(localStorageServiceProvider,$logProvider){
                        localStorageServiceProvider.setPrefix('ccManager.default');
                        localStorageServiceProvider.setNotify(true,true);
                         $logProvider.debugEnabled(false);
                    }])
                    .run(['Preferences','$templateCache', function(preferences, $templateCache){
                        preferences.init();
                        $templateCache.put('ng-table/filters/dropdown-multi-select.html',
                        '<div ng-dropdown-multiselect="" options="$column.data" selected-model="params.filter()[name]" translation-texts="mstranslation"></div>');
                    }]);
ccManagerApp.config(function ($translateProvider) {
  $translateProvider.translations('fr', {
    COL_AGENT_NUMBER: 'No',
    COL_AGENT_FIRSTNAME: 'Prénom',
    COL_AGENT_LASTNAME: 'Nom',
    COL_AGENT_LOGINDATETIME: 'Login',
    COL_AGENT_LOGOUTDATETIME: 'Logout',
    COL_AGENT_STATE: 'Etat',
    COL_AGENT_STATENAME: 'Etat',
    COL_AGENT_STATUS: 'Prés.',
    COL_AGENT_TIMEINSTATE: 'Depuis',
    COL_AGENT_PHONENB: 'Tél',
    COL_AGENT_PAUSEDTIME: 'Tot. Pause',
    COL_AGENT_WRAPUPTIME: 'Wrapup',
    COL_AGENT_INBCALLS: 'App. Prés.',
    COL_AGENT_INBANSCALLS: 'App. Rép.',
    COL_AGENT_INBCALLTIME: 'Total Com. Entr.',
    COL_AGENT_INBAVERCALLTIME: 'Entr. DMC',
    COL_AGENT_INBUNANSCALLS: 'Non Rép.',
    COL_AGENT_INBPERCUNANSCALLS: '% Non Rép.',
    COL_AGENT_OUTCALLS: 'App. Sortants',
    COL_AGENT_OUTCALLTIME: 'Total Com. Sort.',
    ACTION_LOGIN: 'Logguer',
    ACTION_LOGOUT: 'Delogguer',
    ACTION_PAUSE: 'Pause',
    ACTION_UNPAUSE: 'Annuler Pause',
    ACTION_LISTEN: 'Ecouter',
    SELECT_COLUMN: 'Sélectionner les colonnes à afficher',
    COL_NUMBER: 'No',
    COL_DISPLAYNAME: 'Nom',
    COL_WAITINGCALLS: 'Att.',
    COL_LONGESTWAITTIME: 'Tps Att.',
    COL_EWT: 'TAE',
    COL_AVAILABLEAGENTS: 'Disp.',
    COL_TALKINGAGENTS: 'Conv.',
    COL_TOTALNUMBERCALLSENTERED: 'Total',
    COL_TOTALNUMBERCALLSANSWERED: 'Rép.',
    COL_PERCENTAGEANSWEREDBEFORE15: '%Rép. 15s',
    COL_TOTALNUMBERCALLSABANDONNED: 'Aband.',
    COL_PERCENTAGEABANDONNEDAFTER15: '%Aband. 15s',
    COL_TOTALNUMBERCALLSCLOSED: 'Fermé',
    COL_TOTALNUMBERCALLSTIMEOUT: 'Diss.',
    MS_CHECKALL: 'Tout Selectionner',
    MS_UNCHECKALL: 'Tout Deselect.',
    MS_SELECTIONCOUNT: 'Select',
    MS_SELECTIONOF      : '/',
    MS_SEARCHPLACEHOLDER      : 'Chercher...',
    MS_BUTTONDEFAULTTEXT      : 'Choisir',
    MS_DYNAMICBUTTONTEXTSUFFIX: 'selection.'
  });
  $translateProvider.translations('en', {
    COL_AGENT_NUMBER: 'Nb',
    COL_AGENT_FIRSTNAME: 'First Name',
    COL_AGENT_LASTNAME: 'Last Name',
    COL_AGENT_LOGINDATETIME: 'Login',
    COL_AGENT_LOGOUTDATETIME: 'Logout',
    COL_AGENT_STATE: 'State',
    COL_AGENT_STATENAME: 'State',
    COL_AGENT_STATUS: 'Status.',
    COL_AGENT_TIMEINSTATE: 'Since',
    COL_AGENT_PHONENB: 'Phone',
    COL_AGENT_PAUSEDTIME: 'Tot. Pause',
    COL_AGENT_WRAPUPTIME: 'Wrapup',
    COL_AGENT_INBCALLS: 'Inb. Calls',
    COL_AGENT_INBANSCALLS: 'Inb. Answ.',
    COL_AGENT_INBCALLTIME: 'Inb. Total Com.',
    COL_AGENT_INBAVERCALLTIME: 'Inb. Moy. Com.',
    COL_AGENT_INBUNANSCALLS: 'Inb. Unansw.',
    COL_AGENT_INBPERCUNANSCALLS: 'Inb % Unansw.',
    COL_AGENT_OUTCALLS: 'Out. Calls',
    COL_AGENT_OUTCALLTIME: 'Out. Total Com.',
    ACTION_LOGIN: 'Login',
    ACTION_LOGOUT: 'Logout',
    ACTION_PAUSE: 'Pause',
    ACTION_UNPAUSE: 'Un Pause',
    ACTION_LISTEN: 'Listen',
    SELECT_COLUMN: 'Select columns to display',
    COL_NUMBER: 'Nb',
    COL_DISPLAYNAME: 'Name',
    COL_WAITINGCALLS: 'Wait',
    COL_LONGESTWAITTIME: 'Time Wait',
    COL_EWT: 'EWT',
    COL_AVAILABLEAGENTS: 'Avail.',
    COL_TALKINGAGENTS: 'Talk.',
    COL_TOTALNUMBERCALLSENTERED: 'Total',
    COL_TOTALNUMBERCALLSANSWERED: 'Answ.',
    COL_PERCENTAGEANSWEREDBEFORE15: '%Ans. 15s',
    COL_TOTALNUMBERCALLSABANDONNED: 'Aband.',
    COL_PERCENTAGEABANDONNEDAFTER15: '%Aband. 15s',
    COL_TOTALNUMBERCALLSCLOSED: 'Closed',
    COL_TOTALNUMBERCALLSTIMEOUT: 'Divert.',
    MS_CHECKALL: 'Check All',
    MS_UNCHECKALL: 'Uncheck All',
    MS_SELECTIONCOUNT: 'checked',
    MS_SELECTIONOF      : '/',
    MS_SEARCHPLACEHOLDER      : 'Search...',
    MS_BUTTONDEFAULTTEXT      : 'Select',
    MS_DYNAMICBUTTONTEXTSUFFIX: 'checked'
  });
  $translateProvider.determinePreferredLanguage();
  $translateProvider.fallbackLanguage(['fr']);
});

ccManagerApp.filter('agentFilter', ['$filter',function ($filter) {

    var _filterAgents = function (agents, states) {
        if(states.length === 0) return agents;
        var items = {
            states: [],
            out: []
        };
        angular.forEach(states, function(state, key) {
          if(state.id) this.states.push(state.id);
        }, items);
        angular.forEach(agents, function (agent, key) {
            if (this.states.indexOf(agent.state)>=0 || this.states.indexOf(agent.status)>=0) {
                this.out.push(agent);
            }
        }, items);
        return items.out;
    };

    return function (agents, filters) {
        var items = {
            filters: {},
            out: []
        };
        angular.forEach(filters, function (filter, key) {
            items.filters[key] = filters[key];
        });
        var filteredData = items.filters.stateName ? _filterAgents(agents, items.filters.stateName) : agents;
        items.filters.stateName = undefined;
        items.out = items.filters ? $filter('filter')(filteredData, items.filters) : filteredData;

        return items.out;
    };
}]);

ccManagerApp.directive("queuefield", function($compile) {
    var queueFieldTemplate = '<span class="{{arg.fieldClass}} T{{item[arg.fieldDynClass]}}" ng-bind="item[arg.field] | editableFilter:arg.displayFilter"></span>';
    var linker = function(scope, element, attrs) {
        element.html(queueFieldTemplate);
        $compile(element.contents())(scope);
    };
    return {
        restrict: 'E',
        replace: true,
        scope: {
            item: '=',
            arg: '=',
            editableFilter: '=editableFilter',
            df: '='
        },
        link : linker
    };
});

ccManagerApp.filter('editableFilter', function($filter, $parse) {
    return function(input, arg) {
      if ( typeof arg === 'undefined' ){
        return input;
      }
      return $filter(arg.split(":")[0])(input, arg.split(":")[1]);
    };
});