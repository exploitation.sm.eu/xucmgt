/* jshint ignore:start */
(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('ccAgentDynViewController',ccAgentDynViewController);

    ccAgentDynViewController.$inject= ['$scope','ngTableParams','$filter','XucAgent','Preferences','AgentViewColBuilder','XucGroup','XucAgentTotal','$translate','CCMUtils'];

    function ccAgentDynViewController($scope, ngTableParams, $filter, xucAgent, preferences, agentViewColBuilder, xucGroup, xucAgentTotal, $translate, ccmUtils) {
        var colPrefKey = 'agentViewColumns';
        $scope.mstranslation = {
            checkAll : $translate.instant('MS_CHECKALL'),
            uncheckAll : $translate.instant('MS_UNCHECKALL'),
            selectionCount : $translate.instant('MS_SELECTIONCOUNT'),
            selectionOf : $translate.instant('MS_SELECTIONOF'),
            searchPlaceholder : $translate.instant('MS_SEARCHPLACEHOLDER'),
            buttonDefaultText : $translate.instant('MS_BUTTONDEFAULTTEXT'),
            dynamicButtonTextSuffix: $translate.instant('MS_DYNAMICBUTTONTEXTSUFFIX')
        };

        $scope.showGroup = preferences.getOption("AgentView.showGroup",{showGroup:false}).showGroup;

        $scope.agents = [];
        $scope.groups = [];
        $scope.totals = [];
        $scope.totals['default'] = {AgentPausedTotalTime:1000};
        $scope.cols = [];

        var availCols = [];


        $scope.getGroup = function(id) {
            return xucGroup.getGroup(id);
        };

        var buildViewCols = function() {
            if (availCols.length === 0) {
                availCols = agentViewColBuilder.buildCols();
                $scope.cols = preferences.getViewColumns(availCols,colPrefKey);
                agentViewColBuilder.buildTable($scope);
            }
        };


        $scope.orderByGroupName = function(agent) {
            return  xucGroup.getGroup(agent.groupId).name;
        };
        $scope.tableSettings = function() {
            var calculatePage = function(params) {
                if (params.total() <= (params.page() - 1)  * params.count()) {
                    var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
                    params.page(setPage);
                }ccAgentDynViewController
            };
            return {
                filterDelay: 0,

                groupBy: $scope.showGroup ? function(item) {
                    return xucGroup.getGroup(item.groupId).name;
                } : undefined,
                total: $scope.agents.length, // length of data
                counts:[],
                getData: function($defer, params) {
                    var getDataByGroup = function(data) {
                        var orderedData = params.sorting() ?
                                            $filter('orderBy')(data, [$scope.orderByGroupName,params.orderBy()[0]]) : $filter('orderBy')(data, $scope.orderByGroupName);
                        var slicedData = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                        return (params.sorting() ? $filter('orderBy')(slicedData, params.orderBy()) : slicedData);
                    };
                    var getDataByList = function(data) {
                        var orderedData = params.sorting() ?
                                            $filter('orderBy')(data, params.orderBy()) : data;
                        return(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    };
                    calculatePage(params);
                    var data = $scope.agents;
                    var filteredData = params.filter() ?
                                       $filter('agentFilter')(data, params.filter()) : data;
                    params.total(filteredData.length);
                    if($scope.showGroup) {
                        $defer.resolve(getDataByGroup(filteredData));
                    }
                    else {
                        $defer.resolve(getDataByList(filteredData));
                    }
                }
            };
        };

        $scope.agentDynTable = new ngTableParams({
            page: 1,
            count: $scope.showGroup ? 40: 40,
            sorting: {
                firstName: 'asc'
            },
          }, $scope.tableSettings()
        );

        var dfReloadTable = ccmUtils.doDeffered(function() {
            $scope.agentDynTable.reload();
        });

        var dfReloadAll = ccmUtils.doDeffered(function() {
            var qIds = preferences.getSelectedQueues();
            $scope.agents = xucAgent.getAgentsInQueueByIds(qIds);
            $scope.totals = xucAgentTotal.calculate($scope.agents);
            $scope.agentDynTable.reload();
        });

        var dfCalculateTotals = ccmUtils.doDeffered(function(){
            $scope.totals = xucAgentTotal.calculate($scope.agents);
        });
        var isSortedByStat = function() {
            for(var key in $scope.agentDynTable.$params.sorting) {
                if(key.split("stats.").length > 1) return true;
            }
            return false;
        };

        $scope.$on('AgentStatisticsUpdated', function(){
            if (isSortedByStat()) {
                dfReloadTable();
            }
            dfCalculateTotals();
        });

        $scope.$on('AgentStateUpdated', function(){
            if (!angular.isUndefined($scope.agentDynTable.$params.sorting.stateName) ||
                (!angular.isUndefined($scope.agentDynTable.$params.filter.stateName) && $scope.agentDynTable.$params.filter.stateName.length > 0)) {
                dfReloadTable();
             }
        });

        $scope.$on('AgentsLoaded', function(){
            dfReloadAll();
        });

        $scope.$on('QueueMemberUpdated', function() {
            dfReloadAll();
        });

        $scope.$on('preferences.queueSelected', function(event,args) {
            dfReloadAll();
        });

        $scope.$on('preferences.'+colPrefKey, function() {
            $scope.cols = preferences.getViewColumns(availCols, colPrefKey);
        });
        $scope.$on('moveTableCol', function(event,args){
            preferences.moveCol(args.colRef, args.beforeColRef, $scope.cols, colPrefKey);
        });

        buildViewCols();
        dfReloadAll();
    }
})();