(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('queueSelectCtrl',queueSelectCtrl);

    queueSelectCtrl.$inject= ['$scope','XucQueue','Preferences'];

    function queueSelectCtrl($scope, XucQueue, preferences) {
        $scope.queues = XucQueue.getQueues();

        $scope.isSelected = function(queueId) {
            return preferences.isQueueSelected(queueId);
        };
        $scope.toggleQueue = function(queueId) {
            preferences.toggleQueueSelection(queueId);
        };
        var noQueueLoaded = function() {
            return $scope.queues.length === 0;
        };
        $scope.$on('QueuesLoaded', function() {
            $scope.queues = XucQueue.getQueues();
            $scope.status.disabled = ($scope.queues.length === 0);
            $scope.status.opened = preferences.noQueueSelected() && ($scope.queues.length > 0);
        });
        $scope.status = {
            disabled: (noQueueLoaded()),
            opened: (preferences.noQueueSelected())
        };
    }
})();