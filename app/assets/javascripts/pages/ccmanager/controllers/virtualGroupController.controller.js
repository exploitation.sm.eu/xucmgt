/* jshint ignore:start */
(function() {
	'use strict';
	
	
	angular
	.module('ccManager')
	.controller('virtualGroupController',virtualGroupController);

	virtualGroupController.$inject = [ '$scope', '$modal', 'ngTableParams','$filter' ];

	function virtualGroupController($scope, $modal, ngTableParams, $filter) {
		var ModalInstanceCtrl = function($scope, $modalInstance,ngTableParameters, group, queue, penalty) {
			$scope.group = group;
			$scope.agents = angular.copy(group.agents);
			$scope.queue = queue;
			$scope.penalty = penalty;

			$scope.tableSettings = function() {
				var calculatePage = function(params) {
					if (params.total() <= (params.page() - 1) * params.count()) {
						var setPage = (params.page() - 1) > 0 && (params.page() - 1) || 1;
						params.page(setPage);
					}
				};
				return {
					total : $scope.agents.length, // length of data
					counts : [],
					getData : function($defer, params) {
						var orderedData = params.sorting() ? $filter('orderBy')($scope.agents, params.orderBy()): $scope.agents;
						params.total(orderedData.length);
						calculatePage(params);
						$defer.resolve(orderedData.slice((params.page() - 1)* params.count(), params.page()* params.count()));
					}
				};
			};

			
			$scope.agentsInGroup = new ngTableParameters({
				page : 1,
				count : 10,
				sorting : {
					name : 'asc'
				}
			}, $scope.tableSettings());

			$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			};
		};

		$scope.showGroup = function(group, queue, penalty) {
			$scope.group = group;

			var modalInstance = $modal.open({
				templateUrl : 'showGroupContent.html',
				controller : ModalInstanceCtrl,
				resolve : {
					ngTableParameters : function() {
						return ngTableParams;
					},
					group : function() {
						return $scope.group;
					},
					queue : function() {
						return $scope.queue;
					},
					penalty : function() {
						return penalty;
					}
				}
			});

			modalInstance.result.then(function(agent) {
			}, function() {
			});
		};

	}
})();