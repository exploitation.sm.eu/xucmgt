(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('ccChooseAgentCtrl',ccChooseAgentCtrl);

    ccChooseAgentCtrl.$inject= ['$scope','XucAgent','$modal'];

    function ccChooseAgentCtrl($scope, XucAgent, $modal) {

        var ModalInstanceCtrl = function ($scope, $modalInstance, agents) {

          $scope.agents = agents;
          $scope.orderList = "lastName";
          $scope.selected = {
            agent: $scope.agents[0]
          };

          $scope.ok = function () {
            $modalInstance.close($scope.selected.agent);
          };
          $scope.selected = function (agent) {
            $modalInstance.close(agent);
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };

        $scope.agents = XucAgent.getAgents();
        $scope.addAgent = function(queueId,penalty) {
            $scope.agents = XucAgent.getAgentsNotInQueue(queueId);
            var modalInstance = $modal.open({
              templateUrl: 'myModalContent.html',
              controller: ModalInstanceCtrl,
              size: 'sm',
              resolve: {
                agents: function () {
                  return $scope.agents;
                }
              }
            });
            modalInstance.result.then(function (agent) {
                 Cti.setAgentQueue(agent.id,queueId,penalty);
            }, function () {
                }
            );
        };
    }
})();