/* jshint ignore:start */
(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('addAgentGroupCtrl',addAgentGroupCtrl);

    addAgentGroupCtrl.$inject= ['$scope','XucGroup','$modal','ngTableParams','$filter'];

    function addAgentGroupCtrl($scope, xucGroup, $modal, ngTableParams, $filter) {

        var ModalInstanceCtrl = function ($scope, $modalInstance,
                ngTableParameters, xucGroup, queue, penalty) {

          $scope.groups = xucGroup.getAvailableGroups(queue.id, penalty);
          $scope.queue = queue;
          $scope.penalty = penalty;

          $scope.addGroup = function(group, queueId, penalty) {
            xucGroup.addAgentsNotInQueueFromGroupTo(group.id,queueId, penalty);
            $modalInstance.dismiss('done');
          };
          $scope.tableSettings = function() {
            var calculatePage = function(params) {
                if (params.total() <= (params.page() - 1)  * params.count()) {
                    var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
                    params.page(setPage);
                }
            };
            return {
                total: $scope.groups.length, // length of data
                counts:[],
                getData: function($defer, params) {
                    var orderedData = params.sorting() ?
                                        $filter('orderBy')($scope.groups, params.orderBy()) :
                                        $scope.groups;
                    params.total(orderedData.length);
                    calculatePage(params);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            };
          };

          $scope.agentGroups = new ngTableParameters({
            page: 1,
            count: 10,
            sorting: {
                name: 'asc'
            }
          }, $scope.tableSettings()
          );

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };

        $scope.addAgentGroup = function(queue, penalty) {
            $scope.queue = queue;
            $scope.penalty = penalty;

            var modalInstance = $modal.open({
              templateUrl: 'addAgentGroupContent.html',
              controller: ModalInstanceCtrl,
              resolve: {
                ngTableParameters :function () {
                  return ngTableParams;
                },
                xucGroup: function() {
                    return xucGroup;
                },
                queue: function() {
                    return $scope.queue;
                },
                penalty: function() {
                    return $scope.penalty;
                }
              }
            });

            modalInstance.result.then(function (agent) {
            }, function () {
            });
        };
    }
})();