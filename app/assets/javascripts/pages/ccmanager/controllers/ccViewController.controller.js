(function() {
    'use strict';

    angular.module('ccManager').controller('ccViewController', ccViewController);

    ccViewController.$inject = ['$scope', 'localStorageService','$rootScope','$location','Preferences'];

    function ccViewController($scope, localStorageService,$rootScope,$location,preferences) {
        $scope.viewName = localStorageService.get('ccmanagerView') || 'global';
        var _manageViewParameter = function (){
            var viewName = $location.search().view;
            if(viewName){
                $scope.setView(viewName);
            }
        };

        var _manageQueuesParameter = function (){
            var queues = $location.search().queues;
            if(queues){
                preferences.setQueuesVisibility(queues,true);
                $rootScope.$broadcast('preferences.queueSelected');
            }
        };

        $rootScope.$on('$locationChangeSuccess',function(event){
            _manageViewParameter();
            _manageQueuesParameter();
        });

        $rootScope.$on('PostQueuesLoaded', function() {
            _manageQueuesParameter();
        });

        $scope.isActive = function(viewName) {
            return ($scope.viewName === viewName);
        };
        $rootScope.$on('changeView', function(event) {
            $scope.setView(event.viewName);
        });

        $scope.setView = function(viewName) {
            $scope.viewName = viewName;
            localStorageService.set('ccmanagerView', $scope.viewName);
        };
    }
})();