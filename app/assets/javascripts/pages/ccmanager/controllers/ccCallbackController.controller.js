/* jshint ignore:start */
(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('ccCallbackController',ccCallbackController);

    ccCallbackController.$inject= ['$scope','XucCallback'];

    function ccCallbackController($scope, xucCallback) {
        $scope.callbackLists = [];

        $scope.refreshCallbacks = function() {
            $scope.callbackLists = xucCallback.getCallbackLists();
            $scope.$apply();
        };

        $scope.upload = function(listUuid) {
            var charset = 'utf-8';
            if(window.navigator.platform == 'Win32')
                charset = 'iso-8859-1';
            $.ajax({
                type : "POST",
                contentType: "text/plain;charset=" + charset,
                data: $('#import-'+listUuid)[0].files[0],
                url : 'http://' + externalConfig.hostAndPort + '/xuc/api/1.0/callback_lists/' + listUuid + '/callback_requests/csv',
                processData: false,
                error: function(e) {
                    console.log("Erreur !!!!!!!");
                    console.log(e);
                },
                success: function(response) {
                    xucCallback.refreshCallbacks();
                }
            });
        };

        $scope.$on('CallbacksLoaded', $scope.refreshCallbacks);
        $scope.$on('CallbackTaken', $scope.refreshCallbacks);
        $scope.$on('CallbackReleased', $scope.refreshCallbacks);
        $scope.$on('CallbackClotured', $scope.refreshCallbacks);
    }

})();