(function() {
    'use strict';

    angular.module('ccManager').controller('ctiLinkController', ctiLinkController);

    ctiLinkController.$inject = [ '$rootScope', '$scope', '$timeout', 'XucAgent', 'XucQueue', 'XucGroup' ,'$location'];
    function ctiLinkController($rootScope, $scope, $timeout, xucAgent, xucQueue, xucGroup,$location) {
        var clockFrequency = 10;
        var mytimeout;

        _initCti(externalConfig.ctiUsername,externalConfig.ctiPassword,externalConfig.hostAndPort,(externalConfig.ctiUseSsl)?"wss://":"ws://");

        var startAllServices = function() {
            xucGroup.start();
            xucQueue.start();
            xucAgent.start();
        };

        $scope.loggedOnHandler = function(event) {
            $rootScope.$broadcast('ctiLoggedOn');
            startAllServices();
        };

        $scope.linkStatusHandler = function(event) {
            $scope.status = event.status;
            console.log(event.status);
            if (event.status !== 'opened' && event.status !== 'closed') {
                $rootScope.$broadcast('linkDisConnected');
                window.location.replace("/ccmanager?error='unable to connect to xuc server'");
            }
            $scope.$apply();
        };

        // timeout
        var updateClock = function() {
            $scope.clock = moment().format('H:mm:ss');
            mytimeout = $timeout(updateClock, clockFrequency * 1000);
            $scope.$digest();
        };
        $timeout(updateClock, 1000, false);

        Cti.setHandler(Cti.MessageType.LOGGEDON, $scope.loggedOnHandler);
        Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, $scope.linkStatusHandler);
    }
})();