(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('ccAgentDynViewPrefController',ccAgentDynViewPrefController);

    ccAgentDynViewPrefController.$inject= ['$scope','Preferences'];

    function ccAgentDynViewPrefController($scope,preferences) {
        $scope.agentView = preferences.getOption("AgentView.showGroup",{showGroup:false});

        $scope.toggleShowGroup = function() {
            preferences.setOption("AgentView.showGroup", $scope.agentView);
        };
    }
})();