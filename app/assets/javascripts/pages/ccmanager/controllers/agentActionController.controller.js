(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('agentActionController',agentActionController);

    agentActionController.$inject= ['$scope','XucAgent'];

    function agentActionController($scope, xucAgent) {

        $scope.canLogIn = function(agentId) {
            return xucAgent.canLogIn(agentId);
        };
        $scope.login = function(agentId) {
            xucAgent.login(agentId);
        };
        $scope.canLogOut = function(agentId) {
            return xucAgent.canLogOut(agentId);
        };
        $scope.logout = function(agentId) {
            xucAgent.logout(agentId);
        };
        $scope.canPause = function(agentId){
            return xucAgent.canPause(agentId);
        };
        $scope.pause = function(agentId) {
            xucAgent.pause(agentId);
        };
        $scope.canUnPause = function(agentId){
            return xucAgent.canUnPause(agentId);
        };
        $scope.unpause = function(agentId) {
            xucAgent.unpause(agentId);
        };
        $scope.canListen = function(agentId) {
            return xucAgent.canListen(agentId);
        };
        $scope.listen = function(agentId) {
            xucAgent.listen(agentId);
        };
}
})();