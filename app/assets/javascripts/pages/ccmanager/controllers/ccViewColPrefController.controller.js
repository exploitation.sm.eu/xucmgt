(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('ccViewColPrefController',ccViewColPrefController);

    ccViewColPrefController.$inject= ['$scope','Preferences'];

    function ccViewColPrefController($scope,preferences) {
        $scope.selectedCols = [];
        $scope.selectableCols = [];
        $scope.selectActive = false;

        var updateSelectBox = function() {
            $scope.selectedCols = [];
            $scope.selectableCols = [];
            angular.forEach($scope.cols, function(col){
                if (col.show && typeof col.title !== 'undefined') {
                    $scope.selectedCols.push(col);
                }
                if (typeof col.title !== 'undefined') {
                    $scope.selectableCols.push(col);
                }
            });
        };

        var colIsSelected = function(id) {
            var selected = false;
            angular.forEach($scope.selectedCols, function(col){
                if (col.id === id) {
                    selected = true;
                }
            });
            return selected;
        };

        var prepareColToSave = function() {
            var colToSave = [];
            angular.forEach($scope.cols, function(col){
                if (colIsSelected(col.id) || typeof col.title === 'undefined') {
                    colToSave.push({id:col.id, show: true});
                }
                else {
                    colToSave.push({id:col.id, show: false});
                }
            });
            return colToSave;
        };
        $scope.selectColChanged = function(key) {
            var colToSave = prepareColToSave();
            preferences.saveViewColumns(colToSave,key);
        };

        $scope.toggleSelectCol = function() {
            if (!$scope.selectActive) {
                updateSelectBox();
            }
            $scope.selectActive = !$scope.selectActive;
        };
    }
})();