/* jshint ignore:start */
(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('ccQueuesController',ccQueuesController);

    ccQueuesController.$inject= ['XucQueue','XucAgent','XucGroup', '$scope', '$rootScope', 'CCMUtils','Preferences', '$attrs'];

    function ccQueuesController(xucQueue, xucAgent, xucGroup, $scope, $rootScope, ccmUtils, preferences, $attrs) {

        $scope.showCallbacks = ($attrs.showcallbacks === 'true');
        console.log("Show callbacks configuration: ", $scope.showCallbacks);

        var MAXPENALTY = 20;

        $scope.queues = [];
        $scope.groupsArray = [];
        $scope.groupGroups = [];
        $scope.folded = false;
        $scope.agentFilter = {
            showAllAgents : true
        };

        $scope.$watch('showAllAgents', function() {
            buildAllGroups();
        });

        $scope.isFolded = function(penalty) {
            return (penalty === 0) && $scope.folded;
        };
        $scope.isUnFolded = function(penalty) {
            return (penalty === 0) && !$scope.folded;
        };
        $scope.fold = function() {
            $scope.folded = true;
        };
        $scope.unFold = function() {
            $scope.folded = false;
        };
        $scope.showGroup = function(penalty) {
            return ((penalty === 0) || !$scope.folded );
        };
        $scope.toggleShowAll = function() {
            buildAllGroups();
        };

        $scope.$on('linkDisConnected', function(e) {
            $scope.connected = false;
            $scope.queues.length = 0;
        });

        var agentGroupTpl = function(penalty) { return( {'penalty' : penalty, 'agents' : []});};

        var initGroupGbl = function(template, groups) {
            var initGrp = function(penalty) {
                if (typeof groups[penalty] === 'undefined') groups[penalty] = template(penalty);
                if (!groups[penalty-1] && penalty > 0) initGrp(penalty-1);

            };
            return initGrp;
        };

        var addToAgentGroup = function(group, agent) {
            group.agents.push(agent);
        };
        $scope.buildGroups = function(queueId, agents, addToGroup, template) {
            var groups = [];
            var initGroup = initGroupGbl(template, groups);
            var getGroup = function(penalty) {
                if (typeof groups[penalty] === 'undefined') initGroup(penalty);
                return groups[penalty];
            };
            var buildGroups = function() {
                angular.forEach(agents, function (agent, keyAgent) {
                    if ($scope.agentFilter.showAllAgents || agent.state !== 'AgentLoggedOut' && typeof agent.queueMembers[queueId] !== 'undefined') {
                        var group = getGroup(agent.queueMembers[queueId]);
                        addToGroup(group,agent);
                        if (agent.queueMembers[queueId]+1 < MAXPENALTY) initGroup(agent.queueMembers[queueId]+1);
                    }
                });
            };
            initGroup(0);
            buildGroups();
            return groups;
        };
        $scope.buildGroupMember = function(queueId, agents) {
            var groups = $scope.buildGroups(queueId,agents,addToAgentGroup, agentGroupTpl);
            $scope.groupsArray[queueId] = {'idQueue' : queueId, 'groups' : groups};
        };
        $scope.buildGroupOfGroups = function(queueId, agents) {
            var groups = xucGroup.getGroupsForAQueue(queueId);
            $scope.groupGroups[queueId] = {'idQueue' : queueId, 'groups' : groups};
        };

        var initQueueGroupMembers = function() {
            for (var i = 0; i < $scope.queues.length; i++) {
                $scope.buildGroupMember($scope.queues[i].id,[]);
                $scope.buildGroupOfGroups($scope.queues[i].id,[]);
            }
        };
        var buildGroupForAQueue = function(queueId) {
            var ags = xucAgent.getAgentsInQueue(queueId);
            $scope.buildGroupMember(queueId,ags);
            $scope.buildGroupOfGroups(queueId,ags);
        };
        var buildAllGroups = function() {
            for (var i = 0; i < $scope.queues.length; i++) {
                buildGroupForAQueue($scope.queues[i].id);
            }
        };
        var selectQueue = function() {
            var isQueueSelected = function(queue) {
                return preferences.isQueueSelected(queue.id);
            };
            $scope.queues = xucQueue.getQueues().filter(isQueueSelected);
        };

        $scope.$on('QueuesLoaded', function() {
            selectQueue();
            initQueueGroupMembers();
            $rootScope.$broadcast('PostQueuesLoaded');
        });

        $scope.$on('preferences.queueSelected', function(event,args) {
            dfSelectQueue();
        });

        $scope.$on('QueueMemberUpdated', function(event, queueId){
            buildGroupForAQueue(queueId);
            dfDigest();
        });

        $scope.$on('AgentStateUpdated', function(event, agentId){
            dfDigest();
        });

        $scope.$on('AgentConfigUpdated', function(event, agentId) {
            buildAllGroups();
            dfDigest();
        });

        var dfBuildGroups = ccmUtils.doDeffered(function() {
            $scope.$digest();
            buildAllGroups();
        });
        var dfSelectQueue = ccmUtils.doDeffered(function(){
            selectQueue();
            buildAllGroups();
        });
        var dfDigest = ccmUtils.doDeffered(function(){$scope.$digest();});

    }
})();