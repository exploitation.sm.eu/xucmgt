/* jshint ignore:start */
(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('ccQueueDynViewController',ccQueueDynViewController);

    ccQueueDynViewController.$inject= ['$scope','ngTableParams','$filter','XucQueueTotal','ViewColBuilder', 'Preferences'];

    function ccQueueDynViewController($scope, ngTableParams, $filter, xucQueueTotal,viewColBuilder, preferences) {
        var colPrefKey = 'queueViewColumns';

        $scope.stats = xucQueueTotal.getCalcStats();

       var availCols = [];
       availCols.push(viewColBuilder.col('number').withFieldClass('badge').build());
       availCols.push(viewColBuilder.col('displayName').build());
       availCols.push(viewColBuilder.col('WaitingCalls').build());
       availCols.push(viewColBuilder.col('LongestWaitTime').withDisplayFilter('queueTime').build());
       availCols.push(viewColBuilder.col('EWT').withDisplayFilter('queueTime').build());
       availCols.push(viewColBuilder.col('AvailableAgents').build());
       availCols.push(viewColBuilder.col('TalkingAgents').build());
       availCols.push(viewColBuilder.col('TotalNumberCallsEntered').build());
       availCols.push(viewColBuilder.col('TotalNumberCallsAnswered').build());
       availCols.push(viewColBuilder.col('PercentageAnsweredBefore15').withDisplayFilter('number:1').build());
       availCols.push(viewColBuilder.col('TotalNumberCallsAbandonned').build());
       availCols.push(viewColBuilder.col('PercentageAbandonnedAfter15').withDisplayFilter('number:1').build());
       availCols.push(viewColBuilder.col('TotalNumberCallsClosed').build());
       availCols.push(viewColBuilder.col('TotalNumberCallsTimeout').build());

       $scope.totals = {};
       $scope.totals.WaitingCalls = {value:'stats.sum.WaitingCalls'};
       $scope.totals.LongestWaitTime = {value:'stats.max.LongestWaitTime | queueTime'};
       $scope.totals.EWT = {value:'stats.max.EWT | queueTime'};
       $scope.totals.TotalNumberCallsEntered = {value:'stats.sum.TotalNumberCallsEntered'};
       $scope.totals.TotalNumberCallsAnswered = {value:'stats.sum.TotalNumberCallsAnswered'};
       $scope.totals.PercentageAnsweredBefore15 = {value:'stats.global.PercentageAnsweredBefore15 | number:1'};
       $scope.totals.TotalNumberCallsAbandonned = {value:'stats.sum.TotalNumberCallsAbandonned'};
       $scope.totals.PercentageAbandonnedAfter15 = {value:'stats.global.PercentageAbandonnedAfter15 | number:1'};
       $scope.totals.TotalNumberCallsClosed = {value:'stats.sum.TotalNumberCallsClosed'};
       $scope.totals.TotalNumberCallsTimeout = {value:'stats.sum.TotalNumberCallsTimeout'};

       $scope.cols = preferences.getViewColumns(availCols,colPrefKey);

       $scope.tableSettings = function() {
           var calculatePage = function(params) {
               if (params.total() <= (params.page() - 1)  * params.count()) {
                   var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
                   params.page(setPage);
               }
           };
           return {
               total: $scope.queues.length, // length of data
               counts:[],
               getData: function($defer, params) {
                   var orderedData = params.sorting() ?
                                       $filter('orderBy')($scope.queues, params.orderBy()) :
                                       $scope.queues;
                   params.total(orderedData.length);
                   xucQueueTotal.calculate(orderedData);
                   $scope.stats = xucQueueTotal.getCalcStats();
                   calculatePage(params);
                   $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
               }
           };
       };

       $scope.queueDynTable = new ngTableParams({
           page: 1,
           count: 50,
           sorting: {
               displayName: 'asc'
           }
         }, $scope.tableSettings()
       );

       $scope.$watch('queues', function(newQueues, oldQueues) {
           if (newQueues.length>0) $scope.queueDynTable.reload();
       },true);

       $scope.$on('preferences.'+colPrefKey, function() {
           $scope.cols = preferences.getViewColumns(availCols, colPrefKey);
       });
       $scope.$on('moveTableCol', function(event,args){
           preferences.moveCol(args.colRef, args.beforeColRef, $scope.cols, colPrefKey);
       });
    }
})();