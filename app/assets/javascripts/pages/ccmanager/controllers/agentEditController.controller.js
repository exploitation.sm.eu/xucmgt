/* jshint ignore:start */
(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('agentEditController',agentEditController);

    agentEditController.$inject= ['$scope','XucAgent','XucQueue','$modal','ngTableParams','$filter'];

    function agentEditController($scope, xucAgent, xucQueue, $modal, ngTableParams,$filter) {

        $scope.loadAgentQueues = function(queueMembers) {
            var queues = [];
            angular.forEach(queueMembers, function (penalty, queueId) {
                var queueMember = xucQueue.getQueue(queueId);
                queueMember.penalty = penalty;
                queues.push(xucQueue.getQueue(queueId));
            });
            return queues;
        };



        var ModalInstanceCtrl = function ($scope, $modalInstance, agent,
                ngTableParameters, xucQueue, xucAgent) {

          $scope.agent = agent;
          $scope.queues = agent.queues;
          $scope.newQueue = {
            queue: null,
            penalty: 0
          };
          $scope.availableQueues = xucQueue.getQueuesExcept($scope.queues);
          $scope.agentGroups = [];
          $scope.agentGroupsHandler = function(groups) {
            for(var i=0; i<groups.length; i++) {
                group = groups[i];
                group.realName = group.displayName ? group.displayName: group.name;
            }
            $scope.agentGroups = groups;
          };
          Cti.setHandler(Cti.MessageType.AGENTGROUPLIST, $scope.agentGroupsHandler);
          Cti.getList('agentgroup');

          $scope.logout = function() {
            console.log("logout " + agent.id);
            xucAgent.logout(agent.id);
          };

          $scope.tableSettings = function() {
            var calculatePage = function(params) {
                if (params.total() <= (params.page() - 1)  * params.count()) {
                    var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
                    params.page(setPage);
                }
            };
            return {
                total: $scope.queues.length, // length of data
                counts:[],
                getData: function($defer, params) {
                    var orderedData = params.sorting() ?
                                        $filter('orderBy')($scope.queues, params.orderBy()) :
                                        $scope.queues;
                    params.total(orderedData.length);
                    calculatePage(params);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            };
          };

          $scope.agentQueues = new ngTableParameters({
            page: 1,
            count: 10,
            sorting: {
                penalty: 'asc'
            }
          }, $scope.tableSettings()
          );

          $scope.addQueue = function() {
             var queue = $scope.newQueue.queue;
             queue.penalty = $scope.newQueue.penalty;
             $scope.queues.push(queue);
             $scope.availableQueues = xucQueue.getQueuesExcept($scope.queues);
             $scope.newQueue.queue = null;
             $scope.agentQueues.reload();
          };
          $scope.removeQueue = function(queue) {
            $scope.queues.splice($scope.queues.indexOf(queue), 1);
            $scope.availableQueues = xucQueue.getQueuesExcept($scope.queues);
            $scope.agentQueues.reload();
          };
          $scope.ok = function () {
            xucAgent.updateQueues($scope.agent.id,$scope.queues);
            Cti.setAgentGroup($scope.agent.id, $scope.agent.groupId);
            $modalInstance.close($scope.agent);
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
          $scope.validatePenalty = function(queue) {
            if (typeof queue.penalty === 'undefined') {
                queue.penalty = $scope.agent.queueMembers[queue.id] || 0;
            }
          };
        };

        $scope.editAgent = function(agentId) {

            $scope.agent = xucAgent.getAgent(agentId);
            $scope.agent.queues = {};
            $scope.agent.queues = $scope.loadAgentQueues($scope.agent.queueMembers);

            var modalInstance = $modal.open({
              templateUrl: 'agentEditContent.html',
              controller: ModalInstanceCtrl,
              resolve: {
                agent: function () {
                  return $scope.agent;
                },
                ngTableParameters :function () {
                  return ngTableParams;
                },
                xucQueue: function() {
                    return xucQueue;
                },
                xucAgent: function() {
                    return xucAgent;
                }
              }
            });

            modalInstance.result.then(function (agent) {
            }, function () {
                }
            );
        };
    }
})();