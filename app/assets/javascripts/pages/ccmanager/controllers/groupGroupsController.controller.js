(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('groupGroupsController',groupGroupsController);

    groupGroupsController.$inject= ['$scope','XucGroup'];

    function groupGroupsController($scope, xucGroup) {

        $scope.agentInGroup = function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty, clone) {
            if (clone) {
                xucGroup.addAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
            }
            else {
                xucGroup.moveAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
            }
        };
        $scope.removeAgentGroupFromQueueGroup = function(groupId, queueId, penalty) {
            xucGroup.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);
        };
    }
})();