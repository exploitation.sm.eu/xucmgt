(function(){
    'use strict';

    angular
    .module('ccManager')
    .controller('ccAgentsController',ccAgentsController);

    ccAgentsController.$inject= ['$scope', '$timeout','XucAgent'];

    function ccAgentsController($scope, $timeout, xucAgent) {


        $scope.$on('linkDisConnected', function(e) {
            $scope.connected = false;
        });

        $scope.$on('dropAgent', function(event,args) {
            Cti.setAgentQueue(args.idAgent,args.newIdQueue,args.penalty);
            if (! args.clone && (args.newIdQueue !== args.oldIdQueue)) {
                Cti.removeAgentFromQueue(args.idAgent, args.oldIdQueue);
            }
        });

        $scope.$on('dropAgentTrash', function(event,args) {
            Cti.removeAgentFromQueue(args.agentId, args.queueId);
        });
    }
})();