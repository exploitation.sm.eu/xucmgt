(function() {
    'use strict';

    angular.module('ccManager').filter('agentFilter', agentFilter);

    agentFilter.$inject = [ '$filter' ];

    function agentFilter($filter) {

        var _filterAgents = function(agents, states) {
            if (states.length === 0)
                return agents;
            var items = {
                states : [],
                out : []
            };
            angular.forEach(states, function(state, key) {
                if (state.id)
                    this.states.push(state.id);
            }, items);
            angular.forEach(agents, function(agent, key) {
                if (this.states.indexOf(agent.state) >= 0 || this.states.indexOf(agent.status) >= 0) {
                    this.out.push(agent);
                }
            }, items);
            return items.out;
        };

        return function(agents, filters) {
            var items = {
                filters : {},
                out : []
            };
            angular.forEach(filters, function(filter, key) {
                items.filters[key] = filters[key];
            });
            var filteredData = items.filters.stateName ? _filterAgents(agents, items.filters.stateName) : agents;
            items.filters.stateName = undefined;
            items.out = items.filters ? $filter('filter')(filteredData, items.filters) : filteredData;

            return items.out;
        };
    }
})();