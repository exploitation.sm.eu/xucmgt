(function() {
    'use strict';

    angular.module('ccManager').filter('editableFilter', editableFilter);

    editableFilter.$inject = [ '$filter', '$parse' ];

    function editableFilter($filter, $parse) {
        return function(input, arg) {
            if (typeof arg === 'undefined') {
                return input;
            }
            return $filter(arg.split(":")[0])(input, arg.split(":")[1]);
        };
    }
})();