(function(){
    'use strict';

    angular
    .module('ccManager')
    .factory('CCMUtils',CCMUtils);

    CCMUtils.$inject= ['$timeout','$log'];

    function CCMUtils($timeout, $log){
        var deferredTime = 2000;

        var _doDeffered = function(updateFunc) {
            var d;
            var defer = false;
            var doUpdate = function() {
                var deferedUpdate = function() {
                    $log.debug('deferedUpdate ' + JSON.stringify(d));
                    defer = false;
                    updateFunc();
                    d = undefined;
                };
                var cancelDefer = function() {defer = false;};
                if (!defer) {
                    $log.debug('doUpdate');
                    updateFunc();
                    defer = true;
                    $timeout(cancelDefer, deferredTime, false);
                }
                else {
                    $log.debug('defering update');
                    if (!d) {
                        d = $timeout(deferedUpdate, deferredTime, true);
                    }
                }
            };
            return doUpdate;
        };

        return {
            doDeffered : _doDeffered
        };
    }

})();