(function(){
    'use strict';

    angular
    .module('ccManager')
    .factory('AgentViewColBuilder',AgentViewColBuilder);

    AgentViewColBuilder.$inject= ['ViewColBuilder','XucAgent', 'AgentStates','$compile'];

    function AgentViewColBuilder(viewColBuilder, xucAgent, agentStates, $compile) {
        var _buildCols = function() {
            viewColBuilder.setPrefix('COL_AGENT');
            var availCols = [];
            availCols.push({id:'emptyCol', show: true, showTotal: false});
            availCols.push({id:'editCol', type:'edit',   cellClass:"actionIcon", show: true, showTotal: true});

            availCols.push(viewColBuilder.col('number').withFieldClass('badge').withFieldDynClass('state').build());
            availCols.push(viewColBuilder.col('firstName').build());
            availCols.push(viewColBuilder.col('lastName').build());
            availCols.push(viewColBuilder.col('stats.LoginDateTime').withDisplayFilter('dateTime').withNoFilter().build());
            availCols.push(viewColBuilder.col('stats.LogoutDateTime').withDisplayFilter('dateTime').withNoFilter().build());
            availCols.push({id:'actionCol', type:'action', cellClass:"actionIcon", show: true, showTotal: true});
            var stateCol = viewColBuilder.col('stateName').withCellClass('T{{agent.state}}').withFilter('dropdown-multi-select')
                                                       .withDisplayFilter('translate').withFieldDynClass('state').withFilterData(agentStates.buildStates).build();
            stateCol.fieldClass = 'state';
            availCols.push(stateCol);
            var statusCol =  viewColBuilder.col('status').build();
            statusCol.show = false;
            availCols.push(statusCol);
            availCols.push(viewColBuilder.col('timeInState').withNoFilter().withDisplayFilter('timeInState').build());
            availCols.push(viewColBuilder.col('phoneNb').build());
            availCols.push(viewColBuilder.col('stats.PausedTime').withDisplayFilter('totalTime').withNoFilter().build());

            var pauseStatuses = xucAgent.getNotReadyStatuses();
            angular.forEach(pauseStatuses, function(status){
                availCols.push(viewColBuilder.col('stats.'+status.name).withDisplayFilter('totalTime').withNoFilter().withTitle(status.longName).build());
            });
            availCols.push(viewColBuilder.col('stats.WrapupTime').withDisplayFilter('totalTime').withNoFilter().build());

            availCols.push(viewColBuilder.col('stats.InbCalls').withNoFilter().build());
            availCols.push(viewColBuilder.col('stats.InbAnsCalls').withNoFilter().build());
            availCols.push(viewColBuilder.col('stats.InbAverCallTime').withDisplayFilter('totalTime').withNoFilter().build());
            availCols.push(viewColBuilder.col('stats.InbCallTime').withDisplayFilter('totalTime').withNoFilter().build());
            availCols.push(viewColBuilder.col('stats.InbUnansCalls').withNoFilter().build());
            availCols.push(viewColBuilder.col('stats.InbPercUnansCalls').withNoFilter().withDisplayFilter('number:1').build());
            availCols.push(viewColBuilder.col('stats.OutCalls').withNoFilter().build());
            availCols.push(viewColBuilder.col('stats.OutCallTime').withDisplayFilter('totalTime').withNoFilter().build());

            return availCols;
        };

        var _buildTable = function($scope) {
                var colType  = function(type) {
                    return " ng-if=col.type==='"+type+"' ";
                };
                var actionItem = function(canFn, actionFn, icon, label) {
                    return '<li ng-show="'+canFn+'(agent.id)" ng-click="'+actionFn+'(agent.id)" role="presentation" class="AgentMenu">' +
                            '<i class="glyphicon glyphicon-'+icon+' glyphmenu"></i>{{"'+label+'" | translate}}</li>';
                };

                var colAction ='<div '+ colType('action') + 'ng-controller="agentActionController" class="dropdown dropdown-menu-right">' +
                                        '<a data-toggle="dropdown" href="#">' +
                                        '<i class="glyphicon glyphicon-wrench"></i>' +
                                        '</a>' +
                                        '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">' +
                                        actionItem("canLogIn", "login", "log-in","ACTION_LOGIN") +
                                        actionItem("canLogOut", "logout", "log-out","ACTION_LOGOUT") +
                                        actionItem("canPause", "pause", "pause","ACTION_PAUSE") +
                                        actionItem("canUnPause", "unpause", "play","ACTION_UNPAUSE") +
                                        actionItem("canListen", "listen", "bullhorn","ACTION_LISTEN") +
                                    '</div>';


                var colEdit = '<span ' + colType('edit') + 'ng-controller="agentEditController" class="glyphicon glyphicon-pencil" ng-click="editAgent(agent.id)"></span>';
                var colField = '<span ' + colType('field') + 'class="{{col.fieldClass}} T{{agent[col.fieldDynClass]}}" ng-bind="agent[col.field] | editableFilter:col.displayFilter"></span>';
                var colStat  = '<span ' + colType('stat') + 'class="{{col.fieldClass}} T{{agent[col.fieldDynClass]}}" ng-bind="agent.stats[col.field]  | editableFilter:col.displayFilter"></span>';
                var agentField =  colEdit + colField + colStat + colAction;

                    var agentViewTableWithGroup = '<table ng-table-dynamic="agentDynTable with cols" cols="cols" show-filter="true" class="table table table-condensed">' +
                            '<tbody ng-repeat="group in $groups | orderBy:'+"'"+'value'+"'"+'" >' +
                                '<tr class="ng-table-group agent-table-group-data">' +
                                    '<td>' +
                                        '<a href="" ng-click="group.$hideRows = !group.$hideRows">' +
                                            '<span class="glyphicon" ng-class="{ '+"'glyphicon-chevron-right'"+': group.$hideRows, '+"'glyphicon-chevron-down'"+': !group.$hideRows }"></span>' +
                                            '<strong> {{ group.value }} ({{group.data.length}})</strong>' +
                                        '</a>' +
                                    '</td>' +
                                    '<td ng-if="(col.showTotal && col.show)" ng-repeat="col in cols">{{totals[group.value][col.field] | editableFilter:col.displayFilter}}</td>' +
                                '</tr>' +
                                '<tr ng-if="!group.$hideRows" ng-repeat="agent in group.data">' +
                                    '<td class="{{col.cellClass}} T{{agent[col.fieldClass]}}" data-header-class="'+"'tableTitle'"+'" ng-repeat="col in $columns">'+
                                       agentField +
                                    '</td>' +
                                '</tr>' +
                            '</tbody>' +
                        '</table>';


                    var agentViewTable = '<table ng-table-dynamic="agentDynTable with cols" show-filter="true" class="table table-condensed">' +
                                            '<tr ng-repeat="agent in $data">' +
                                                 '<td class="{{col.cellClass}} T{{agent[col.fieldClass]}}" data-header-class="" ng-repeat="col in $columns">' +
                                                    agentField +
                                                 '</td>' +
                                            '</tr>' +
                                          '</table>';

                var newDirective = angular.element($scope.showGroup ? agentViewTableWithGroup : agentViewTable);
                var element = angular.element( document.querySelector( '#agtView' ) );
                element.empty();
                element.append(newDirective);
                $compile(newDirective)($scope);
        };
        return {
            buildCols: _buildCols,
            buildTable: _buildTable
        };
    }
})();