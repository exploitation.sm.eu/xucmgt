(function(){
    'use strict';

    angular
    .module('ccManager')
    .factory('ViewColBuilder',ViewColBuilder);

    ViewColBuilder.$inject= ['$translate'];

    function ViewColBuilder($translate){
        var translatePrefix = 'COL';

        var _setPrefix= function(prefix) {
            translatePrefix = prefix;
        };

        var _buildCol = function(name) {
            this.field = {};
            var fieldName = name.split(".").length > 1 ? name.split(".")[1] : name.split(".")[0];
            var fieldType = name.split(".").length > 1 ? 'stat' : 'field';
            var f = {};
            f[name] = 'text';

            this.field.id = fieldName+'Col';
            this.field.ref = fieldName+'Col';
            this.field.type = fieldType;
            this.field['class'] = 'tableTitle';
            this.field.title = $translate.instant(translatePrefix+'_'+ fieldName.toUpperCase());
            this.field.sortable = name;
            this.field.show = true;
            this.field.field = fieldName;
            this.field.fieldClass = fieldName;
            this.field.filter = f;
            this.field.showTotal = true;

            this.withFilter = function(ftype) {
                var f = {};
                f[name] = ftype;
                this.field.filter = f;
                return this;
            };
            this.withDisplayFilter = function(df) {
                this.field.displayFilter = df;
                return this;
            };
            this.withFieldClass = function(cl) {
                this.field.fieldClass = cl;
                return this;
            };
            this.withFieldDynClass = function(cl) {
                this.field.fieldDynClass = cl;
                return this;
            };
            this.withNoFilter = function() {
                this.field.filter = undefined;
                return this;
            };
            this.hide = function() {
                this.field.show = false;
                return this;
            };
            this.withTitle = function(t) {
                this.field.title = t;
                return this;
            };
            this.withCellClass = function(cc) {
                this.field.cellClass = cc;
                return this;
            };
            this.withFilterData = function(fd) {
                this.field.filterData = fd;
                return this;
            };
            this.build = function() {
                return this.field;
            };
            return this;
        };

        return {
            col: _buildCol,
            setPrefix: _setPrefix
        };
    }

})();