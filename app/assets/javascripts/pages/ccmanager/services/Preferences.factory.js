(function() {
    'use strict';

    angular.module('ccManager').factory('Preferences', Preferences);

    Preferences.$inject = [ 'localStorageService', '$rootScope','XucQueue' ];

    function Preferences(localStorageService, $rootScope,xucQueue) {

        var QueueSelectedKey = 'queueSelected';
        var AgentViewColumnsKey = 'agentViewColumns';
        var QueueViewColumnsKey = 'queueViewColumns';

        var queueIds = [];

        var _init = function() {
            queueIds = localStorageService.get(QueueSelectedKey) || [];
        };

        var _reset = function() {
            queueIds = [];
        };

        var _getSelectedQueues = function() {
            queueIds = localStorageService.get(QueueSelectedKey) || [];
            return queueIds;
        };

        var _setQueueVisibility = function(queueName,visibility) {
            var queueId=-1;
            _.each(xucQueue.getQueues(),function(queue) {
                if(queue.name.toLowerCase()==queueName.toLowerCase()){
                    queueId = queue.id;
                }
            });

            if(visibility){
                if (queueIds.indexOf(queueId) ==-1) {
                    queueIds.push(queueId);
                }
            }else{
                if (queueIds.indexOf(queueId) >-1) {
                    queueIds.splice(queueIds.indexOf(queueId), 1);
                }
            }
            localStorageService.set(QueueSelectedKey, queueIds);
        };

        var _setQueuesVisibility = function(queueNames,visibility) {
            _reset();
            _.each(queueNames.split(','),function(v){
                _setQueueVisibility(v,visibility);
            });
        };

        var _toggleQueueSelection = function(queueId) {
            if (queueIds.indexOf(queueId) >= 0) {
                queueIds.splice(queueIds.indexOf(queueId), 1);
            } else {
                queueIds.push(queueId);
            }
            localStorageService.set(QueueSelectedKey, queueIds);
        };

        var _isQueueSelected = function(queueId) {
            return queueIds.indexOf(queueId) >= 0;
        };

        var _noQueueSelected = function() {
            return queueIds.length === 0;
        };

        var _getViewColumns = function(availColumns, key) {
            var updatedCols = [];
            var aCols = angular.copy(availColumns);
            var savedCols = localStorageService.get(key) || [];

            angular.forEach(savedCols, function(sCol) {
                angular.forEach(aCols, function(aCol, key) {
                    if (aCol.id === sCol.id) {
                        aCol.show = sCol.show;
                        updatedCols.push(aCols.splice(key, 1)[0]);
                    }
                });

            });

            return updatedCols.concat(aCols);
        };

        var _saveViewColumns = function(cols, key) {
            localStorageService.set(key, cols);
        };

        var _moveCol = function(fromCol, targetCol, tableCol, colPrefKey) {
            var fromIdx = -1;
            var targetIdx = -1;

            var _moveToLeft = function(fromIdx, column, targetCol) {
                var colsToSave = [];
                angular.forEach(tableCol, function(col, key) {
                    if (col.id === targetCol) {
                        colsToSave.push(tableCol[fromIdx]);
                    }
                    if (col.id !== column) {
                        colsToSave.push(col);
                    }
                });
                _saveViewColumns(colsToSave, colPrefKey);
            };
            var _moveToRight = function(fromIdx, column, targetCol) {
                var colsToSave = [];
                angular.forEach(tableCol, function(col, key) {
                    if (col.id !== column) {
                        colsToSave.push(col);
                    }
                    if (col.id === targetCol) {
                        colsToSave.push(tableCol[fromIdx]);
                    }
                });
                _saveViewColumns(colsToSave, colPrefKey);
            };
            angular.forEach(tableCol, function(col, key) {
                if (col.id === fromCol)
                    fromIdx = key;
                if (col.id === targetCol)
                    targetIdx = key;
            });
            if (targetIdx >= 0 && fromIdx >= 0) {
                if (fromIdx > targetIdx)
                    _moveToLeft(fromIdx, fromCol, targetCol);
                else
                    _moveToRight(fromIdx, fromCol, targetCol);
            }

        };
        var _getOption = function(key, defaultValue) {
            var savedOption = localStorageService.get(key) || defaultValue;
            return savedOption;
        };
        var _setOption = function(key, value) {
            localStorageService.set(key, value);
        };

        $rootScope.$on('LocalStorageModule.notification.setitem', function(event, args) {
            $rootScope.$broadcast('preferences.' + args.key);
        });

        return {
            init : _init,
            reset : _reset,
            getSelectedQueues : _getSelectedQueues,
            toggleQueueSelection : _toggleQueueSelection,
            setQueueVisibility : _setQueueVisibility,
            setQueuesVisibility : _setQueuesVisibility,
            isQueueSelected : _isQueueSelected,
            noQueueSelected : _noQueueSelected,
            saveViewColumns : _saveViewColumns,
            getViewColumns : _getViewColumns,
            getOption : _getOption,
            setOption : _setOption,
            moveCol : _moveCol
        };
    }

})();