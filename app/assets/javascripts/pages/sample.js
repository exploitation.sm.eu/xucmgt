$(function() {
    console.log("sample handlers");
    var agentStateEventHandler = function(agentState) {
        $('#agent_events').prepend(
                '<li class="list-group-item">' + new Date().toLocaleString() + ": " + JSON.stringify(agentState) + '</li>');
        $('#xuc_agentData').val("cause :  " + agentState.cause);
        if (agentState.name !== "AgentLoggedOut") {
            loginHandler(agentState);
        }
        switch (agentState.name) {
        case "AgentReady":
            readyHandler();
            break;
        case "AgentOnPause":
            onPauseHandler();
            break;
        case "AgentOnCall":
            onCallHandler(agentState);
            break;
        case "AgentOnWrapup":
            onWrapupHandler();
            break;
        case "AgentLoggedOut":
            logoutHandler();
            break;
        }
    };
    var loginHandler = function(event) {
        $('#xuc_agentStatus').val("Logged in");
        $('#xuc_agentPhoneNumber').val(event.phoneNb);
    };

    var logoutHandler = function() {
        $('#xuc_agentStatus').val("Logged out");
    };

    var readyHandler = function() {
        $('#xuc_agentStatus').val("Ready");
    };
    var onPauseHandler = function() {
        $('#xuc_agentStatus').val("Paused");
    };
    var onCallHandler = function(state) {
        $('#xuc_agentStatus').val("On Call");
        $('#xuc_agentData').val("Acd : " + state.acd + " dir : " + state.direction + " Type : " + state.callType);
        $('#xuc_agentMonitorState').val(state.monitorState);
    };
    var onWrapupHandler = function() {
        $('#xuc_agentStatus').val("Wrapup");
    };
    var usersStatusesHandler = function(statuses) {
        $.each(statuses, function(key, item) {
            $("#userPresence")
                    .append("<li><a href=\"#\" id=\"" + item.name + "\">" + item.longName + "</a></li>");
        });
        $('.dropdown-menu li a').click(function(event) {
            Cti.changeUserStatus($(this).attr('id'));
        });
    };

    var userStatusHandler = function(event) {
        console.log("user Status Handler " + JSON.stringify(event));
        $('#xuc_userStatus').val(event.status);
    };

    var userConfigHandler = function(event) {
        //{"userId":1,
        //"dndEnabled":false,"naFwdEnabled":false,"naFwdDestination":"8812",
        //"uncFwdEnabled":true,"uncFwdDestination":"1102","busyFwdEnabled":false,"busyFwdDestination":"5546",
        //"firstName":"Brucé","lastName":"Wail","fullName":"Brucé Wail","mobileNumber":"","agentId":19,"lineIds":[1]}}
        console.log("user config Handler " + JSON.stringify(event));
        $('#xuc_userId').text(event.userId);
        $('#xuc_fullName').text(event.fullName);
        $('#xuc_mobileNumber').text(event.mobileNumber);
        $('#xuc_agentId').text(event.agentId);
        $('#xuc_voiceMailId').text(event.voiceMailId);
        $('#xuc_voiceMailEnabled').text(event.voiceMailEnabled);
        if (event.naFwdDestination !== null) {
            $('#xuc_naFwdEnabled').prop('checked', event.naFwdEnabled);
            $('#xuc_naFwdDestination').val(event.naFwdDestination);
        }
        if (event.uncFwdDestination !== null) {
            $('#xuc_uncFwdEnabled').prop('checked', event.uncFwdEnabled);
            $('#xuc_uncFwdDestination').val(event.uncFwdDestination);
        }

        if (event.busyFwdDestination !== null) {
            console.log("busy fwd " + event.busyFwdEnabled);
            $('#xuc_busyFwdEnabled').prop('checked', event.busyFwdEnabled);
            $('#xuc_busyFwdDestination').val(event.busyFwdDestination);
        }
    };
    var phoneStatusHandler = function(event) {
        console.log("phone Status Handler " + JSON.stringify(event));
        $('#xuc_phoneStatus').val(event.status);
    };

    var voiceMailStatusHandler = function(event) {
        $('#xuc_newMessages').text(event.newMessages);
        $('#xuc_waitingMessages').text(event.waitingMessages);
        $('#xuc_oldMessages').text(event.oldMessages);
    };

    var linkStatusHandler = function(event) {
        console.log("link Status Handler " + JSON.stringify(event));
        if (event.status === "closed") {
            $('#xuc_logon_panel').show();
        }
    };

    var loggedOnHandler = function() {
        console.log("Logged On Handler ");
        $('#xuc_logon_panel').hide();
    };

    var queueStatisticsHandler = function(event) {
        console.log("queue statistics Handler " + JSON.stringify(event));
        $('#queue_stats').prepend(
                '<li class="list-group-item">' + new Date().toLocaleString() + ": " + JSON.stringify(event) + '</li>');
    };
    var queueConfigHandler = function(queueConfig) {
        $('#queue_config').prepend('<li class="list-group-item">' + JSON.stringify(queueConfig) + '</li>');
    };
    var queueMemberHandler = function(queueMember) {
        $('#queue_member').prepend('<li class="list-group-item">' + JSON.stringify(queueMember) + '</li>');
    };
    var directoryResultHandler = function(directoryResult) {
        console.log("Directory result" + JSON.stringify(directoryResult));
        DirectoryDisplay.prepareTable();
        DirectoryDisplay.displayHeaders(directoryResult.headers);
        DirectoryDisplay.displayEntries(directoryResult.entries);
    };

    var agentConfigHandler = function(agentConfig) {
        console.log("agent config Handler " + JSON.stringify(agentConfig));
        $('#agent_config').prepend('<li class="list-group-item">' + JSON.stringify(agentConfig) + '</li>');
    };
    var agentGroupConfigHandler = function(agentGroups) {
        console.log("agent group config Handler " + JSON.stringify(agentGroups));
        $('#agentgroup_config').prepend('<li class="list-group-item">' + JSON.stringify(agentGroups) + '</li>');
    };

    var agentErrorHandler = function(error) {
        console.log("agent error Handler " + JSON.stringify(error));
        $('#agent_error').html(
                '<p class="text-danger">' + new Date().toLocaleString() + ": " + JSON.stringify(error) + '</p>');
    };
    var errorHandler = function(error) {
        console.log("error Handler " + JSON.stringify(error));
        $('#agent_error').html(
                '<p class="text-danger">' + new Date().toLocaleString() + ": " + JSON.stringify(error) + '</p>');
    };
    var agentDirectoryHandler = function(agentDirectory) {
        console.log("agent directory Handler " + JSON.stringify(agentDirectory));
        $('#agent_directory').prepend('<li class="list-group-item">' + JSON.stringify(agentDirectory) + '</li>');
    };

    var conferencesHandler = function(conferences) {
        console.log("conferences Handler " + JSON.stringify(conferences));
        $('#conferences').prepend('<li class="list-group-item">' + JSON.stringify(conferences) + '</li>');
    };
    var agentListenHandler = function(listenEvent) {
        $('#xuc_agentListenStatus').val(JSON.stringify(listenEvent));
    };
    var xucserver = window.location.hostname+":"+window.location.port;

    $('#xuc_server').val(xucserver);

    $('#xuc_sign_in').click(
            function(event) {
                var server = $('#xuc_server').val();
                var username = $('#xuc_username').val();
                var password = $('#xuc_password').val();
                var phoneNumber = $('#xuc_phoneNumber').val();
                console.log("sign in " + server + " : " + username + " " + password + " " + phoneNumber);
                var wsurl = "ws://" + server + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
                        phoneNumber + "&amp;password=" + password;
                Cti.debugMsg = true;
                Cti.debugHandler = false;
                Cti.setHandler(Cti.MessageType.USERSTATUSES, usersStatusesHandler);
                Cti.setHandler(Cti.MessageType.USERSTATUSUPDATE, userStatusHandler);
                Cti.setHandler(Cti.MessageType.USERCONFIGUPDATE, userConfigHandler);
                Cti.setHandler(Cti.MessageType.LOGGEDON, loggedOnHandler);
                Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, phoneStatusHandler);
                Cti.setHandler(Cti.MessageType.VOICEMAILSTATUSUPDATE, voiceMailStatusHandler);
                Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, linkStatusHandler);
                Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, queueStatisticsHandler);
                Cti.setHandler(Cti.MessageType.QUEUECONFIG, queueConfigHandler);
                Cti.setHandler(Cti.MessageType.QUEUELIST, queueConfigHandler);
                Cti.setHandler(Cti.MessageType.QUEUEMEMBER, queueMemberHandler);
                Cti.setHandler(Cti.MessageType.QUEUEMEMBERLIST, queueMemberHandler);
                Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, directoryResultHandler);

                Cti.setHandler(Cti.MessageType.AGENTCONFIG, agentConfigHandler);
                Cti.setHandler(Cti.MessageType.AGENTLIST, agentConfigHandler);
                Cti.setHandler(Cti.MessageType.AGENTLISTEN, agentListenHandler);
                Cti.setHandler(Cti.MessageType.AGENTGROUPLIST, agentGroupConfigHandler);
                Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, agentStateEventHandler);
                Cti.setHandler(Cti.MessageType.AGENTERROR, agentErrorHandler);
                Cti.setHandler(Cti.MessageType.ERROR, errorHandler);
                Cti.setHandler(Cti.MessageType.AGENTDIRECTORY, agentDirectoryHandler);

                Cti.setHandler(Cti.MessageType.CONFERENCES, conferencesHandler);

                DirectoryDisplay.init("#directoryresult");
                Cti.WebSocket.init(wsurl, username, phoneNumber);
            });
    $('#xuc_restart').click( function(event) {
        Cti.close();
        var server = $('#xuc_server').val();
        var username = $('#xuc_username').val();
        var password = $('#xuc_password').val();
        var phoneNumber = $('#xuc_phoneNumber').val();
        var wsurl = "ws://" + server + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
                        phoneNumber + "&amp;password=" + password;
        Cti.WebSocket.init(wsurl, username, phoneNumber);

    });
    $('#xuc_login_btn').click(function(event) {
        Cti.loginAgent($('#xuc_agentPhoneNumber').val());
    });
    $('#xuc_logout_btn').click(function(event) {
        Cti.logoutAgent();
    });
    $('#xuc_pause_btn').click(function(event) {
        Cti.pauseAgent();
    });
    $('#xuc_unpause_btn').click(function(event) {
        Cti.unpauseAgent();
    });
    $('#xuc_subscribe_to_queue_stats_btn').click(function(event) {
        Cti.subscribeToQueueStats();
    });
    $('#xuc_answer_btn').click(function(event) {
        Cti.answer();
    });
    $('#xuc_hangup_btn').click(function(event) {
        Cti.hangup();
    });
    $('#xuc_search_btn').click(function(event) {
        Cti.searchDirectory($("#xuc_destination").val());
    });
    $('#xuc_enable_dnd_btn').click(function(event) {
        Cti.dnd(true);
    });
    $('#xuc_disable_dnd_btn').click(function(event) {
        Cti.dnd(false);
    });
    $('#xuc_dial_btn').click(function(event) {
        Cti.dial($("#xuc_destination").val());
    });
    $('#xuc_direct_transfer_btn').click(function(event) {
        Cti.directTransfer($("#xuc_destination").val());
    });
    $('#xuc_attended_transfer_btn').click(function(event) {
        Cti.attendedTransfer($("#xuc_destination").val());
    });
    $('#xuc_complete_transfer_btn').click(function(event) {
        Cti.completeTransfer();
    });
    $('#xuc_cancel_transfer_btn').click(function(event) {
        Cti.cancelTransfer();
    });
    $('#xuc_monitorpause_btn').click(function(event) {
        Cti.monitorPause(parseInt($('#xuc_agentId').text()));
    });
    $('#xuc_monitorunpause_btn').click(function(event) {
        Cti.monitorUnpause(parseInt($('#xuc_agentId').text()));
    });
    $('#xuc_get_config_queues').click(function(event) {
        $('#queue_config').html("");
        Cti.getList("queue");
    });
    $('#xuc_get_config_queuemembers').click(function(event) {
        Cti.getList("queuemember");
    });
    $('#xuc_get_config_agents').click(function(event) {
        Cti.getList("agent");
    });
    $('#xuc_get_config_agentgroups').click(function(event) {
        Cti.getList("agentgroup");
    });
    $('#xuc_get_agentstates').click(function(event) {
        Cti.getAgentStates();
    });
    $('#xuc_clean_queue_stats').click(function(event) {
         $('#queue_stats').empty();
    });
    $('#xuc_clean_agent_events').click(function(event) {
         $('#agent_events').empty();
         $('#agentstates').empty();
    });
    $('#xuc_clean_queue_config').click(function(event) {
         $('#queue_config').empty();
    });

    $('#xuc_clean_queue_member').click(function(event) {
         $('#queue_member').empty();
    });
    $('#xuc_clean_agent_config').click(function(event) {
         $('#agent_config').empty();
    });
    $('#xuc_clean_agent_directory').click(function(event) {
         $('#agent_directory').empty();
    });
    $('#xuc_clean_agentgroup_config').click(function(event) {
         $('#agentgroup_config').empty();
    });
    $('#xuc_clean_conferences').click(function(event) {
         $('#conferences').empty();
    });

    $('#xuc_subscribe_to_agent_event_btn').click(function(event) {
        Cti.subscribeToAgentEvents();
    });
    $('#xuc_get_agent_directory').click(function(event) {
        Cti.getAgentDirectory();
    });
    $('#xuc_set_agent_queue_btn').click(function(event) {
        Cti.setAgentQueue($("#xuc_agentId_target").val(), $("#xuc_queueId").val(), $("#xuc_penalty").val());
    });
    $('#xuc_remove_agent_from_queue_btn').click(function(event) {
        Cti.removeAgentFromQueue($("#xuc_agentId_target").val(),$("#xuc_queueId").val());
    });
    $('#xuc_listen_agent_btn').click(function(event) {
        Cti.listenAgent(parseInt($("#xuc_agentId_target").val()));
    });
    $('#xuc_get_config_conferences').click(function(event) {
        Cti.getConferenceRooms();
    });
    $('#xuc_invite_conf_room').click(function(event) {
        var userId = $('#xuc_confUserId').val();
        console.log("User ID vaut " + userId);
        Cti.inviteConferenceRoom(parseInt(userId));
    });
    $('#xuc_naFwdEnabled').click(function() {
        console.log($(this).is(':checked'));
        Cti.naFwd($("#xuc_naFwdDestination").val(),$(this).is(':checked'));
        $(this).prop('checked', !($(this).is(':checked')));
    });
    $('#xuc_uncFwdEnabled').click(function() {
        console.log($(this).is(':checked'));
        Cti.uncFwd($("#xuc_uncFwdDestination").val(),$(this).is(':checked'));
        $(this).prop('checked', !($(this).is(':checked')));
    });
    $('#xuc_busyFwdEnabled').click(function() {
        console.log($(this).is(':checked'));
        Cti.busyFwd($("#xuc_busyFwdDestination").val(),$(this).is(':checked'));
        $(this).prop('checked', !($(this).is(':checked')));
    });
});
