var pratixModule = angular.module('Pratix', ['xuc.services','ui.bootstrap','pascalprecht.translate','ui.router']);

pratixModule.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider, $translateProvider) {

    $stateProvider
        .state('login', {
            url: '/login?error',
            templateUrl: '/pratix/login.html',
            controller: 'LoginController',
            data: {
                requireLogin: false
            }
        })
        .state('interface', {
            url: '/pratix',
            templateUrl: '/pratix/main.html',
            controller: 'InitController',
            data: {
                requireLogin: true
            }
            })
        .state('interface.contacts', {
            url: '/contacts?showFavorites&search',
            templateUrl: '/pratix/contacts.html',
            controller: "ContactsController"
        })
        .state('interface.conferences', {
            url: '/conferences',
            templateUrl: '/pratix/conferences.html',
            controller: "ConfRoomController"
        })
        .state('interface.history', {
            url: '/history',
            templateUrl: '/pratix/history.html',
            controller: "HistoryController"
        });

    $urlRouterProvider.otherwise('/login');

}]);

pratixModule.config(['$translateProvider', function($translateProvider) {

    $translateProvider.translations('fr', {
        FORWARDED : 'En renvoi',
        FORWARD_LABEL : 'Renvoi /',
        CLICK_TO_CHANGE : 'Cliquer pour changer',
        NO_ANSWER_ABREV : 'NR',
        BUSY_ABREV : 'OCC'
    });
    $translateProvider.translations('en', {
        FORWARDED : 'Forwarded',
        FORWARD_LABEL : 'Forwarded /',
        CLICK_TO_CHANGE : 'Click to change',
        NO_ANSWER_ABREV : 'NA',
        BUSY_ABREV : 'BUSY'
    });
    $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
           'en_*': 'en',
           'fr_*': 'fr'
    });
    $translateProvider.determinePreferredLanguage();
    $translateProvider.fallbackLanguage(['fr']);
}]);

var normalize = function(nb) {
    nb = ""+nb;
    nb = nb.replace(/^\+/,"00");
    return nb.replace(/[ ()\.]/g,"");
};

pratixModule.run(function ($rootScope, $state, XucLink, XucUser) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var requireLogin = toState.data.requireLogin;

        if (requireLogin && ! XucLink.isLogged()) {
            console.log("Login required");
            $state.go('login');
            event.preventDefault();
        }
    });
});

pratixModule.controller('LoginController', ['$scope', '$state', '$stateParams', '$window', function($scope, $state, $stateParams, $window) {

    $scope.error = $stateParams.error;

    $scope.loginAndRedirect = function(event, args) {
        if(event.name == "ctiLoggedOn") {
            $scope.error = '';
            $scope.username = "";
            $scope.password = "";
            $scope.$apply();
            $state.go('interface.contacts', {'showFavorites': true});
        } else {
            $('#loginError').show();
        }
    };

    $scope.login = function() {
        console.log("logging on");
        $state.go('login');
        $("#loginError").hide();

        Cti.debugMsg = true;
        var xucLink = angular.element(document.body).injector().get("XucLink");
        xucLink.setHostAndPort($("#hostAndPort")[0].value);
        if($('#token').length > 0)
            xucLink.setToken($('#token')[0].value);
        xucLink.setRedirectToHomeUrl(false);
        xucLink.initCti($scope.username,$scope.password,0);
        $scope.$on('ctiLoggedOn', $scope.loginAndRedirect);

    };

    $("#loginError").hide();

    if($window.location.search.indexOf('token') > -1)
        $scope.login();
}]);

pratixModule.controller('InitController', ['$scope', '$rootScope', '$state', '$window', function($scope, $rootScope, $state, $window) {
    $("#numberTodial").keypress(function(event) {
        if ( event.which == $.ui.keyCode.ENTER ) {
            event.preventDefault();
            var toBeDialed = normalize($("#numberTodial").val());
            $("#numberTodial").val(toBeDialed);
            if ($.isNumeric(toBeDialed)) {
                Cti.dial(toBeDialed);
            }
            else {
                $state.go('interface.contacts', {'showFavorites': false, 'search': toBeDialed});
            }
        }
    });

    $('#xuc_search').click(function(event){
        $state.go('interface.contacts', {'showFavorites': false, 'search': $("#numberTodial").val()});
    });

    $scope.$on('linkDisConnected', function() {
        $window.location.search = '';
        $state.go('login', {'error': 'Link closed'});
    });

}]);

pratixModule.controller('VoiceMailController',['$scope','XucVoiceMail',function($scope, XucVoiceMail){
    $scope.voiceMail = XucVoiceMail.getVoiceMail();

    $scope.$on('voicemailUpdated', function(e) {
        $scope.voiceMail = XucVoiceMail.getVoiceMail();
        $scope.$digest();
    });

}]);

pratixModule.controller('ConfRoomController',['$scope','XucConfRoom',function($scope, XucConfRoom){
    $scope.confRooms = [];

    $scope.$on('ConfRoomsUpdated', function(e) {
        $scope.confRooms = XucConfRoom.getConferenceRooms();
        console.log('on ConfRoomsUpdated ' + JSON.stringify($scope.confRooms));
        $scope.$digest();
    });

    $scope.getStartTime = function(confRoom) {
        console.log(confRoom.members.length);
        if(confRoom.members.length > 0) {
            return moment(confRoom.startTime).format('HH:mm:ss');
        }
        else {
            return '-';
        }
    };

    $scope.init = function() {
        $scope.confRooms = XucConfRoom.requestConferenceRooms();
    };

    $scope.init();
}]);

pratixModule.controller('HistoryController', function($scope){
    $scope.history = [];

    $scope.statusImage = function(status) {
        var baseUrl = '/assets/images/';
        if(status == 'emitted') return baseUrl + 'emitted-call.svg';
        else if(status == 'answered') return baseUrl + 'answered-call.svg';
        else if(status == 'missed') return baseUrl + 'missed-call.svg';
        return '';
    };

    $scope.getCallHistory = function() {
        Cti.getUserCallHistory(10);
    };

    $scope.processCallHistory = function(history) {
        $scope.history = history;
        for(var i=0; i<$scope.history.length; i++) {
            var callDetail = $scope.history[i];
            if(callDetail.status == 'emitted')
                callDetail.number = callDetail.dstNum;
            else
                callDetail.number = callDetail.srcNum;
        }
        $scope.$apply();
    };

    $scope.processPhoneStatusUpdate = function(statusUpdate) {
        if(statusUpdate.status == 'AVAILABLE') $scope.getCallHistory();
    };

    $scope.dial = function(number) {
        Cti.dial(number);
    };

    $scope.init = function() {
        Cti.setHandler(Cti.MessageType.CALLHISTORY, $scope.processCallHistory);
        Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, $scope.processPhoneStatusUpdate);
        $scope.getCallHistory();
    };

    $scope.init();
});

pratixModule.controller('UserController',['$scope','XucUser','$translate',function($scope, xucUser,$translate){
    $scope.user = xucUser.getUser();
    $scope.forwarded_title = "";
    $translate('FORWARDED').then(function(transl){
        $scope.forwarded_title = transl;
    });
    $translate('FORWARD_LABEL').then(function(transl){
        $scope.forwarded_label = transl;
    });
    $translate('CLICK_TO_CHANGE').then(function(transl){
        $scope.click_to_change = transl;
    });
    $translate('NO_ANSWER_ABREV').then(function(transl){
        $scope.na_abrev = transl;
    });
    $translate('BUSY_ABREV').then(function(transl){
        $scope.busy_abrev = transl;
    });

    var buildForwardLabel = function(naDest, busyDest) {
            var fwdLabel =  $scope.forwarded_label;
            if ($scope.user.naFwdEnabled) {
                fwdLabel = fwdLabel + ' ' + $scope.na_abrev + ' ' + naDest;
            }
            if ($scope.user.busyFwdEnabled) {
                fwdLabel = fwdLabel + ' ' + $scope.busy_abrev + ' ' +  busyDest;
            }
            return fwdLabel;
    };

    $scope.forwardName = function() {
        if($scope.user.uncFwdEnabled) return($scope.forwarded_title);
        if($scope.user.naFwdEnabled || $scope.user.busyFwdEnabled) {
            return buildForwardLabel("","");
        }
        return (' ');
    };
    $scope.forwardPopover = function() {
        if($scope.user.uncFwdEnabled) return( $scope.forwarded_title+ ' ' + $scope.user.uncFwdDestination);
        if($scope.user.naFwdEnabled || $scope.user.busyFwdEnabled) {
            return buildForwardLabel($scope.user.naFwdDestination,$scope.user.busyFwdDestination);
        }
        return ($scope.click_to_change);
    };

    $scope.isForwarded = function() {
        return  $scope.user.naFwdEnabled || $scope.user.uncFwdEnabled ||  $scope.user.busyFwdEnabled;
    };

    $scope.$on('userConfigUpdated', function(e) {
        $scope.user = xucUser.getUser();
        $scope.$digest();
    });

}]);
pratixModule.controller('ForwardController',['$scope','$modal',function($scope, $modal){

    var ModalInstanceCtrl = function ($scope, $modalInstance, user) {
        $scope.user = angular.copy(user);

        $scope.updatedUncFw = false;
        $scope.updatedBusyFw = false;
        $scope.updatedNaFw = false;

        $scope.updateUncFwd = function() {
            $scope.updatedUncFw = true;
        };

        $scope.updateBusyFwd = function() {
            $scope.updatedBusyFw = true;
        };

        $scope.updateNaFwd = function() {
            $scope.updatedNaFw = true;
        };

        $scope.changeUncFwd = function(user) {
            console.log(user);
            Cti.uncFwd(user.uncFwdDestination, user.uncFwdEnabled);
            $modalInstance.dismiss('changeUncFwd');
        };
        $scope.changeNaFwd = function(user) {
            Cti.naFwd(user.naFwdDestination, user.naFwdEnabled);
            $modalInstance.dismiss('changeNaFwd');
        };
        $scope.changeBusyFwd = function(user) {
            Cti.busyFwd(user.busyFwdDestination, user.busyFwdEnabled);
            $modalInstance.dismiss('changeBusyFwd');
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.processAndClose = function () {
            $scope.process();
            $scope.close();
        };

        $scope.process = function() {
            if ($scope.updatedBusyFw && $scope.user.busyFwdEnabled) {
                console.log('updBusy');
                $scope.updatedBusyFw = false;
                Cti.busyFwd($scope.user.busyFwdDestination, $scope.user.busyFwdEnabled);
            }
            if ($scope.updatedUncFw && $scope.user.uncFwdEnabled) {
                console.log('updUnc');
                $scope.updatedUncFw = false;
                Cti.uncFwd($scope.user.uncFwdDestination, $scope.user.uncFwdEnabled);
            }
            if ($scope.updatedNaFw && $scope.user.naFwdEnabled) {
                console.log('updNA');
                $scope.updatedNaFw = false;
                Cti.naFwd($scope.user.naFwdDestination, $scope.user.naFwdEnabled);
            }
        };

        $scope.close = function() {
            $modalInstance.dismiss('close');
        };

    };

    $scope.showForward = function() {

        var modalInstance = $modal.open({
          templateUrl: 'forwardDialog.html',
          size: 'sm',
          controller: ModalInstanceCtrl,
          resolve: {
              user: function() {
                  return $scope.user;
              },

          }
        });

        modalInstance.result.then(function () {
        }, function () {
             console.log('Modal dismissed at: ' + new Date());
            }
        );
    };
}]);

pratixModule.controller('ViewController',['$scope','$rootScope','$state',function($scope,$rootScope,$state){

    $scope.isActive = function(viewName) {
        return $state.is(viewName);
    };

}]);
pratixModule.controller('PhoneController', function($rootScope,$scope, $timeout) {

    $scope.dial = function() {
        Cti.dial($scope.destination);
    };
    $scope.attendedTransfer = function() {
        Cti.attendedTransfer($scope.destination);
    };
    $scope.completeTransfer = function() {
        Cti.completeTransfer();
    };
    $scope.cancelTransfer = function() {
        Cti.cancelTransfer();
    };
    $scope.answer = function() {
        Cti.answer();
    };
    $scope.hangup = function() {
        Cti.hangup();
    };
    $scope.conference = function() {
        Cti.conference();
    };
    $scope.hold = function() {
        Cti.hold();
    };
});

pratixModule.controller('ContactsController', function($rootScope, $scope, $timeout, $state, $stateParams, XucDirectory) {

    $scope.$on('searchResultUpdated', function(e) {
        $scope.searchResult = XucDirectory.getSearchResult();
        $scope.headers = XucDirectory.getHeaders().slice(0,5);
        $scope.$apply();
    });

    $scope.$on('favoritesUpdated', function(e) {
        $scope.favorites = XucDirectory.getFavorites();
        $scope.headers = XucDirectory.getHeaders().slice(0,5);
        $scope.$apply();
    });

    $scope.isEmpty = function(field) {
        if (typeof field != 'undefined') {
            if (field.length > 0) {
                return true;
            }
        }
        return false;
    };

    $scope.$on('DirectoryLookUp', function(event, term) {
        $scope.search(term);
    });

    $scope.search = function(term) {
        console.log("searching for: ", term);
        Cti.directoryLookUp(term);
        $scope.showSearch = true;
    };

    $scope.getFavorites = function() {
        Cti.getFavorites();
    };

    $scope.addFavorite = function(contact) {
        Cti.addFavorite(contact.contact_id, contact.source);
    };

    $scope.removeFavorite = function(contact) {
        Cti.removeFavorite(contact.contact_id, contact.source);
    };

    $scope.tel = function(number) {
        console.log("tel: " + number.toString());
        Cti.dial(number.toString());
    };

    $scope.init = function() {
        if ($stateParams.showFavorites === false) {
            $scope.showSearch = true;
        }
        else {
            $scope.getFavorites();
            $scope.showSearch = false;
        }

        if( typeof($stateParams.search) != 'undefined') {
            $scope.search($stateParams.search);
        }
    };

    $scope.init();

});

var initCti = function(username, password, hostAndPort) {
    Cti.debugMsg = false;
    var xucLink = angular.element(document.body).injector().get("XucLink");
    xucLink.setHostAndPort(hostAndPort);
    xucLink.setHomeUrl("/pratix");
    xucLink.initCti(username,password,0);
};


