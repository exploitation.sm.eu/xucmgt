var ccsuperApp = angular.module('ccSupervision', ['xuc.services','ngCookies']);


ccsuperApp.controller('ccQueuesController',
    ['XucQueue','$scope','$timeout','$rootScope','$cookieStore',
    function(xucQueue,$scope, $timeout, $rootScope,$cookieStore) {
    var updateQueueFrequency = 2;

    $scope.queues = xucQueue.getQueues();
    $scope.queuesStats = {};
    $scope.clockFrequency = 5;
    $scope.filterQueues = [];
    $scope.globalFilterQueues = false;
    $scope.cookieQueueName = 'cookieQueue';

    var danger = "nok";
    var warning = "warning";
    var safe = "ok";

    $scope.totaux = {};

    $scope.updateTotaux = function() {
        angular.forEach($scope.totaux, function(value, key) {
            $scope.totaux[key] = 0;
        });

        angular.forEach($scope.queues, function(queueValue, queueKey) {
            angular.forEach(queueValue, function(clientValue, clientKey) {
                $scope.totaux[clientKey] += clientValue;
            });
        });
    };

    $scope.$on('ctiLoggedOn', function(e) {
        xucQueue.start();
    });

    $scope.$on('linkDisConnected', function(e) {
        $scope.connected = false;
        $scope.queues.length = 0;
        $scope.updateTotaux();
    });

    $scope.filterQueue = function(queueId, queueDisplayName) {
        var index = $scope.filterQueues.indexOf(queueId);
        if (index < 0) {
            $scope.filterQueues.push(Number(queueId));
        } else {
            $scope.filterQueues.splice(index, 1);
        }
        $scope.filterAgents();
        $scope.createCookie();
    };

    $scope.filterAllQueues = function() {
        if ($scope.filterQueues.length === 0) {
            angular.forEach($scope.queues, function(queue, keyQueue) {
                $scope.filterQueues.push(queue.id);
            });
        }
        else {
            $scope.filterQueues = [];
        }
        $rootScope.$broadcast('filterQueues', $scope.filterQueues);
        $scope.createCookie();
    };

    $scope.createCookie = function() {
        var expirationDate = new Date();
        expirationDate.setFullYear(expirationDate.getFullYear() + 1);
        document.cookie = $scope.cookieQueueName + "=" + angular.toJson($scope.filterQueues) + "; path=/; expires =" +
                expirationDate.toGMTString();
    };

    $scope.readCookie = function() {
        $scope.filterQueues = $cookieStore.get($scope.cookieQueueName)  || [];
        $scope.$digest();
        $scope.filterAgents();
        return false;
    };

    $scope.isChecked = function(queueId) {
        return  $scope.filterQueues.indexOf(queueId) >= 0;
    };

    $scope.filterAgents = function() {
        $rootScope.$broadcast('filterQueues', $scope.filterQueues);
    };

    $scope.getQueues = function() {
        xucQueue.updateQueues(updateQueueFrequency);
        $scope.queues = xucQueue.getQueues();
        $timeout($scope.getQueues,updateQueueFrequency*1000,false);
        $scope.updateTotaux();
        $scope.$digest();
    };
    $timeout($scope.getQueues,updateQueueFrequency*1000,false);

    $timeout($scope.readCookie, 1000);
    $timeout($scope.filterAgents, 10000);
    $timeout($scope.filterAgents, 15000);
    $timeout($scope.filterAgents, 20000);
}]);

ccsuperApp.controller('ccAgentsController',
    ['$scope', '$timeout', '$filter', 'XucAgent',
    function($scope, $timeout, $filter, xucAgent) {

    $scope.agents = [];
    $scope.agentStateTimer = 2;
    $scope.nameMaxLen = 10;
    $scope.agentStyle = "";
    $scope.agent4Columns = true;
    $scope.agentStyle3Columns = "col-md-4";
    $scope.agentStyle4Columns = "col-md-3";
    $scope.agentStyle1 = "agentsColumnStyle";
    $scope.agentStyle2 = "agentsColumnStyle2";
    $scope.orders = [ {
        'criteria' : 'name',
        'labelfr' : 'Tri par nom (A->Z)',
        'labelen' : 'Order by name (A->Z)'
    }, {
        'criteria' : '-name',
        'labelfr' : 'Tri par nom (Z->A)',
        'labelen' : 'Order by name (Z->A)'
    }, {
        'criteria' : 'state',
        'labelfr' : 'Tri par état (A->Z)',
        'labelen' : 'Order by state (A->Z)'
    }, {
        'criteria' : '-state',
        'labelfr' : 'Tri par état (Z->A)',
        'labelen' : 'Order by state (Z->A)'
    }, {
        'criteria' : '-momentStart',
        'labelfr' : 'Tri par temps (00:00 -> 59:59)',
        'labelen' : 'Order by time (00:00->59:59)'
    }, {
        'criteria' : 'momentStart',
        'labelfr' : 'Tri par temps (59:59 -> 00:00)',
        'labelen' : 'Order by time (59:59 -> 00:00)'
    }, {
        'criteria' : 'status',
        'labelfr' : 'Tri par statut (A->Z)',
        'labelen' : 'Order by status (A->Z)'
    }, {
        'criteria' : '-status',
        'labelfr' : 'Tri par status (Z->A)',
        'labelen' : 'Order by status (Z->A)'
    } ];
    $scope.cookieOrder = {
        'name' : 'cookieOrderAgents',
        'value' : 'name'
    };
    $scope.cookieTheme = {
        'name' : 'cookieThemeAgents',
        'value' : $scope.agentStyle2
    };
    $scope.filterQueues = [];
    $scope.userStatuses = {};

    $scope.createCookie = function(cookieACreer) {
        var expirationDate = new Date();
        expirationDate.setFullYear(expirationDate.getFullYear() + 1);
        document.cookie = cookieACreer.name + "=" + cookieACreer.value + "; path=/; expires =" +
                expirationDate.toGMTString();
    };

    $scope.readCookie = function(cookieALire) {
        var nameEQ = cookieALire.name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) {
                cookieALire.value = c.substring(nameEQ.length, c.length);
                return cookieALire.value;
            }
        }
        return null;
    };

    $scope.setAgentStyle = function() {
        if($scope.agent4Columns)
            ($scope.agentStyle = $scope.agentStyle4Columns);
        else
            ($scope.agentStyle = $scope.agentStyle3Columns);
        $scope.agentStyle += " ";
        $scope.readCookie($scope.cookieTheme);
        $scope.agentStyle += $scope.cookieTheme.value;
    };

    $scope.agentswitchstyle = function() {
        if($scope.cookieTheme.value == $scope.agentStyle1)
            $scope.cookieTheme.value = $scope.agentStyle2;
        else
            $scope.cookieTheme.value = $scope.agentStyle1;
        $scope.createCookie($scope.cookieTheme);
        $scope.setAgentStyle();
    };

    // timeout
    $scope.updateAgentStates = function() {

        angular.forEach($scope.agents, function(value, key) {
            if ($scope.agents[key].state !== "AgentLoggedOut") {
                $scope.agents[key].timeInState = moment().countdown($scope.agents[key].momentStart);
            } else {
                $scope.agents[key].timeInState = moment().add('seconds', 0);
                $scope.agents[key].phoneNb = "";
            }
        });
        $scope.$apply();
        mytimeout = $timeout($scope.updateAgentStates, $scope.agentStateTimer * 1000, false);
    };
    $timeout($scope.updateAgentStates, 1000, false);

    $scope.orderAgents = function() {
        var criteria = $scope.readCookie($scope.cookieOrder);
        if(criteria === null)
            $scope.agents = $filter('orderBy')($scope.agents,'name');
        else
            $scope.agents = $filter('orderBy')($scope.agents,[criteria, 'name']);
    };

    $scope.removeAgentsNotDisplayed = function() {
        var filter = function(agent,index) {
            var disp = false;
            if (agent.state  !== 'AgentLoggedOut') {
                if ($scope.filterQueues.length === 0){
                    disp = true;
                }
                else {
                    angular.forEach(agent.queues, function(queue, keyQueue) {
                        if ($scope.filterQueues.indexOf(queue) > -1)
                            disp = true;
                    });
                }
            }
            return disp;
        };
        return $filter('filter')($scope.agents,filter);
    };

    $scope.agentsArray = [ [], [], [], [] ];

    $scope.updateAgentsList = function(nouvelAgent) {
        angular.forEach($scope.agentsArray, function(agent, keyAgent) {
            if (agent.id == nouvelAgent.id) {
                agent = nouvelAgent;
                return;
            }
        });
    };

    $scope.setDisplayNames = function(agents) {
        angular.forEach(agents, function(agent, keyAgent) {
            if (typeof agent.name === "undefined")
                agent.name = (agent.lastName.toUpperCase() + " " + agent.firstName).substring(0, $scope.nameMaxLen);
        });
    };

    $scope.dispatchAgents = function() {
        var toBeDisplayed = $scope.removeAgentsNotDisplayed();
        var toBeDisplayedLength = toBeDisplayed.length;
        var toBeDisplayedIndex = 0;
        $scope.setDisplayNames(toBeDisplayed);

        $scope.agentsArray = [ [], [], [], [] ];
        var agentsArrayIndex = 0;
        $scope.agent4Columns = (toBeDisplayed.length >= 10);
        if (!$scope.agent4Columns) $scope.agentsArray.pop();
        var agentsArrayLength = $scope.agentsArray.length;

        var nbResteAgent = toBeDisplayedLength % agentsArrayLength;
        var nbAgentByColumn = ((toBeDisplayedLength - nbResteAgent) / agentsArrayLength);
        if (nbResteAgent > 0) nbAgentByColumn++;

        for (i = 0; i < agentsArrayLength; i++) {
            $scope.agentsArray[i] = toBeDisplayed.splice(0, nbAgentByColumn);
        }

        angular.forEach($scope.agentsArray, function(column, indexColumn) {
            while (column.length < nbAgentByColumn) {
                column.push({
                    state : 'NoAgent',
                    name : '-',
                    phoneNb : '-',
                    timeInState : '-',
                    status : '-'
                });
            }
        });
        $scope.setAgentStyle();
    };

    $scope.changeOrder = function() {
        $scope.createCookie($scope.cookieOrder);
        $scope.orderAgents();
        $scope.dispatchAgents();
    };
    $scope.$on('AgentsLoaded', function(event) {
        $scope.agents = xucAgent.getAgents();
        $scope.dispatchAgents();
    });

    $scope.$on('ctiLoggedOn', function(e) {
        $scope.connected = true;
        xucAgent.start();
        $timeout($scope.orderAgents, 15000);
    });
    $scope.$on('linkDisConnected', function(e) {
        $scope.connected = false;
        $scope.agents.length = 0;
        $scope.dispatchAgents();
    });
    $scope.$on('QueueMemberUpdated', function(event, args) {
        $scope.dispatchAgents();
    });
    $scope.$on('AgentStateUpdated', function(event, args) {
        $scope.dispatchAgents();
    });
    $scope.$on('filterQueues', function(event, args) {
        $scope.filterQueues = args;
        $scope.dispatchAgents();
    });

    $scope.onUserStatuses = function(userStatuses) {
        angular.forEach(userStatuses, function(userStatus, keyUserStatus) {
            $scope.userStatuses[userStatus.name] = userStatus.longName;
        });
    };

    Cti.setHandler(Cti.MessageType.USERSTATUSES, $scope.onUserStatuses);

}]);

ccsuperApp.controller('ctiLinkController', function($rootScope, $scope, $timeout) {
    $scope.reconnectTimeout = 20000;

    $scope.loggedOnHandler = function(event) {
        $rootScope.$broadcast('ctiLoggedOn');
    };

    $scope.linkStatusHandler = function(event) {
        console.log("link status : " + event.status);
        $scope.status = event.status;
        if (event.status !== 'opened') {
            $rootScope.$broadcast('linkDisConnected');
            $timeout($scope.reconnect, $scope.reconnectTimeout);
        }
        $scope.$apply();
    };

    $scope.reconnect = function(event) {
        console.log("trying to reconnect");
        initCti(supervisionUsername);
    };

    // timeout
    $scope.updateClock = function() {
        $scope.clock = moment().format('H:mm:ss');
        mytimeout = $timeout($scope.updateClock, $scope.clockFrequency * 1000);
    };
    $timeout($scope.updateClock, $scope.clockFrequency * 1000);

    Cti.setHandler(Cti.MessageType.LOGGEDON, $scope.loggedOnHandler);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, $scope.linkStatusHandler);
});

var supervisionUsername = "";
var supervisionPassword = "";
var initCti = function(username, password, hostAndPort) {
    supervisionUsername = username;
    supervisionPassword = password;
    var phoneNumber = 0;
    console.log("sign in " + supervisionUsername + " " + password);
    var wsurl = "ws://" + hostAndPort + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
                            phoneNumber + "&amp;password=" + password;
    Cti.debugMsg = false;
    Cti.WebSocket.init(wsurl, supervisionUsername, 0);
};
