$(function() {
    var supervision = {};
    supervision.container = "#all_queues";
    supervision.nbOfQueues = 0;
    supervision.lineNb = 0;
    supervision.queues = [];

    var drawOrUpdateDoNut = function(divid, counters, radius) {
        var dataSet = counters;
        var todisplay = {
            Lost : "Lost",
            Link : "Link"
        };

        dataSet = dataSet.filter(function(el) {
            return todisplay[el.statName];
        });
        var validData = dataSet.filter(function(el) {
            if (el.value >= 0)
                return el.value;
        });

        if (validData.length === 0)
            return;
        if (dataSet.length === 0)
            return;

        var getValues = function(d) {
            return d.value;
        };

        var arc = d3.svg.arc().outerRadius(radius - 10).innerRadius(60);

        var agentpie = d3.layout.pie().value(getValues);

        var color = d3.scale.ordinal().range([ "green", "red", "orange", "yellow" ]);

        var arcs = d3.select(divid).select("svg").select(".arcGrp").selectAll("path").data(agentpie(dataSet));

        arcs.enter().append("svg:path").attr("stroke", "white").attr("stroke-width", 0.5).attr("fill", function(d, i) {
            return color(i);
        }).attr("d", arc).each(function(d) {
            this._current = d.value;
        });

        arcs.transition().attr("stroke", "white").attr("stroke-width", 0.5).attr("fill", function(d, i) {
            return color(i);
        }).attr("d", arc).each(function(d) {
            this._current = d.value;
        });

    };

    var drawOrUpdateCenterLabel = function(divid, counters) {
        var wtclass = "normal";

        var todisplay = {
            WaitingCalls : "WaitingCalls"
        };
        var dataSet = counters.filter(function(el) {
            return todisplay[el.statName];
        });

        if (dataSet.length === 0)
            return;

        var getData = function(d) {
            return d.value;
        };

        var getClass = function(d) {
            if (d.value > 3)
                return "error";
            if (d.value > 1)
                return "warning";
            else
                return "normal";
        };

        var center_label = d3.select(divid).select("svg").select(".ctrGroup").selectAll("text").data(dataSet);

        center_label.enter().append("svg:text").attr("dy", ".35em").attr("class", wtclass)
                .attr("text-anchor", "middle").text(getData);

        center_label.transition().attr("dy", ".35em").attr("class", getClass).attr("text-anchor", "middle").text(
                getData);
    };

    var drawDoNuts = function(divid, counters) {

        var width = 200, height = 200, radius = Math.min(width, height) / 2;

        if (d3.select(divid).selectAll("svg").empty()) {
            var svg = d3.select(divid).append("svg:svg").attr("width", width).attr("height", height);

            svg.append("svg:g").attr("class", "arcGrp").attr("transform",
                    "translate(" + (width / 2) + "," + (height / 2) + ")");

            svg.append("svg:g").attr("class", "ctrGroup").attr("transform",
                    "translate(" + (width / 2) + "," + (height / 2) + ")");

        }
        drawOrUpdateDoNut(divid, counters, radius);
        drawOrUpdateCenterLabel(divid, counters);
    };

    var queueConfigHandler = function(queueConfig) {
        console.log("queue config Handler " + JSON.stringify(queueConfig));
        supervision.queues[supervision.nbOfQueues] = queueConfig.id;
        if (supervision.nbOfQueues % 3 === 0) {
            supervision.lineNb = supervision.lineNb + 1;
            createLine(supervision.lineNb);
        }
        supervision.nbOfQueues = supervision.nbOfQueues + 1;
        createQueue(supervision.lineNb, supervision.nbOfQueues, queueConfig);
        console.log(supervision.nbOfQueues + " " + (supervision.lineNb));
        getStats();
    };

    var queueStatisticsHandler = function(event) {
        var divid = "#queue_body" + event.queueId;
        drawDoNuts(divid, event.counters);
    };

    var createLine = function(lineNb) {
        $(supervision.container).append('<li class="list-group-item" id="li_queue' + lineNb + '"/>');
        $("#li_queue" + lineNb).append('<div class="row" id="row_queue' + lineNb + '">');
    };

    var createQueue = function(lineNb, queueNb, queue) {
        $("#row_queue" + lineNb).append('<div class="col-md-4" id="col_queue' + queueNb + '">');
        $("#col_queue" + queueNb).append('<div class="panel panel-info" id="panel_info' + queueNb + '">');
        $("#panel_info" + queueNb).append(
                '<div class="panel-heading">' + queue.displayName + ' (' + queue.number + ')' + '</div>');
        $("#panel_info" + queueNb).append('<div class="panel-body text-center" id="queue_body' + queue.id + '"/>');
        $("#panel_info" + queueNb).append('<div class="panel-footer"/>');
    };
    var getStats = function() {
        for (var i = 0; i < supervision.queues.length; i++) {
            Cti.getQueueStatistics(supervision.queues[i], 3600, 60);
        }
    };
    var initUi = function() {
        $(supervision.container).empty();
        setInterval(getStats, 5000);
    };

    var loggedOnHandler = function() {
        $('#xuc_logon_panel').hide();
        Cti.getConfig("queue");
        Cti.subscribeToQueueStats();
    };

    $('#xuc_sign_in').click(
            function(event) {
                initUi();
                var username = $('#xuc_username').val();
                var password = $('#xuc_password').val();
                var hostAndPort = $('#xuc_hostAndPort').val();
                var phoneNumber = 0;
                console.log("sign in " + username + " " + password + " to " + hostAndPort);
                var wsurl = "ws://" + hostAndPort + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
                                            phoneNumber + "&amp;password=" + password;

                Cti.setHandler(Cti.MessageType.QUEUECONFIG, queueConfigHandler);
                Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, queueStatisticsHandler);
                Cti.setHandler(Cti.MessageType.LOGGEDON, loggedOnHandler);
                Cti.WebSocket.init(wsurl, username, 0);
            });
});
