var agentModule = angular.module('Agent', ['xuc.services','pascalprecht.translate','ngTable', 'ui.grid',
                                           'ui.grid.pagination', 'ui.bootstrap', 'LocalStorageModule']);

agentModule.config(function ($translateProvider, $translatePartialLoaderProvider, localStorageServiceProvider) {
    $translatePartialLoaderProvider.addPart('agent');
    $translatePartialLoaderProvider.addPart('queue');
    $translatePartialLoaderProvider.addPart('phonebar');
    $translatePartialLoaderProvider.addPart('sheet');
    $translateProvider.useLoader('$translatePartialLoader', {
    urlTemplate: 'assets/i18n/{part}-{lang}.json'
    });
    $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
           'en_*': 'en',
           'fr_*': 'fr'
    });
    $translateProvider.determinePreferredLanguage();
    $translateProvider.fallbackLanguage(['fr']);

    localStorageServiceProvider.setPrefix('agent.settings');
    localStorageServiceProvider.setNotify(true,true);
});

agentModule.controller('MonitorStateController', ['$scope','$attrs', function($scope, $attrs){
    $scope.canPauseMonitor = function() {
        return $scope.monitorState==='ACTIVE';
    };
    $scope.canUnpauseMonitor = function() {
        return $scope.monitorState==='PAUSED';
    };

    $scope.monitorPause = function() {
        Cti.monitorPause($scope.agent.id);
    };
    $scope.monitorUnPause = function() {
        Cti.monitorUnpause($scope.agent.id);
    };

    $scope.showRecordingControls = ($attrs.showrecord === 'true');
    console.log("Show recording configuration: ", $scope.showRecordingControls);
}]);

agentModule.controller('AgentController', ['$rootScope','$scope','$timeout','$translate','XucLink','XucAgent',
    function($rootScope, $scope, $timeout, $translate, xucLink, xucAgent) {

    var stateRefreshTimeout = 2000;

    $scope.loginButton = {};
    $scope.agentState = {};
    $scope.agentState.name = "-";
    $scope.agentState.start = moment();
    $scope.agentState.moment = "00:00";
    $scope.agentState.cause = "";
    $rootScope.agent = {};
    $scope.agent = {};
    $rootScope.agent.queueMembers = {};
    $scope.loggedIn = false;

    $scope.loginHandler = function(event) {
        $scope.loggedIn = true;
        $scope.$apply();
    };
    $scope.logoutHandler = function() {
        if ($scope.loggedIn) {
            $scope.loggedIn = false;
            $scope.$apply();
            window.location.replace("/agentLoginError?error=" + $translate.instant('ERROR_LOGGED_OUT') + "");
        }
    };

    $scope.logoutAgent = function() {
            Cti.logoutAgent();
            window.location.replace("/agent");
    };

    $scope.pausedHandler = function() {

        $scope.canPause = false;
        $scope.$apply();
    };
    $scope.readyHandler = function() {
        $scope.canPause = true;
        $scope.$apply();
    };
    $scope.onAgentStateEvent = function(event) {
        if (event.agentId == $rootScope.agent.id) {
            $scope.phoneNb = event.phoneNb;
            $rootScope.agent.phoneNb = event.phoneNb;
            $rootScope.agent.id = event.agentId;
            $scope.agent.phoneNb = event.phoneNb;
            $scope.agent.id = event.agentId;
            $scope.monitorState = event.monitorState;
            $scope.agentState.name = event.name;
            $scope.agentState.cause = "";
            var mt = xucAgent.buildMoment(event.since);
            $scope.agentState.momentStart = mt.momentStart;
            $scope.agentState.timeInState = mt.timeInState;

            switch (event.name) {
            case "AgentOnPause":
                $scope.agentState.cause = xucAgent.getUserStatusDisplayName(event.cause);
                console.log(event.cause);
                $scope.pausedHandler();
                $scope.loginHandler();
                break;
            case "AgentReady":
                $scope.readyHandler();
                $scope.loginHandler();
                break;
            case "AgentOnCall":
                $scope.loginHandler();
                break;
            case "AgentLoggedOut":
                $scope.logoutHandler();
                break;
            }
        }
    };

    $scope.onAgentError = function(event) {
        console.log("Agent error, logging out: ", event);
        if (event.Error === "LoggedInOnAnotherPhone") {
            Cti.logoutAgent();
        }
        window.location.replace("/agentLoginError?error=" + $translate.instant(event.Error) + "");
    };

    $scope.refreshState = function() {
        countdown.setLabels(
        ' ms| s| mn| h| d| w| m| y| dec| cent| mill',
        ' ms| s|||| wks|| yrs',
        '');

        $scope.agentState.timeInState = moment().countdown($scope.agentState.momentStart);
        mytimeout = $timeout($scope.refreshState, stateRefreshTimeout,false);
        $scope.$digest();
    };
    var mytimeout = $timeout($scope.refreshState, stateRefreshTimeout,false);

    $scope.$on('ctiLoggedOn', function(e,user){
        console.log('user loggedon ' + JSON.stringify(user));
        Cti.loginAgent(user.phoneNumber);
        console.log('login requested on ' + user.phoneNumber);
    });

    $scope.stop = function() {
        $timeout.cancel(mytimeout);
    };
    Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, $scope.onAgentStateEvent);
    Cti.setHandler(Cti.MessageType.AGENTERROR, $scope.onAgentError);

}]);
agentModule.controller('AgentUserController',['$scope','$rootScope', 'XucUser',function($scope, $rootScope, xucUser){
    $scope.user = xucUser.getUser();

    $scope.$on('userConfigUpdated', function(e) {
        $scope.user = xucUser.getUser();
        $rootScope.agent.id = $scope.user.agentId;
        $scope.$digest();
    });

}]);
agentModule.controller('AgentListenController',['$scope',function($scope){
    $scope.listened = false;

    $scope.onAgentStateEvent = function(event) {
        if (event.name !== "AgentOnCall") {
            $scope.listened = false;
            $scope.$digest();
        }
    };

    $scope.onAgentListened = function(agentListened) {
        $scope.listened = agentListened.started;
        $scope.$digest();
    };

    Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, $scope.onAgentStateEvent);
    Cti.setHandler(Cti.MessageType.AGENTLISTEN, $scope.onAgentListened);

}]);
agentModule.controller('AgentPauseController',['$scope','XucAgent',function($scope, xucAgent){

    $scope.notReadyStatuses = xucAgent.getNotReadyStatuses();
    $scope.readyStatuses = xucAgent.getReadyStatuses();

    $scope.togglePause = function() {
        if ($scope.canPause)
            Cti.pauseAgent();
        else
            if ($scope.readyStatuses.length === 1) {
                Cti.changeUserStatus($scope.readyStatuses[0].name);
            }
            else {
                Cti.unpauseAgent();
            }
    };

    $scope.noStatuses = function() {
        return (
            ($scope.canPause && $scope.notReadyStatuses.length === 0) || (!$scope.canPause && $scope.readyStatuses.length <= 1));
    };
    $scope.canPauseWithStatus = function() {
        return ($scope.canPause && ($scope.notReadyStatuses.length > 0));
    };
    $scope.canUnPauseWithStatus = function() {
        return (!$scope.canPause && ($scope.readyStatuses.length > 1));
    };

    $scope.$on('AgentUserStatusesLoaded', function(e) {
        $scope.notReadyStatuses =  xucAgent.getNotReadyStatuses();
        $scope.readyStatuses = xucAgent.getReadyStatuses();
    });

    $scope.updateStatus = function(statusName) {
        console.log(statusName);
        Cti.changeUserStatus(statusName);
    };

}]);

agentModule.controller('AgentGroupViewController',
    ['$rootScope','$scope','$translate','$filter','uiGridConstants','XucAgent','$interval', '$timeout',
    function($rootScope, $scope, $translate, $filter, uiGridConstants, xucAgent, $interval, $timeout) {

    $scope.refreshTimeout = 3000;
    $scope.$on('ctiLoggedOn', function() {
        xucAgent.start();
    });

    $scope.columns = [
        { field: 'firstName', displayName: 'COL_AGENT_FIRSTNAME', headerCellFilter: 'translate',
                sort: { direction: uiGridConstants.DESC, priority: 1}},
        { field: 'lastName', displayName: 'COL_AGENT_LASTNAME', headerCellFilter: 'translate',
            sort: { direction: uiGridConstants.DESC, priority: 2}},
        { field: 'phoneNb', displayName: 'COL_AGENT_PHONENB', headerCellFilter: 'translate',
            sort: { direction: uiGridConstants.ASC, priority: 3}, cellTemplate: '/assets/templates/callableCellTemplate.html'},
        { field: 'stateName', displayName: 'COL_AGENT_STATE', headerCellFilter: 'translate',  cellFilter: 'translate', cellClass: 'state',
            cellTemplate: '<div class="ui-grid-cell-contents T{{row.entity.state}}" title="TOOLTIP">{{COL_FIELD CUSTOM_FILTERS}}</div>'},
        { field: 'state', visible: false},
        { field: 'timeInState', displayName: 'COL_AGENT_TIMEINSTATE', headerCellFilter: 'translate', cellFilter: 'timeInState | dashWhenEmpty'},
    ];

    $scope.$on('AgentsLoaded', function(agents) {
        if ($rootScope.agent.id) {
            var groupId = xucAgent.getAgent($rootScope.agent.id).groupId;
            $scope.gridOptions.data = $scope.filterItself(xucAgent.getAgentsInGroup(groupId));
            console.log('agentOfGroup:', xucAgent.getAgentsInGroup(groupId));
            $scope.gridApi.core.refresh();
            $scope.$broadcast('xucRefreshPagination');
            $interval($scope.refreshSinceValues(), $scope.refreshTimeout);
            console.log('Agents ', JSON.stringify(xucAgent.getAgents()));
        }
    });

    $scope.filterItself = function(agents) {
        return agents.filter(function(agent) {return agent.id != $rootScope.agent.id; });
    };

    $scope.refreshSinceValues = function() {
        $scope.gridApi.core.refresh();
    };

    $scope.gridOptions = {
        data: [],
        columnDefs: $scope.columns,
        enableColumnMenus: false,
        paginationPageSizes: [10, 20, 50],
        paginationPageSize: 10,
        enablePagination: true,
        enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
        enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
        showColumnFooter: false,
        onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
        }
    };

    // hack to have ui-grid work with ng-show
    $scope.$watch('display.callbacks', function() {
        $timeout(function() {
            $scope.gridApi.core.handleWindowResize();
        }, 0, false);
    });
}]);

agentModule.controller('AgentHeaderStatsView',
    ['$rootScope','$scope','$translate','$filter','uiGridConstants','XucAgent','$interval',
    function($rootScope, $scope, $translate, $filter, uiGridConstants, xucAgent, $interval) {

    $scope.refreshTimeout = 3000;
    $scope.loaded = false;

    $scope.$on('AgentStatisticsUpdated', function(){
        $scope.updateAgentData();
    });

    $scope.$on('AgentStateUpdated', function(){
        $scope.updateAgentData();
    });

    $scope.$on('AgentsLoaded', function(){
        $scope.loaded = true;
        $scope.updateAgentData();
    });

    $scope.updateAgentData = function() {
        if ($scope.loaded) {
            var agentData = xucAgent.getAgent($rootScope.agent.id);
            $scope.gridOptions.data = [agentData];
        }
    };

    $scope.columns = [
        { field: 'stats.PausedTime', displayName: 'COL_AGENT_PAUSEDTIME', headerCellFilter: 'translate', cellFilter: 'totalTime | dashWhenEmpty', width: '10%'},
        { field: 'stats.WrapupTime', displayName: 'COL_AGENT_WRAPUPTIME', headerCellFilter: 'translate', cellFilter: 'totalTime | dashWhenEmpty', width: '8%'},
        { field: 'stats.InbCalls', displayName: 'COL_AGENT_INBCALLS', headerCellFilter: 'translate', cellFilter: "dashWhenEmpty", width: '8%'},
        { field: 'stats.InbAnsCalls', displayName: 'COL_AGENT_INBANSCALLS', headerCellFilter: 'translate', cellFilter: "dashWhenEmpty", width: '8%'},
        { field: 'stats.InbAverCallTime', displayName: 'COL_AGENT_INBCALLTIME', headerCellFilter: 'translate', cellFilter: 'totalTime | dashWhenEmpty', width: '12%'},
        { field: 'stats.InbCallTime', displayName: 'COL_AGENT_INBAVERCALLTIME', headerCellFilter: 'translate', cellFilter: 'totalTime | dashWhenEmpty', width: '12%'},
        { field: 'stats.InbUnansCalls', displayName: 'COL_AGENT_INBUNANSCALLS', headerCellFilter: 'translate', cellFilter: "dashWhenEmpty", width: '10%'},
        { field: 'stats.InbPercUnansCalls', displayName: 'COL_AGENT_INBPERCUNANSCALLS', headerCellFilter: 'translate', cellFilter: 'number:1 | dashWhenEmpty', width: '12%'},
        { field: 'stats.OutCalls', displayName: 'COL_AGENT_OUTCALLS', headerCellFilter: 'translate', cellFilter: 'dashWhenEmpty', width: '8%'},
        { field: 'stats.OutCallTime', displayName: 'COL_AGENT_OUTCALLTIME', headerCellFilter: 'translate', cellFilter: 'totalTime | dashWhenEmpty', width: '12%'},
    ];

    $scope.gridOptions = {
        data: [],
        columnDefs: $scope.columns,
        enableColumnMenus: false,
        enablePagination: false,
        enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
        enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
        showColumnFooter: false,
        onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
        }
    };
}]);

agentModule.controller('PhoneController', function($rootScope, $scope, $timeout) {
    $scope.phoneStatus = {};
    $scope.phoneStatus.label = "";
    $scope.phoneStatus.start = moment();
    $scope.phoneStatus.moment = "0 second";

    $scope.normalize = function(nb) {
        nb = "" + nb;
        return nb.replace(/[ ()\.]/g, "");
    };
    $scope.phoneStatusHandler = function(event) {
        $scope.phoneStatus.label = event.status;
        $scope.phoneStatus.start = moment();
        if (event.status == "AVAILABLE")
            $rootScope.$broadcast('phone_available');
        $scope.$apply();
    };
    $scope.updatePhoneStatus = function() {
        $scope.phoneStatus.moment = moment().countdown($scope.phoneStatus.start).toString();
        $scope.$digest();
        mytimeout = $timeout($scope.updatePhoneStatus, 5000, false);
    };

    $scope.stop = function() {
        $timeout.cancel(mytimeout);
    };

    $scope.dial = function() {
        Cti.originate($scope.normalize($scope.destination));
    };
    $scope.attendedTransfer = function() {
        Cti.attendedTransfer($scope.normalize($scope.destination));
    };
    $scope.completeTransfer = function() {
        Cti.completeTransfer();
    };
    $scope.cancelTransfer = function() {
        Cti.cancelTransfer();
    };
    $scope.answer = function() {
        Cti.answer();
    };
    $scope.hangup = function() {
        Cti.hangup();
    };
    $scope.conference = function() {
        Cti.conference();
    };
    $scope.hold = function() {
        Cti.hold();
    };
    var mytimeout = $timeout($scope.updatePhoneStatus, 5000, false);
    Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, $scope.phoneStatusHandler);
});

agentModule.controller('CallbackController', ['$scope', '$modal', '$rootScope', '$translate', '$timeout', 'uiGridConstants', 'XucCallback',
                       function($scope, $modal, $rootScope, $translate, $timeout, uiGridConstants, xucCallback) {
    $scope.callbacks = [];
    $scope.myQueues = [];

    $scope.columns = [
        { field: 'dueDate', visible: false, sort: { direction: uiGridConstants.ASC, priority: 1}},
        { field: 'preferredPeriod.periodStart', displayName: '', sort: { direction: uiGridConstants.ASC, priority: 2}, enableSorting: false,
        cellTemplate: '/assets/templates/callbackStatusCellTemplate.html'},
        { field: 'phoneNumber', displayName: 'COL_CB_PHONE_NUMBER', headerCellFilter: 'translate', enableSorting: false},
        { field: 'mobilePhoneNumber', displayName: 'COL_CB_MOBILE', headerCellFilter: 'translate', enableSorting: false},
        { field: 'fullName', displayName: 'COL_CB_FULLNAME', headerCellFilter: 'translate',
        cellTemplate: '<span>{{row.entity.firstName}} {{row.entity.lastName}}</span>', enableSorting: false},
        { field: 'company', displayName: 'COL_CB_COMPANY', headerCellFilter: 'translate', enableSorting: false},
        { field: 'test', displayName: '', cellFilter: 'translate', enableSorting: false,
        cellTemplate: '<a href="#"><span class="glyphicon glyphicon-arrow-right" style="color:green" ng-click="grid.appScope.takeCallback(row)"></span></a>'}
    ];

    function getTime(dateStr, timeStr) {
        return new Date(dateStr + 'T' + timeStr);
    }

    function onTwoDigits(i) {
        var iStr = i.toString();
        if(iStr.length == 1) return '0' + iStr;
        return iStr;
    }

    $scope.getCallbackStatus = function(callback) {
        var start = getTime(callback.dueDate, callback.preferredPeriod.periodStart);
        var end = getTime(callback.dueDate, callback.preferredPeriod.periodEnd);
        var now = new Date();

        if(now > end) return 'status_too_late';
        if(now < start) return 'status_later';
        return 'status_ok';
    };

    $scope.gridOptions = {
        data: [],
        columnDefs: $scope.columns,
        enableColumnMenus: false,
        paginationPageSizes: [10, 20, 50],
        paginationPageSize: 10,
        enablePagination: true,
        enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
        enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
        showColumnFooter: false,
        onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
        }
    };

    $scope.onModalDismissed = function(uuid) {
        return function() {
            xucCallback.releaseCallback(uuid);
        };
    };

    $scope.onQueueMemberList = function(qmList) {
        $scope.myQueues = [];
        for(var i = 0; i < qmList.length; i++) {
            if(qmList[i].agentId == $rootScope.agent.id)
                $scope.myQueues.push(qmList[i].queueId);
        }
    };

    $scope.onCallbacksLoaded = function() {
        var lists = xucCallback.getCallbackLists();
        $scope.callbacks = [];
        for(var i=0; i<lists.length; i++) {
            if($.inArray(lists[i].queueId, $scope.myQueues) >= 0) {
                for(var j=0; j<lists[i].callbacks.length; j++) {
                    if(!lists[i].callbacks[j].agentId)
                        $scope.callbacks.push(lists[i].callbacks[j]);
                }
            }
        }
        $scope.gridOptions.data = $scope.callbacks;
        $scope.gridApi.core.refresh();
        $scope.$apply();
    };

    $scope.takeCallback = function(row) {
        $scope.pendingUuid = row.entity.uuid;
        xucCallback.takeCallback($scope.pendingUuid);
    };

    $scope.callbackPopup = function(uuid) {
        var modalInstance = $modal.open({
            templateUrl: 'callbackPopup.html',
            controller: 'CallbackPopupController',
            resolve: {
                callback :function () {
                    for(var i=0; i<$scope.callbacks.length; i++) {
                        if($scope.callbacks[i].uuid == uuid)
                            return $scope.callbacks[i];
                    }
                },
                statuses: function(){return xucCallback.statuses;},
                xucCallback: function(){return xucCallback;}
            }
        }).result.then($scope.onModalClosed, $scope.onModalDismissed(uuid));
    };

    $scope.onCallbackTaken = function(event, cbTaken) {
        if(cbTaken.uuid == $scope.pendingUuid) {
            delete $scope.pendingUuid;
            if(cbTaken.agentId == $rootScope.agent.id)
                $scope.callbackPopup(cbTaken.uuid);
        }
        $scope.onCallbacksLoaded();
    };

    $scope.onCallbackReleased = function(event, cbReleased) {
        $scope.onCallbacksLoaded();
    };

    Cti.setHandler(Cti.MessageType.LOGGEDON, xucCallback.start);
    Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, $scope.onAgentStateEvent);
    Cti.setHandler(Cti.MessageType.QUEUEMEMBERLIST, $scope.onQueueMemberList);
    $scope.$on('CallbacksLoaded', $scope.onCallbacksLoaded);
    $scope.$on('CallbackTaken', $scope.onCallbackTaken);
    $scope.$on('CallbackReleased', $scope.onCallbackReleased);

    // hack to have ui-grid work with ng-show
    $scope.$watch('display.callbacks', function() {
        $timeout(function() {
            $scope.gridApi.core.handleWindowResize();
        }, 0, false);
    });
}]);

agentModule.controller('CallbackPopupController', function ($scope, $modalInstance, xucCallback, callback, statuses) {
    $scope.callback = callback;
    $scope.statuses = statuses;
    $scope.ticket = {
        uuid: null,
        status: null,
        comment: null
    };

    $scope.updateTicket = function() {
        xucCallback.updateCallbackTicket($scope.ticket);
    };

    $scope.ok = function () {
        $scope.updateTicket();
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.startCallback = function(number) {
        xucCallback.startCallback($scope.callback.uuid, number);
    };

    $scope.onCallbackStarted = function(e, cbStarted) {
        if($scope.callback.uuid == cbStarted.requestUuid)
            $scope.ticket.uuid = cbStarted.ticketUuid;
    };

    $scope.$on('CallbackStarted', $scope.onCallbackStarted);
});

agentModule.controller("AgentsCallbacksController", function($scope, $attrs) {
    $scope.configShowCallbacks = ($attrs.showcallbacks === 'true');
    console.log("Show callbacks configuration: ", $scope.configShowCallbacks);

    $scope.display = {
        callbacks: false
    };

    $scope.showAgents = function() {
        $scope.display.callbacks = false;
    };

    $scope.showCallbacks = function() {
        $scope.display.callbacks = true;
    };
});

agentModule.directive('xucAutoPgctrlGrid', ['$timeout', function($timeout) {
    var upperTresholdKeepingPaginationShown = 10;
    return {
        link: function($scope, element, attrs) {
            var checkGrid = function() {
                if ($scope.gridOptions.data.length < $scope.gridOptions.paginationPageSize &&
                    $scope.gridOptions.paginationPageSize <= upperTresholdKeepingPaginationShown) {
                    $scope.gridApi.grid.options.enablePaginationControls=false;
                }
                else {
                    $scope.gridApi.grid.options.enablePaginationControls=true;
                }
            };
         $scope.$on('xucRefreshPagination', checkGrid);
        }
    };
}]);

var initCti = function(username, password, phoneNumber, hostAndPort) {
    Cti.debugMsg = false;
    var xucLink = angular.element(document.body).injector().get("XucLink");
    xucLink.setHostAndPort(hostAndPort);
    xucLink.setHomeUrl("/agent");
    xucLink.initCti(username,password,phoneNumber);
};

