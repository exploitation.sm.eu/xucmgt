(function(){
    'use strict';
    angular
    .module('Agent')
    .controller('Sheet',callbackCtrl);

    callbackCtrl.$inject= ['$scope', '$translate'];

    function callbackCtrl($scope, $translate) {
        $scope.showSheet = false;
        $scope.fields = [];

        $scope.onEstablished = function(event) {
            $scope.fields.push({ 'name': $translate.instant('CALLER_NUMBER'), 'value': event.otherDN });
            $scope.fields.push({ 'name': $translate.instant('QUEUE_NAME'), 'value': event.queueName });
            console.log('fields: ', $scope.fields);
            $scope.showSheet = true;
            $scope.$apply();
        };

        $scope.onReleased = function() {
            $scope.fields = [];
            $scope.showSheet = false;
        };

        $scope.onPhoneEvent = function(event) {
            switch(event.eventType) {
                case 'EventEstablished':
                    $scope.onEstablished(event);
                    break;
                case 'EventReleased':
                    $scope.onReleased();
                    break;
            }
        };
        Cti.setHandler(Cti.MessageType.PHONEEVENT, $scope.onPhoneEvent);
    }
})();
