(function(){
    'use strict';
    angular
    .module('Agent')
    .controller('ContactRequest',callbackCtrl);

    callbackCtrl.$inject= ['$scope','XucDirectory'];

    function callbackCtrl($scope, XucDirectory) {
        $scope.normalize = function(nb) {
            nb = "" + nb;
            nb = nb.replace(/^\+/,"00");
            return nb.replace(/[ ()\.]/g, "");
        };

        $scope.isPhoneNumber = function (s) {
            return /^\d+$/.test(s);
        };

        $scope.processContactRequest = function(contact) {
            var normalized = $scope.normalize(contact.destination);
            if (normalized.length <= 0) {
                console.log("INFO: ignoring empty string");
            } else {
                if ($scope.isPhoneNumber(normalized)) {
                    $scope.call(normalized);
                }
                else {
                    $scope.search(contact.destination);
                }
            }
        };

        $scope.search = function(term) {
            console.log("searching for: ", term);
            Cti.directoryLookUp(term);
        };

        $scope.call = function(number) {
            console.log("calling: " + number.toString());
            Cti.dial(number.toString());
        };
    }
})();
