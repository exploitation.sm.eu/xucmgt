(function(){
    'use strict';
    angular
    .module('Agent')
    .controller('ContactsPresentation',callbackCtrl);

    callbackCtrl.$inject= ['$scope','$timeout', 'XucDirectory'];

    function callbackCtrl($scope, $timeout, XucDirectory) {
        $scope.showSearch = false;
        $scope.fadeoutTimeout = 30000;

        $scope.$on('searchResultUpdated', function(e) {
            $scope.searchResult = XucDirectory.getSearchResult();
            console.log("searchResult updated: ", $scope.searchResult);
            $scope.headers = XucDirectory.getHeaders();
            $timeout($scope.statusFadeout, $scope.fadeoutTimeout);
            $scope.showSearch=true;
            $scope.$apply();
        });

        $scope.statusFadeout = function() {
            console.log("Dropping user statuses");
            angular.forEach($scope.searchResult, function(result) {
                result.status=4;
            });
        };

        $scope.hideSearch = function() {
            $scope.showSearch=false;
        };
    }
})();
