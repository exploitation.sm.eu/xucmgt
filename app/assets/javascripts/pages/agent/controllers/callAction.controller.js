(function(){
    'use strict';
    angular
    .module('Agent')
    .controller('CallAction',callbackCtrl)
    .run(function(XucPhoneState) {
        console.log("Starting XucPhoneState");
    });

    callbackCtrl.$inject= ['$scope','XucPhoneState'];

    function callbackCtrl($scope, xucPhoneState) {
        $scope.dial = function(dst) {
            Cti.dial(dst);
        };

        $scope.directTransfer = function(dst) {
            Cti.directTransfer(dst);
        };

        $scope.setPhoneStatuses = function(phoneStatus) {
            if (xucPhoneState.isPhoneAvailable(phoneStatus)) {
                $scope.phoneAvailable = true;
                $scope.phoneOffHook = false;
                $scope.phoneOther = false;
            }
            else if (xucPhoneState.isPhoneOffHook(phoneStatus)) {
                $scope.phoneOffHook = true;
                $scope.phoneAvailable = false;
                $scope.phoneOther = false;
            }
            else {
                $scope.phoneOther = true;
                $scope.phoneOffHook = false;
                $scope.phoneAvailable = false;
            }
        };

        $scope.$on('phoneStateUpdated', function(event, status) {
            $scope.setPhoneStatuses(status);
        });

        $scope.init = function() {
            var status = xucPhoneState.getState();
            $scope.setPhoneStatuses(status);
        };

        $scope.init();
    }

})();
