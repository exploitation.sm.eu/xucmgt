(function(){
    'use strict';
    angular
    .module('Agent')
    .controller('QueueView',callbackCtrl);

    callbackCtrl.$inject= ['$scope','$translate','$filter', '$attrs', 'uiGridConstants', 'XucQueueTotal', 'AgentPreferences'];

    function callbackCtrl($scope, $translate, $filter, $attrs, uiGridConstants, xucQueueTotal, agentPreferences) {

        $scope.stats = xucQueueTotal.getCalcStats();
        $scope.allQueues = false;
        $scope.showQueueControls = ($attrs.showqueuecontrols === 'true');
        console.log("Show Queue Controls: ", $scope.showQueueControls);

        $scope.$watch('queues', function(newQueues, oldQueues) {
            $scope.refreshQueues($scope.addAgentAssociationState(newQueues));
        }, true);

        $scope.$watch('favoriteQueues', function() {
            $scope.refreshQueues($scope.queues);
        }, true);

        $scope.addAgentAssociationState = function(queues) {
            var result = [];
            angular.forEach(queues, function(queue) {
                queue.associated=$scope.agentWorksOnQueue(queue);
                if (queue.associated === true) {
                    agentPreferences.addFavoriteQueue(queue.id);
                }
                result.push(queue);
            });
            return result;
        };

        $scope.refreshQueues = function(newQueues) {
            if ($scope.allQueues) {
                $scope.gridOptions.data = newQueues;
            }
            else {
                $scope.gridOptions.data = $filter('filter')(newQueues, $scope.isFavoriteQueue);
            }
            $scope.gridApi.core.refresh();
            $scope.$broadcast('xucRefreshPagination');
            xucQueueTotal.calculate(newQueues);
            $scope.stats = xucQueueTotal.getCalcStats();
            $('#sumWaitingCalls').text('Total: ' + $scope.stats.sum.WaitingCalls);
            $('#maxLongestWaitTime').text('Max: ' + $scope.stats.max.LongestWaitTime);
        };

        $scope.showAgentQueues = function() {
            $scope.allQueues = false;
            $scope.refreshQueues($scope.queues);
        };

        $scope.showAllQueues = function() {
            $scope.allQueues = true;
            $scope.refreshQueues($scope.queues);
        };

        $scope.canAssociate = function(logged) {
            return !logged && $scope.showQueueControls;
        };

        $scope.canDissociate = function(logged) {
            return logged && $scope.showQueueControls;
        };

        $scope.canRemoveFromFavorites = function(logged) {
            return !$scope.allQueues && !logged;
        };

        $scope.noActionPossible = function(logged) {
            return !($scope.canAssociate(logged) || $scope.canDissociate(logged) || $scope.canRemoveFromFavorites(logged));
        };

        $scope.columns = [
            { field: 'id', visible: false},
            { field: 'associated', sort: { direction: uiGridConstants.DESC, priority: 1}, enableFiltering: false,
                displayName: '', cellTemplate: '/assets/templates/queueStatusCellTemplate.html'},
            { field: 'number', displayName: 'COL_NUMBER', headerCellFilter: 'translate',
                sort: { direction: uiGridConstants.ASC, priority: 3}, cellTemplate: '/assets/templates/callableCellTemplate.html'},
            { field: 'displayName', displayName: 'COL_DISPLAYNAME', headerCellFilter: 'translate'},
            { field: 'WaitingCalls', displayName: 'COL_WAITINGCALLS', headerCellFilter: 'translate',
                footerCellTemplate: '<div class="ui-grid-cell-contents" id="sumWaitingCalls"></div>',
                sort: { direction: uiGridConstants.DESC, priority: 2}, enableFiltering: false},
            { field: 'LongestWaitTime', displayName: 'COL_LONGESTWAITTIME', headerCellFilter: 'translate',
                footerCellTemplate: '<div class="ui-grid-cell-contents" id="maxLongestWaitTime"></div>',
                enableFiltering: false},
            { field: 'AvailableAgents', displayName: 'COL_AVAILABLEAGENTS', headerCellFilter: 'translate',
                enableFiltering: false}
        ];

        $scope.gridOptions = {
            enableFiltering: true,
            data: $scope.agentQueues,
            columnDefs: $scope.columns,
            enableColumnMenus: false,
            paginationPageSizes: [10, 20, 50],
            paginationPageSize: 10,
            enablePagination: true,
            enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
            enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
            showColumnFooter: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
            }
        };
    }
})();
