(function(){
    'use strict';
    angular
    .module('Agent')
    .controller('Queue',callbackCtrl);

    callbackCtrl.$inject= ['XucQueue','AgentPreferences','$scope','$timeout','$rootScope'];

    function callbackCtrl(XucQueue,AgentPreferences,$scope,$timeout,$rootScope) {
        $scope.defaultPenalty = 0;

        $scope.queues = [];

        var queueRefreshTimeout = 5000;

        $scope.loadQueueMembers = function() {
            Cti.getConfig("queuemember");
        };

        $scope.agentWorksOnQueue = function(queue) {
            return (typeof $rootScope.agent.queueMembers[queue.id] !== 'undefined');
        };

        $scope.isFavoriteQueue = function(queue) {
            return AgentPreferences.isFavoriteQueue(queue.id);
        };

        $scope.$on('ctiLoggedOn', function(e) {
            XucQueue.start();
            console.log("cti logged on");
            $timeout($scope.loadQueueMembers,1000);
        });

        $scope.$on('preferences.set.favoriteQueues', function(name, newvalue) {
            $scope.favoriteQueues = newvalue;
        });

        $scope.onQueueMember = function(queueMember) {
            if (queueMember.agentId === $rootScope.agent.id) {
                if (queueMember.penalty >= 0) {
                    $rootScope.agent.queueMembers[queueMember.queueId] = {};
                    $rootScope.agent.queueMembers[queueMember.queueId] = queueMember.penalty;
                } else {
                    delete $rootScope.agent.queueMembers[queueMember.queueId];
                }
            }
        };

        $scope.enterQueue = function(queueId) {
            Cti.setAgentQueue($rootScope.agent.id, queueId, $scope.defaultPenalty);
            AgentPreferences.addFavoriteQueue(queueId);
        };

        $scope.leaveQueue = function(queueId) {
            Cti.removeAgentFromQueue($rootScope.agent.id, queueId);
        };

        $scope.removeFromFavorites = function(queueId) {
            AgentPreferences.removeFavoriteQueue(queueId);
        };

        $scope.getQueues = function() {
            $scope.favoriteQueues = AgentPreferences.getFavoriteQueues();
            $scope.queues = XucQueue.getQueues();
            $timeout($scope.getQueues,queueRefreshTimeout,false);
            $scope.$digest();
        };

        $timeout($scope.getQueues,queueRefreshTimeout,false);
        Cti.setHandler(Cti.MessageType.QUEUEMEMBER, $scope.onQueueMember);
    }
})();
