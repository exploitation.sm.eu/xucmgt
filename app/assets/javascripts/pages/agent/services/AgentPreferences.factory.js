(function() {
    'use strict';

    angular.module('Agent').factory('AgentPreferences', AgentPreferences);

    AgentPreferences.$inject = [ 'localStorageService', '$rootScope' ];

    function AgentPreferences(localStorageService, $rootScope) {
        var favoriteQueuesKey = 'favoriteQueues';
        var favoriteQueues = [];

        var _init = function() {
            favoriteQueues = localStorageService.get(favoriteQueuesKey) || [];
        };
        _init();

        var _getFavoriteQueues = function(queues) {
            return favoriteQueues;
        };
        var _setFavoriteQueues = function(queues) {
            localStorageService.set(favoriteQueuesKey, queues);
        };
        var _addFavoriteQueue = function(queueId) {
            if (favoriteQueues.indexOf(queueId) < 0 ) {
                favoriteQueues.push(queueId);
                _setFavoriteQueues(favoriteQueues);
            }
        };
        var _removeFavoriteQueue = function(queueId) {
            var index = favoriteQueues.indexOf(queueId);
            if (index >= 0 ) {
                favoriteQueues.splice(index, 1);
                _setFavoriteQueues(favoriteQueues);
            }
        };
        var _isFavoriteQueue = function(queueId) {
            return favoriteQueues.indexOf(queueId) >= 0 ? true : false;
        };

        $rootScope.$on('LocalStorageModule.notification.setitem', function(event, args) {
            $rootScope.$broadcast('preferences.set.' + args.key, args.newvalue);
        });

        return {
            getFavoriteQueues : _getFavoriteQueues,
            setFavoriteQueues : _setFavoriteQueues,
            addFavoriteQueue : _addFavoriteQueue,
            removeFavoriteQueue: _removeFavoriteQueue,
            isFavoriteQueue : _isFavoriteQueue,
            _init : _init
        };
    }

})();
