
var xucServices = angular.module('xuc.services', ['pascalprecht.translate']);

xucServices.config(function ($translateProvider) {
  $translateProvider.translations('fr', {
    AgentReady: 'Prêt',
    AgentOnPause: 'En pause',
    AgentLoggedOut: 'Deloggué',
    AgentOnCall: 'Conv.',
    AgentOnIncomingCall: 'Conv. Entr.',
    AgentOnOutgoingCall: 'Conv. Sort.',
    AgentOnWrapup: 'Post Appel',
    AgentOnAcdCall: 'Conv. ACD',
    AgentDialing: 'Num.',
    AgentRinging: 'Sonnerie',
    Fax: 'Fax',
    NoAnswer: 'Non réponse',
    Answered: 'Répondu',
    Callback: 'A rappeler'
  });
  $translateProvider.translations('en', {
    AgentReady: 'Ready',
    AgentOnPause: 'Paused',
    AgentLoggedOut: 'Loggued Out',
    AgentOnCall: 'On Call',
    AgentOnIncomingCall: 'Inc. Call',
    AgentOnOutgoingCall: 'Out. Call',
    AgentOnWrapup: 'Wrapup',
    AgentOnAcdCall: 'ACD Call',
    AgentDialing: 'Dialing',
    AgentRinging: 'Ringing',
    Fax: 'Fax',
    NoAnswer: 'No answer',
    Answered: 'Answered',
    Callback: 'Call back'
  });
  $translateProvider.determinePreferredLanguage();
  $translateProvider.fallbackLanguage(['en']);
});

 xucServices.filter('queueTime', function() {
    return function(seconds) {
           if (typeof seconds == "undefined" || seconds == "-")
               return "";
           else if (seconds < 60)
               return seconds;
           if (seconds < 3600)
               return moment().hour(0).minute(0).seconds(0).add('seconds', seconds).format("m:ss");
           return moment().hour(0).minute(0).seconds(0).add('seconds', seconds).format("H:mm:ss");
        };
  });
 xucServices.filter('totalTime', function() {
    return function(t) {
           if (typeof t == "undefined" || t == "-")
               return "";
           if(t < 100*3600) {
             return ('0'+Math.floor(t/3600) % 100).slice(-2)+':'+('0'+Math.floor(t/60)%60).slice(-2)+':'+('0' + t % 60).slice(-2);
           }
           else {
            if(t < 1000*3600) {
                return ('00'+Math.floor(t/3600) % 1000).slice(-3)+':'+('0'+Math.floor(t/60)%60).slice(-2)+':'+('0' + t % 60).slice(-2);
            }
            else {
                return "###:##:##";
            }
           }
        };
  });
 xucServices.filter('timeInState', function() {
    return function(timeInState) {
        if ( typeof timeInState !== 'undefined') {
           return timeInState.toClockString();
        }
        else return "";
    };
  });

  xucServices.filter('booleanText', function() {
    return function(bool) {
        if(bool)
            return 'Y';
        else
            return 'N';
    };
  });
  xucServices.filter('dateTime', function() {
    return function(timeStamp) {
        if(timeStamp) {
            var dateFormat="HH:mm:ss";
            if (moment().dayOfYear() !== moment(timeStamp).dayOfYear()) {
                dateFormat="DD HH:mm:ss";
            }
            return moment(timeStamp).format(dateFormat);
        }
        else {
            return "-";
        }
    };
  });
  xucServices.filter('dashWhenEmpty', function() {
    return function(number) {
        if(number) {
            return number;
        }
        else {
            return "-";
        }
    };
  });

xucServices.factory('AgentStates',['XucAgent','$translate','$q', function(xucAgent, $translate, $q) {
    var definedStates = ['AgentReady','AgentRinging','AgentDialing','AgentOnIncomingCall','AgentOnOutgoingCall','AgentOnAcdCall',
                            'AgentOnWrapup','AgentOnPause','AgentLoggedOut'];
    var _buildStates = function() {
        var def = $q.defer(),
                arr = [],
                states = [];
        angular.forEach(definedStates, function(item){
            if (arr.indexOf(item) === -1) {
                arr.push(item);
                states.push({
                    'id': item,
                    'label': $translate.instant(item)
                });
            }
        });
        angular.forEach(xucAgent.getNotReadyStatuses(), function(item){
                    if (arr.indexOf(item) === -1) {
                        arr.push(item);
                        states.push({
                            'id': item.longName,
                            'label': item.longName
                        });
                    }
        });
        def.resolve(states);
        return def;
    };
    return {
        buildStates: _buildStates
    };
}]);


xucServices.factory('XucQueue',['$timeout','$rootScope',function($timeout, $rootScope){
    return Xuc.Queue($timeout, $rootScope);
}]);

var Xuc = {};

Xuc.Queue = function($timeout, $rootScope) {
    'use strict';
    var NoLongestWaitTime = "-";
    var updateQueueFrequency = 5;

    var queues = [];
    var loadQueues = function() {
        Cti.getList("queue");
        subscribeToQueueStats();
    };
    var subscribeToQueueStats = function() {
        Cti.subscribeToQueueStats();
    };

    var _queueReplace = function(queueConfig) {
        var replaced = false;
        angular.forEach(queues, function(queue, key) {
            if (queue.id === queueConfig.id) {
                angular.forEach(queueConfig, function(value, property) {
                    queue[property] = value;
                });
                replaced = true;
            }
        });
        return replaced;
    };

    this.onQueueConfig = function(queueConfig) {
        queueConfig.LongestWaitTime = NoLongestWaitTime;
        queueConfig.WaitingCalls = 0;
        var replaced = _queueReplace(queueConfig);
        if (_queueReplace(queueConfig) === false) {
            queues.splice(1, 0, queueConfig);
        }
    };

    this.onQueueList = function(queueList) {
        for (var i = 0; i < queueList.length; i++) {
            this.onQueueConfig(queueList[i]);
        }
        $rootScope.$broadcast('QueuesLoaded');
    };
    this.onQueueStatistics = function(queueStatistics) {
        var updateCounters = function(counters, queue) {
            angular.forEach(counters, function(counter, key) {
                queue[counter.statName] = counter.value;
            });
            if (queue.WaitingCalls === 0) {
                queue.LongestWaitTime = NoLongestWaitTime;
            }
        };
        var queueSelector = function(i) {return queues[i].id === queueStatistics.queueId;};
        if (typeof queueStatistics.queueId === 'undefined') {
            queueSelector = function(i) {return queues[i].name === queueStatistics.queueRef;};
        }
        for (var j = 0; j < queues.length; j++) {
            if (queueSelector(j)) {
                updateCounters(queueStatistics.counters,queues[j]);
            }
        }
        $rootScope.$broadcast('QueueStatsUpdated');
    };

    this.start = function() {
        loadQueues();
    };

    this.updateQueues = function(updateQueueFrequency) {
        for (var i = 0; i < queues.length; i++) {
            if (queues[i].LongestWaitTime >= 0 && queues[i].WaitingCalls !== 0) {
                queues[i].LongestWaitTime += updateQueueFrequency;
            } else {
                queues[i].LongestWaitTime = "-";
            }
        }
    };

    this.updateLongestWaitTime = function() {
        this.updateQueues(updateQueueFrequency);
        $timeout(this.updateLongestWaitTime.bind(this),updateQueueFrequency*1000,false);
    };
    $timeout(this.updateLongestWaitTime.bind(this),updateQueueFrequency*1000,false);

    this.getQueue = function(queueId) {
        for (var j = 0; j < queues.length; j++) {
            if (queues[j].id === queueId) {
                return queues[j];
            }
        }
    };
    this.getQueueByIds = function(ids) {
        var qFilter =  function(queue) {
            return (ids.indexOf(queue.id) >= 0);
        };
        return queues.filter(qFilter);
    };

    this.getQueues = function() {
        return queues;
    };

    this.getQueuesExcept = function(excludeQueues) {
        var acceptQueue = function(queue) {
            for (var j = 0; j < excludeQueues.length; j++) {
                if (excludeQueues[j].id === queue.id) {
                    return false;
                }
            }
            return true;
        };
        return queues.filter(acceptQueue);
    };

    Cti.setHandler(Cti.MessageType.QUEUECONFIG, this.onQueueConfig.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUELIST, this.onQueueList.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, this.onQueueStatistics.bind(this));
    return this;
};

xucServices.factory('XucAgent',
    ['$rootScope','XucQueue','$filter','$timeout',
    function($rootScope, xucQueue, $filter, $timeout){
    return Xuc_Agent($rootScope, xucQueue, $filter, $timeout);
}]);
var Xuc_Agent = function($rootScope, xucQueue, $filter, $timeout) {
    var agents = [];
    var agentStatesWatingForConfig = [];
    var userStatuses = [];
    var agentStateTimer = 3;
    var notReadyStatuses = [];
    var readyStatuses = [];

    var buildAgent = function(id) {
        return {
            'id' : id,
            'lastName' : '',
            'firstName' : '',
            'queueMembers' : [],
            'state' : 'AgentLoggedOut',
            'status' : '-',
            'stats' : {}
        };
    };
    var newAgent = function(id) {
        var agent = buildAgent(id);
        agents.push(agent);
        return(agent);
    };
    var newAgentStatesWatingForConfig = function(id) {
        var agent = buildAgent(id);
        agentStatesWatingForConfig.push(agent);
        return(agent);
    };

    var updateAgent = function(agentUpdate,agent) {
        angular.forEach(agentUpdate, function(value, property) {
            agent[property] = value;
        });
        $rootScope.$broadcast('AgentConfigUpdated', agent.id);
    };

    var updateQueueMember = function(queueMember) {
        var agent = this.getAgent(queueMember.agentId) || newAgentStatesWatingForConfig(queueMember.agentId);
        if (queueMember.penalty >= 0) {
            agent.queueMembers[queueMember.queueId] = {};
            agent.queueMembers[queueMember.queueId] = queueMember.penalty;
        }
        else {
            delete agent.queueMembers[queueMember.queueId];
        }
    };


    var statusActionsHas = function(actions, actionName) {
        var hasAction = false;
        angular.forEach(actions, function (action, key) {
            if(action.name === actionName) {
                hasAction = true;
            }
        });
        return hasAction;
    };

    var statusActionIsNotready = function(actions) {
        return statusActionsHas(actions, "queuepause_all");
    };
    var statusActionIsReady = function(actions) {
        return statusActionsHas(actions, "queueunpause_all");
    };

    this.onUserStatuses = function(statuses) {
        angular.forEach(statuses, function (userStatus, keyUserStatus) {
            userStatuses[userStatus.name] = userStatus.longName;

            if(statusActionIsNotready(userStatus.actions)) {
                notReadyStatuses.push(userStatus);
            }
            if(statusActionIsReady(userStatus.actions)) {
                readyStatuses.push(userStatus);
            }
        });
        $rootScope.$broadcast('AgentUserStatusesLoaded');
    };
    this.getNotReadyStatuses = function() {
        return notReadyStatuses;
    };
    this.getReadyStatuses = function() {
        return readyStatuses;
    };
    this.getUserStatusDisplayName = function(name) {
        return (userStatuses[name] || "");
    };
    this.getAgents = function() {
        return agents;
    };
    this.getAgentsInGroup = function(idOfGroup) {
        return agents.filter(function(agent) { return agent.groupId == idOfGroup; });
    };


    this.getAgent = function(agentId) {
        for (var j = 0; j < agents.length; j++) {
            if (agents[j].id === agentId) {
                return agents[j];
            }
        }
        return null;
    };
    this.getAndCopyAgentStatesWaitingForConfig = function(agentId) {
        for (var j = 0; j < agentStatesWatingForConfig.length; j++) {
            if (agentStatesWatingForConfig[j].id === agentId) {
                var ag = agentStatesWatingForConfig[j];
                agents.push(ag);
                agentStatesWatingForConfig.splice(j,1);
                return ag;
            }
        }
        return null;
    };
    this.getAgentsNotInQueue = function(queueId) {
        var ags = [];
        angular.forEach(agents, function(agent, index) {
            if(typeof agent.queueMembers[queueId] === 'undefined') {
                ags.push(agent);
            }
        });
        return ags;
    };
    this.getAgentsInQueue = function(queueId) {
        var ags = [];
        angular.forEach(agents, function(agent, index) {
            if(typeof agent.queueMembers[queueId] !== 'undefined') {
                ags.push(agent);
            }
        });
        return ags;
    };
    this._getAgentsInQueues = function(filter) {
        var agentsInQueues = [];

        var queues = filter();

        angular.forEach(queues, function(queue, index) {
            angular.forEach(this.getAgentsInQueue(queue.id), function(agent,index){
                if (agentsInQueues.indexOf(agent) < 0) agentsInQueues.push(agent);
            });
        });
        return agentsInQueues;
    };
    this.getAgentsInQueues = function(queueName) {
        var qFilter = function() {
            var queues = xucQueue.getQueues();
            queues = $filter('filter')(queues, { name :queueName});
            return queues;
        };
        return this._getAgentsInQueues(qFilter);
    };

    this.getAgentsInQueueByIds = function(ids) {

        var qFilter = function() {
            return xucQueue.getQueueByIds(ids);
        };
        return this._getAgentsInQueues(qFilter);
    };

    this.onAgentList  = function(agentList) {
        for (var i = 0; i < agentList.length; i++) {
            this.onAgentConfig(agentList[i]);
        }
        angular.forEach(agents, function(agent, index) {
            if(agent.firstName === '') {
                agents.splice(index,1);
            }
        });
        $rootScope.$broadcast('AgentsLoaded');
        Cti.subscribeToAgentStats();
    };
    this.onAgentConfig = function(agentConfig) {
        var agent = this.getAgent(agentConfig.id) || (getAndCopyAgentStatesWaitingForConfig(agentConfig.id) || newAgent(agentConfig.id));
        updateAgent(agentConfig,agent);
    };
    this.buildMoment = function(since) {
        var start = moment().add('seconds', -(since));
        return {
            'momentStart' : start.toDate(),
            'timeInState' : moment().countdown(start.toDate())
        };
    };

    var _buildState = function(agentState) {
        var state = agentState.name;
        state = agentState.acd ? 'AgentOnAcdCall': agentState.name;
        if (agentState.direction === 'Incoming' && !agentState.acd) {
            state = 'AgentOnIncomingCall';
        }
        if (agentState.direction === 'Outgoing') {
            state = 'AgentOnOutgoingCall';
        }
        return state;
    };

    var _isANotReadyStatus = function(statusLongName) {
        var found = false;
        angular.forEach(notReadyStatuses, function(status, key) {
            if (status.longName === statusLongName) found = true;
        });
        return found;
    };
    var _buildStateName = function(agent) {
        if (agent.state === 'AgentOnPause' && agent.status !== '') {
            if (_isANotReadyStatus(agent.status))
            return agent.status;
            else return agent.state;
        }
        else {
            return agent.state;
        }
    };
    this.onAgentState = function(agentState) {

        var agent = this.getAgent(agentState.agentId) || newAgentStatesWatingForConfig(agentState.agentId);
        agent.status = userStatuses[agentState.cause] || '';
        agent.state = _buildState(agentState);
        agent.stateName = _buildStateName(agent);
        agent.phoneNb =  agentState.phoneNb || '';
        agent.phoneNb = (agent.phoneNb === 'null' ? '' : agent.phoneNb);
        agent.queues = agentState.queues;
        if (agentState.name === "AgentLoggedOut") {
            agent.timeInState = undefined;
            agent.momentStart = undefined;
        }
        else {
            var mt = this.buildMoment(agentState.since);
            agent.momentStart = mt.momentStart;
            agent.timeInState = mt.timeInState;
        }
        $rootScope.$broadcast('AgentStateUpdated',agent.id);
    };

    this.onQueueMember = function(queueMember) {
        updateQueueMember(queueMember);
        $rootScope.$broadcast('QueueMemberUpdated', queueMember.queueId);
    };

    this.onQueueMemberList = function(queueMemberList) {
        var queueIds = [];
        for (var i = 0; i < queueMemberList.length; i++) {
            updateQueueMember(queueMemberList[i]);
            queueIds[queueMemberList[i].queueId] = true;
        }
        angular.forEach(queueIds, function(done, queueId) {
            $rootScope.$broadcast('QueueMemberUpdated', queueId);
        });
    };

    // timeout
    this.updateAgentStates = function() {
        var nowMt = moment();
        angular.forEach(agents, function(value, key) {
            if (agents[key].state !== "AgentLoggedOut") {
                agents[key].timeInState = nowMt.countdown(agents[key].momentStart);
            }
        });
        $timeout(this.updateAgentStates.bind(this), agentStateTimer * 1000, false);
    };
    $timeout(this.updateAgentStates.bind(this), 1000, false);

    this.onAgentStatistics = function(agentStatistics) {
        var agent = this.getAgent(agentStatistics.id);
        angular.forEach(agentStatistics.statistics, function(statistic, key) {
            agent.stats[statistic.name] = statistic.value;
        });
        $rootScope.$broadcast('AgentStatisticsUpdated');
    };

    this.start = function() {
        console.log('********************** agent service starting *********************');
        Cti.getList("agent");
        Cti.getList("queuemember");
        Cti.getAgentStates();
        Cti.subscribeToAgentEvents();
    };

    this.canLogIn = function(agentId){
        return (this.getAgent(agentId).state === 'AgentLoggedOut');
    };

    this.login = function(agentId) {
        Cti.loginAgent(this.getAgent(agentId).phoneNb,agentId);
    };

    this.canLogOut = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentReady' || this.getAgent(agentId).state === 'AgentOnPause');
    };
    this.logout = function(agentId) {
        Cti.logoutAgent(agentId);
    };
    this.canPause = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentReady');
    };
    this.pause = function(agentId) {
        Cti.pauseAgent(agentId);
    };
    this.canUnPause = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentOnPause');
    };
    this.unpause = function(agentId) {
        Cti.unpauseAgent(agentId);
    };
    this.canListen = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentOnAcdCall' ||
                        this.getAgent(agentId).state === 'AgentOnCall' ||
                        this.getAgent(agentId).state === 'AgentOnIncomingCall' ||
                        this.getAgent(agentId).state === 'AgentOnOutgoingCall');
    };
    this.listen = function(agentId) {
        Cti.listenAgent(agentId);
    };
    this.updateQueues = function(agentId, updatedQueues) {
        var agent = this.getAgent(agentId);
        var updateQueue = function(queueToUpdate) {
            if(queueToUpdate.penalty !== agent.queueMembers[queueToUpdate.id]) {
                Cti.setAgentQueue(agentId,queueToUpdate.id,queueToUpdate.penalty);
            }
        };
        var updateOrRemoveQueue = function(queueId) {
            for (var j = 0; j < updatedQueues.length; j++) {
                if (updatedQueues[j].id === queueId) {
                  updateQueue(updatedQueues[j]);
                  updatedQueues.splice(j,1);
                  return;
                }
            }
            Cti.removeAgentFromQueue(agentId,queueId);
        };
        var addAgentToQueues = function() {
            for (var j = 0; j < updatedQueues.length; j++) {
                if (typeof agent.queueMembers[updatedQueues[j].id] === 'undefined')
                    Cti.setAgentQueue(agentId,updatedQueues[j].id,updatedQueues[j].penalty);
            }
        };
        angular.forEach(agent.queueMembers, function (penalty, queueId) {
            updateOrRemoveQueue(queueId);
        });
        addAgentToQueues();

    };

    Cti.setHandler(Cti.MessageType.AGENTCONFIG, this.onAgentConfig.bind(this));
    Cti.setHandler(Cti.MessageType.AGENTLIST, this.onAgentList.bind(this));
    Cti.setHandler(Cti.MessageType.USERSTATUSES, this.onUserStatuses.bind(this));
    Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, this.onAgentState.bind(this));
    Cti.setHandler(Cti.MessageType.AGENTSTATISTICS, this.onAgentStatistics.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUEMEMBER, this.onQueueMember.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUEMEMBERLIST, this.onQueueMemberList.bind(this));

    return this;
};

xucServices.factory('XucGroup',['$rootScope','XucAgent','$filter','$log',function($rootScope, xucAgent, $filter, $log){
    return Xuc_Group($rootScope, xucAgent, $filter, $log);
}]);
var Xuc_Group = function($rootScope, xucAgent, $filter, $log) {
    var MAXPENALTY = 20;

    var groups = [];

    var _onAgentGroupList = function(agentGroups) {
        groups = agentGroups;
    };

    var _getGroups = function() {
        return groups;
    };

    var _getGroup = function(groupId) {
        for (var j = 0; j < groups.length; j++) {
            if (groups[j].id === groupId) {
                return groups[j];
            }
        }
        return null;
    };
    var _getAvailableGroups = function(queueId, penalty) {
        var availableGroups = [];
        var availableAgents = xucAgent.getAgentsNotInQueue(queueId);

        angular.forEach(availableAgents, function(agent, key) {
            var group = _getGroup(agent.groupId);
            if (this.indexOf(group) <0) this.push(_getGroup(agent.groupId));
        }, availableGroups);

        return availableGroups;
    };
    var _getGroupsForAQueue = function(queueId) {
        var queueGroups = [];
        var agents = xucAgent.getAgentsInQueue(queueId);

        var initGroup = function(penalty) {
            if (typeof queueGroups[penalty] === 'undefined') queueGroups[penalty] = {'penalty' : penalty, 'groups' : []};
            if (!queueGroups[penalty-1] && penalty > 0) initGroup(penalty-1);
        };
        var getGroup = function(penalty) {
            if (typeof queueGroups[penalty] === 'undefined') initGroup(penalty);
            return queueGroups[penalty];
        };
        var getVGroup = function(groupOfGroup, groupId) {
            for (var j = 0; j < groupOfGroup.groups.length; j++) {
                if (groupOfGroup.groups[j].id === groupId) {
                    return groupOfGroup.groups[j];
                }
            }
            var group = angular.copy(_getGroup(groupId));
            group.nbOfAgents = 0;
            group.agents = [];
            groupOfGroup.groups.push(group);
            return group;
        };

        var addToGroup = function(groupOfGroup, agent) {
            var group = getVGroup(groupOfGroup, agent.groupId);
            group.nbOfAgents = group.nbOfAgents + 1;
            group.agents.push(agent);
        };

        var buildGroups = function() {
            angular.forEach(agents, function (agent, keyAgent) {
                if (typeof agent.queueMembers[queueId] !== 'undefined' && typeof agent.groupId !== 'undefined') {
                    var groups = getGroup(agent.queueMembers[queueId]);
                    addToGroup(groups,agent);
                    if (agent.queueMembers[queueId]+1 < MAXPENALTY) initGroup(agent.queueMembers[queueId]+1);
                }
            });
        };
        initGroup(0);
        buildGroups();
        return queueGroups;
    };
    var _moveAgentsInGroup = function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty) {
        Cti.moveAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
    };
    var _addAgentsInGroup = function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty) {
        Cti.addAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
    };
    var _removeAgentGroupFromQueueGroup = function(groupId, queueId, penalty) {
        Cti.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);
    };
    var _addAgentsNotInQueueFromGroupTo = function(groupId, queueId, penalty) {
        Cti.addAgentsNotInQueueFromGroupTo(groupId, queueId, penalty);
    };
    var _start = function() {
        Cti.getList("agentgroup");
    };

    Cti.setHandler(Cti.MessageType.AGENTGROUPLIST, _onAgentGroupList);

    return {
        getGroups : _getGroups,
        onAgentGroupList : _onAgentGroupList,
        start : _start,
        getGroup : _getGroup,
        getGroupsForAQueue : _getGroupsForAQueue,
        getAvailableGroups : _getAvailableGroups,
        moveAgentsInGroup : _moveAgentsInGroup,
        addAgentsInGroup : _addAgentsInGroup,
        removeAgentGroupFromQueueGroup: _removeAgentGroupFromQueueGroup,
        addAgentsNotInQueueFromGroupTo: _addAgentsNotInQueueFromGroupTo
    };
};

xucServices.factory('XucConfRoom',['$rootScope',function($rootScope){
    return XucConfRoom($rootScope);
}]);
var XucConfRoom = function($rootScope) {
    var _confRooms = [];
    $rootScope.$on('ctiLoggedOn', function(e) {
        Cti.getConferenceRooms();
    });

    var _confRoomLoaded = function(newConfRooms) {
        _confRooms = newConfRooms;
        $rootScope.$broadcast('ConfRoomsUpdated');
    };

    var _getConferenceRooms = function() {
        return _confRooms;
    };
    Cti.setHandler(Cti.MessageType.CONFERENCES, _confRoomLoaded);

    var _requestConferenceRooms = function() {
        Cti.getConferenceRooms();
    };

    return {
        requestConferenceRooms: _requestConferenceRooms,
        getConferenceRooms : _getConferenceRooms
    };
};

xucServices.factory('XucVoiceMail',['$rootScope',function($rootScope){
    return XucVoiceMail($rootScope);
}]);
var XucVoiceMail = function($rootScope) {
    var voiceMail = {};
    voiceMail.newMessages = 0;
    voiceMail.waitingMessages = 0;
    voiceMail.oldMessages = 0;


    var _onVoiceMailUpdate = function(voiceMailUpdate) {
        voiceMail = angular.copy(voiceMailUpdate);
        $rootScope.$broadcast('voicemailUpdated');
    };

    var _getVoiceMail = function() {
        return voiceMail;
    };

    Cti.setHandler(Cti.MessageType.VOICEMAILSTATUSUPDATE, _onVoiceMailUpdate);

    return {
        onVoiceMailUpdate : _onVoiceMailUpdate,
        getVoiceMail : _getVoiceMail
    };
};

xucServices.factory('XucUser',['$rootScope',function($rootScope){
    return XucUser($rootScope);
}]);
var XucUser = function($rootScope) {
    var user = {};
    user.fullName = '--------';
    user.naFwdEnabled = false;
    user.uncFwdEnabled = false;
    user.busyFwdEnabled = false;

    var _updateForwards = function(userConfig) {
        if(userConfig.naFwdDestination !== null) {
            user.naFwdDestination = userConfig.naFwdDestination;
            user.naFwdEnabled =userConfig.naFwdEnabled;
        }
        if(userConfig.uncFwdDestination !== null) {
            user.uncFwdDestination = userConfig.uncFwdDestination;
            user.uncFwdEnabled =userConfig.uncFwdEnabled;
        }
        if(userConfig.busyFwdDestination !== null) {
            user.busyFwdDestination = userConfig.busyFwdDestination;
            user.busyFwdEnabled =userConfig.busyFwdEnabled;
        }
    };


    var _onUserConfig = function(userConfig) {
        if(userConfig.fullName !== null) {
            user = angular.copy(userConfig);
        }
        _updateForwards(userConfig);
        $rootScope.$broadcast('userConfigUpdated');
    };

    var _getUser = function() {
        return user;
    };

    Cti.setHandler(Cti.MessageType.USERCONFIGUPDATE, _onUserConfig);

    return {
        onUserConfig : _onUserConfig,
        getUser : _getUser
    };
};

xucServices.factory('XucAgentTotal',['XucGroup', function(xucGroup){

    var _get_stat_group = function(stats, groupId) {
        var groupName = xucGroup.getGroup(groupId).name;
        if (!stats[groupName])
            stats[groupName] = {};
        return stats[groupName];
    };
    var _calculate_stat = function(agentStats, stats) {
        angular.forEach(agentStats, function (value, property) {
            if (!isNaN(value)) {
                stats[property] = stats[property] ? stats[property] + value : value;
            }
        });
        return stats;
    };
    var _calculate = function(agents) {
        var stats = {};
        angular.forEach(agents, function (agent, keyAgent) {
            groupStat = _get_stat_group(stats,agent.groupId);
            _calculate_stat(agent.stats, groupStat);
        });
        return stats;
    };
    return {
        calculate:_calculate
    };
}]);

xucServices.factory('XucQueueTotal',[function(){
    return XucQueueTotal();
}]);
var XucQueueTotal = function() {

    var queueStats = {};
    queueStats.sum = {};
    queueStats.max = {};
    queueStats.global = {};

    var _razCalc = function() {

        queueStats.sum.TotalNumberCallsEntered = 0;
        queueStats.sum.TotalNumberCallsAnswered = 0;
        queueStats.sum.TotalNumberCallsAbandonned = 0;
        queueStats.sum.TotalNumberCallsClosed = 0;
        queueStats.sum.TotalNumberCallsTimeout = 0;
        queueStats.sum.WaitingCalls = 0;
        queueStats.sum.TotalNumberCallsAnsweredBefore15 = 0;
        queueStats.sum.TotalNumberCallsAbandonnedAfter15 = 0;
        queueStats.global.PercentageAnsweredBefore15 = 0;
        queueStats.global.PercentageAbandonnedAfter15 = 0;
        queueStats.max.LongestWaitTime = 0;
        queueStats.max.EWT = 0;
    };

    var _getCalcStats = function() {
        return queueStats;
    };

    var _calculateSum = function(property, value) {
      if (typeof(queueStats.sum[property]) !== 'undefined') {
        queueStats.sum[property] = queueStats.sum[property] + value;
      }
    };
    var _calculateMax = function(property, value) {
      if (typeof(queueStats.max[property]) !== 'undefined' && !isNaN(parseInt(value)) ) {
        if (value > queueStats.max[property])
          queueStats.max[property] = value;
      }
    };
    var _updateQueueTotals = function(queue) {
        angular.forEach(queue, function(value, property){
            _calculateSum(property, value);
            _calculateMax(property, value);
        });
    };
    var _calculatePercentages = function() {
        if (queueStats.sum.TotalNumberCallsEntered > 0) {
          queueStats.global.PercentageAnsweredBefore15 = _percent(queueStats.sum.TotalNumberCallsAnsweredBefore15,queueStats.sum.TotalNumberCallsEntered);
          queueStats.global.PercentageAbandonnedAfter15 = _percent(queueStats.sum.TotalNumberCallsAbandonnedAfter15,queueStats.sum.TotalNumberCallsEntered);
        }
    };

    var _percent = function(nom, denom) {
        var percentage = 0;
        if (denom > 0) {
           percentage = (nom/denom)*100;
           if (percentage > 100) percentage = 100;
        }
        return percentage;
    };

    var _calculate = function(queues) {
        _razCalc();
        angular.forEach(queues, function(queue, key){
          _updateQueueTotals(queue);
        });
        _calculatePercentages();
    };
    _razCalc();

    return {
        getCalcStats : _getCalcStats,
        calculate : _calculate
    };


};

xucServices.factory('XucDirectory',['$rootScope','$filter',function($rootScope,$filter){
    return XucDirectory($rootScope,$filter);
}]);
var XucDirectory = function($rootScope, $filter) {
    var searchResult = [];
    var favorites = [];
    var headers = [];

    var _onSearchResult = function(result) {
        searchResult = angular.copy(result.entries);
        _setHeaders(result);
        $rootScope.$broadcast('searchResultUpdated');
    };

    var _onFavorites = function(newFavorites) {
        favorites = angular.copy(newFavorites.entries);
        _setHeaders(newFavorites);
        $rootScope.$broadcast('favoritesUpdated');
    };

    var _setHeaders = function(result) {
        headers = angular.copy(result.headers);
        if(headers.indexOf("Favoris") != -1) {
            headers.splice(-3, 2);
        }
    };

    var _onFavoriteUpdated = function(update) {
        if (update.action === "Removed" || update.action === "Added") {
            console.log("Favorite updated: ", update);
            searchResult.forEach(function (elem) {
                if (elem.contact_id == update.contact_id && elem.source == update.source) {
                    elem.favorite = ! elem.favorite;
                }
            });
        }
        else {
            console.log("SERVER ERROR: Favorite update failed: ", update);
        }
        _updateFavorites(update);
        $rootScope.$broadcast('searchResultUpdated');
    };

    var _updateFavorites = function(update) {
        console.log("Should update favorites with ", update);
        if (update.action === "Removed") {
            var result = _find(favorites, update);
            if (result.index >= 0) {
                console.log("Removing from favorites ", favorites[result.index]);
                favorites.splice(result.index, 1);
            }
        }
        else if (update.action == "Added") {
            var newFavorite = _find(searchResult, update);
            console.log("Adding new favorite ", newFavorite.elem);
            favorites.push(newFavorite.elem);
        }
        $rootScope.$broadcast('favoritesUpdated');
    };

    var _find = function(contacts, update) {
        var result;
        contacts.forEach(function(elem, i, a) {
            if (elem.contact_id === update.contact_id && elem.source === update.source) {
                result = {'elem': elem, 'index': i};
            }
        });
        return result;
    };

    var _getSearchResult = function() {
        return searchResult;
    };

    var _getFavorites = function() {
        return favorites;
    };

    var _getHeaders = function() {
        return headers;
    };

    Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, _onSearchResult);
    Cti.setHandler(Cti.MessageType.FAVORITES, _onFavorites);
    Cti.setHandler(Cti.MessageType.FAVORITEUPDATED, _onFavoriteUpdated);

    return {
        onSearchResult : _onSearchResult,
        onFavorites : _onFavorites,
        onFavoriteUpdated : _onFavoriteUpdated,
        getSearchResult : _getSearchResult,
        getFavorites : _getFavorites,
        getHeaders : _getHeaders
    };
};

xucServices.factory('XucCallback',['$rootScope','$translate', 'XucQueue', 'XucAgent', function($rootScope, $translate, xucQueue, xucAgent) {
    return XucCallback($rootScope, $translate, xucQueue, xucAgent);
}]);

var XucCallback = function($rootScope, $translate, xucQueue, xucAgent) {
    var callbackLists = [];

    var _onCallbackLists = function(lists) {
        mixWithQueuesAndAgents(lists);
        callbackLists = lists;
        $rootScope.$broadcast('CallbacksLoaded');
    };

    var _callbackByUuid = function(uuid) {
        for(var i=0; i<callbackLists.length; i++) {
            for(var j=0; j<callbackLists[i].callbacks.length; j++) {
                if(callbackLists[i].callbacks[j].uuid == uuid) {
                    return callbackLists[i].callbacks[j];
                }
            }
        }
    };

    var _onCallbackTaken = function(cbTaken) {
        var callback = _callbackByUuid(cbTaken.uuid);
        if(callback) {
            callback.agentId = cbTaken.agentId;
            callback.agent = xucAgent.getAgent(cbTaken.agentId);
        }
        $rootScope.$broadcast('CallbackTaken', cbTaken);
    };

    var _onCallbackReleased = function(cbReleased) {
        var callback = _callbackByUuid(cbReleased.uuid);
        if(callback) {
            callback.agentId = null;
            callback.agent = null;
        }
        $rootScope.$broadcast('CallbackReleased', cbReleased);
    };

    var mixWithQueuesAndAgents = function(lists) {
        for(var i=0; i<lists.length; i++) {
            lists[i].queue = xucQueue.getQueue(lists[i].queueId);
            mixWithAgents(lists[i].callbacks);
        }
    };

    var mixWithAgents = function(requests) {
        for(var i=0; i<requests.length; i++) {
            if(requests[i].agentId)
                requests[i].agent = xucAgent.getAgent(requests[i].agentId);
        }
    };

    var _getCallbackLists = function() {
        return callbackLists;
    };

    var _refreshCallbacks = function() {
        Callback.getCallbackLists();
    };

    var _takeCallback = function(uuid) {
        Callback.takeCallback(uuid);
    };

    var _releaseCallback = function(uuid) {
        Callback.releaseCallback(uuid);
    };

    var _onQueuesLoaded = function(e) {
        Callback.init(Cti);
        Callback.getCallbackLists();
    };

    var _startCallback = function(uuid, number) {
        Callback.startCallback(uuid, number);
    };

    var _onCallbackStarted = function(cbStarted) {
        $rootScope.$broadcast('CallbackStarted', cbStarted);
    };

    var _updateCallbackTicket = function(ticket) {
        Callback.updateCallbackTicket(ticket.uuid, ticket.status, ticket.comment);
    };

    var _onCallbackClotured = function(cbClotured) {
        _removeCallbackByUuid(cbClotured.uuid);
        $rootScope.$broadcast('CallbackClotured', cbClotured);
    };

    var _removeCallbackByUuid = function(uuid) {
        for(var i=0; i<callbackLists.length; i++) {
            for(var j=0; j<callbackLists[i].callbacks.length; j++) {
                if(callbackLists[i].callbacks[j].uuid == uuid) {
                    callbackLists[i].callbacks.splice(j, 1);
                }
            }
        }
    };

    $rootScope.$on('QueuesLoaded', _onQueuesLoaded);

    Cti.setHandler(Callback.MessageType.CALLBACKLISTS, _onCallbackLists);
    Cti.setHandler(Callback.MessageType.CALLBACKTAKEN, _onCallbackTaken);
    Cti.setHandler(Callback.MessageType.CALLBACKRELEASED, _onCallbackReleased);
    Cti.setHandler(Callback.MessageType.CALLBACKSTARTED, _onCallbackStarted);
    Cti.setHandler(Callback.MessageType.CALLBACKCLOTURED, _onCallbackClotured);

    var _start = function() {
        xucQueue.start();
    };

    var _statuses = [
        {name: 'NoAnswer', displayName: $translate.instant('NoAnswer')},
        {name: 'Answered', displayName: $translate.instant('Answered')},
        {name: 'Fax', displayName: $translate.instant('Fax')},
        {name: 'Callback', displayName: $translate.instant('Callback')}
    ];

    return {
        getCallbackLists: _getCallbackLists,
        refreshCallbacks: _refreshCallbacks,
        start: _start,
        takeCallback: _takeCallback,
        releaseCallback: _releaseCallback,
        statuses: _statuses,
        _onQueuesLoaded: _onQueuesLoaded,
        _onCallbackLists: _onCallbackLists,
        _onCallbackTaken: _onCallbackTaken,
        _onCallbackReleased: _onCallbackReleased,
        startCallback: _startCallback,
        _onCallbackStarted: _onCallbackStarted,
        updateCallbackTicket: _updateCallbackTicket,
        _onCallbackClotured: _onCallbackClotured
    };
};

xucServices.factory('XucPhoneState',['$rootScope',function($rootScope){
    return XucPhoneState($rootScope);
}]);
var XucPhoneState = function($rootScope) {
    var _state = 'UNKNOWN';

    var _getState = function() {
        return _state;
    };

    var _isPhoneAvailable = function(status) {
        return status === "AVAILABLE";
    };

    var _isPhoneOffHook = function(status) {
        return status === "ONHOLD" ||
               status === "BUSY_AND_RINGING" ||
               status === "CALLING" ||
               status === "BUSY";
    };

    var _statusHandler = function(event) {
        _state = event.status;
        $rootScope.$broadcast('phoneStateUpdated', _state);
    };
    Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, _statusHandler);

    return {
        getState: _getState,
        isPhoneAvailable: _isPhoneAvailable,
        isPhoneOffHook: _isPhoneOffHook,
        _statusHandler: _statusHandler
    };
};

xucServices.factory('XucLink',['$rootScope',function($rootScope){
    return XucLink($rootScope);
}]);
var XucLink = function($rootScope) {
    var _hostAndPort = "";
    var _homeUrl = "/";
    var _redirectToHomeUrl = true;
    var _user = {};
    var _logged = false;
    var _token = null;

    var _loggedOnHandler = function(event) {
        _logged = true;
        $rootScope.$broadcast('ctiLoggedOn', _user);
    };

    var _linkStatusHandler = function(event) {
        console.log("link status : " + event.status);
        if (event.status !== 'opened') {
            _logged = false;
            $rootScope.$broadcast('linkDisConnected');
            if(_redirectToHomeUrl) {
                window.location.replace(_homeUrl+"?error='unable to connect to xuc server'");
            }

        }
    };

    var _setHostAndPort = function(hostAndPort) {
        _hostAndPort = hostAndPort;
    };
    var _setHomeUrl = function(homeUrl) {
        _homeUrl = homeUrl;
    };
    var _setRedirectToHomeUrl = function(value) {
        _redirectToHomeUrl = value;
    };
    var _initCti = function(username, password, phoneNumber) {
        _user.username = username;
        _user.password = password;
        _user.phoneNumber = phoneNumber;
        var wsurl = '';
        if(_token)
            wsurl = "ws://" + _hostAndPort + "/xuc/ctichannel/tokenAuthentication?token=" + _token;
        else
            wsurl = "ws://" + _hostAndPort + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
                                phoneNumber + "&amp;password=" + encodeURIComponent(password);

        console.log("sign in " + username + " " + password + ' to ' + _hostAndPort);
        Cti.WebSocket.init(wsurl, username, phoneNumber);
    };

    var _logout = function() {
        Cti.close();
        _logged = false;
        $rootScope.broadcast("ctiLoggedOut", _user);
        if (_redirectToHomeUrl) {
            console.log("redirecting");
            window.location.replace(_homeUrl);
        }
    };

    var _isLogged = function() {
        return _logged;
    };

    var _setToken = function(token) {
        _token = token;
    };

    Cti.setHandler(Cti.MessageType.LOGGEDON, _loggedOnHandler);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, _linkStatusHandler);

    return {
        setHostAndPort : _setHostAndPort,
        setHomeUrl : _setHomeUrl,
        setRedirectToHomeUrl: _setRedirectToHomeUrl,
        initCti : _initCti,
        logout : _logout,
        isLogged : _isLogged,
        setToken: _setToken
    };
};
