package controllers

import play.api.{Logger, Application}
import play.api.i18n.Lang
import play.api.mvc.{RequestHeader, Controller}

class FallbackLangController extends Controller {
  val logger = Logger(getClass.getName)
  override implicit def request2lang(implicit request: RequestHeader) = {
    import play.api.Play.current
    val selLang = preferred(request.acceptLanguages)
    logger.info(s"Browser langs : ${request.acceptLanguages} selected Lang :  $selLang")
    selLang
  }

  def preferred(langs: Seq[Lang])(implicit app: Application): Lang = {
    val all = Lang.availables

    def collectPreferred(ls: Seq[Lang]) = ls.collectFirst(Function.unlift { lang =>
      all.find(_.satisfies(lang))
    })

    collectPreferred(langs.collect {
      // collect all languages that specified a country code, and strip the country code
      case Lang(l, c) => Lang(l)
    })
      .orElse(all.headOption)
      .getOrElse(Lang.defaultLang)
  }

}
