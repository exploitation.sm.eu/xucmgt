package controllers

import controllers.helpers.PrettyController
import play.api.Logger
import play.api.mvc.Action

object CcCoach extends MainController {
  override val log = Logger(getClass.getName)
  override val loginImage = routes.Assets.at("images/avencallbox.jpg").toString
  override val loginRoute = routes.CcCoach.login
  override val isAgent = false
  val title = "XiVO CC Coach"
  override val loginTitle = "XiVO CC Coach Login"

  val Home = Redirect(routes.CcCoach.connect)
  def index = Action { implicit request => Home }
  def logout = Action { implicit request => Home }

  def login = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => {
        log.error("Login form error, returning for corrections: " + formWithErrors.toString)
        BadRequest(PrettyController.prettify(views.html.xuc.loginview(formWithErrors,loginRoute, loginTitle, loginImage,agent=isAgent)))
      },
      user => {
        log.info(s"$user Logging to cccoach xuc host and port $hostAndPort")
        Ok(PrettyController.prettify(views.html.cccoach.cccoach(title, user, hostAndPort)))
      })
  }
}