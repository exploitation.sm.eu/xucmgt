package controllers

import controllers.helpers.PrettyController
import play.api.Logger
import play.api.mvc.Action

object Agent extends MainController {
  override val log = Logger(getClass.getName)
  override val loginImage = routes.Assets.at("images/avencallbox.jpg").toString
  override val loginRoute = routes.Agent.login
  override val isAgent = true
  val title = "XiVO Agent"
  override val loginTitle = "XiVO Agent Login"


  val Home = Redirect(routes.Agent.connect)
  def index = Action { implicit request => Home }

  def login = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => {
        log.debug("Login form error, returning for corrections: " + formWithErrors.toString)
        BadRequest(PrettyController.prettify(views.html.xuc.loginview(formWithErrors,loginRoute, loginTitle, loginImage,agent=isAgent)))
      },
      user => {
        log.info(s"$user Logging to agent xuc host and port $hostAndPort")
        Ok(PrettyController.prettify(views.html.agent.client(title, user, hostAndPort)))
      })
  }

}