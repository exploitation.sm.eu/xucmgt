package controllers

import controllers.helpers.PrettyController
import play.api.Logger
import play.api.mvc.Action

object CcManager extends MainController {
  override val log = Logger(getClass.getName)
  override val loginImage = routes.Assets.at("images/avencallbox.jpg").toString
  override val loginRoute = routes.CcManager.login
  override val isAgent = false
  val title = "XiVO CC Manager"
  override val loginTitle = "XiVO CC Manager Login"

  val Home = Redirect(routes.CcManager.connect)
  def index = Action { implicit request => Home }
  def logout = Action { implicit request => Home }

  def login = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => {
        log.error("Login form error, returning for corrections: " + formWithErrors.toString)
        BadRequest(PrettyController.prettify(views.html.xuc.loginview(formWithErrors,loginRoute, loginTitle, loginImage,agent=isAgent)))
      },
      user => {
        log.info(s"$user Logging to ccmanager xuc host and port $hostAndPort")
        Ok(PrettyController.prettify(views.html.ccmanager.ccmanager(title, user, hostAndPort)))
      })
  }
}