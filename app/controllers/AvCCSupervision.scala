package controllers

import configuration.Config
import controllers.helpers.PrettyController
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.text
import play.api.data.Forms.tuple
import play.api.i18n.Lang
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.Routes

object AvCcSupervision extends Controller {

  val lang = Lang("fr")
  val title = "Contact Center Supervision"

  val IndexFa = Redirect(routes.AvCcSupervision.indexfa)
  
  def index = Action { IndexFa }

  def indexfa = Action { implicit request =>
    Ok(PrettyController.prettify(views.html.ccsupervision.supervisionfa(s"$title - fa", Config.hostAndPort, lang)))
  }

  def indexaf = Action { implicit request =>
    Ok(PrettyController.prettify(views.html.ccsupervision.supervisionaf(s"$title -af", Config.hostAndPort, lang)))
  }

}