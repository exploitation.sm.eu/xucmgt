package controllers

import configuration.Config
import controllers.helpers.PrettyController
import play.api.i18n.Lang
import play.api.mvc.{Action, Controller}

object CcSupervision extends Controller {

  val lang = Lang("fr")
  val title = "Contact Center Supervision"

  def index = Action { implicit request =>
    Ok(PrettyController.prettify(views.html.ccsupervision.ccsupervision(title, Config.hostAndPort, lang)))

  }

}