package controllers


import configuration.Config
import play.api.Logger
import play.api.mvc.Action

object Pratix extends FallbackLangController {
  val log = Logger(getClass.getName)

  val loginImage = routes.Assets.at("images/avencallbox.jpg").toString

  def connect   = Action(implicit request   => Ok(views.html.xivo.pratix(Config.hostAndPort)))
  def loginPage = Action(implicit request   => Ok(views.html.xivo.login("XiVO Assistant Login", loginImage)))
  def mainPage  = Action(implicit request   => Ok(views.html.xivo.main()))
  def contacts  = Action(implicit request   => Ok(views.html.xivo.contacts()))
  def confroom  = Action(implicit request   => Ok(views.html.xivo.confroom()))
  def history   = Action(implicit request   => Ok(views.html.xivo.history()))
  def sso       = Action(implicit request   => request.getQueryString("token") match {
    case None => Redirect(s"http://${Config.hostAndPort}/xuc/sso?orig=http://${request.host}${request.path}")
    case Some(token) => Ok(views.html.xivo.pratix(Config.hostAndPort, Some(token)))
  })

}
