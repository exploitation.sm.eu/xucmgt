package controllers

import configuration.Config
import controllers.helpers.PrettyController
import models.XucUser
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc.{Action, Call, Controller}
import play.api.Logger

abstract class MainController extends FallbackLangController {
  val log = Logger(getClass.getName)

  val hostAndPort = Config.hostAndPort
  val loginImage:String
  val loginRoute:Call
  val isAgent:Boolean
  val loginTitle = "XiVO Login"

  def connect = Action { implicit request =>
    log.debug("login page")
    Ok(PrettyController.prettify(views.html.xuc.loginview(loginForm, loginRoute, loginTitle, loginImage, agent=isAgent)))
  }

  def loginError(error: String) = Action { implicit request =>
    log.info("Back to login page with error: " + error)
    val loginFormWithErrors = loginForm.withGlobalError(error)
    BadRequest(PrettyController.prettify(views.html.xuc.loginview(loginFormWithErrors,loginRoute, loginTitle, loginImage,agent=isAgent)))
  }

  val loginForm = Form(
    mapping(
      "ctiUsername" -> nonEmptyText,
      "ctiPassword" -> text(minLength = 3),
      "phoneNumber" -> optional(number)
    )(XucUser.apply)(XucUser.unapply)
  )

}
