package controllers

import configuration.Config
import controllers.helpers.PrettyController
import play.api.i18n.Lang
import play.api.mvc.{Action, Controller}

object Supervision extends Controller {

  val lang = Lang("fr")
  val title = "Xivo Supervision Sample"

  def index = Action { implicit request =>
    Ok(PrettyController.prettify(views.html.sample.supervision(title, Config.hostAndPort)))
  }

}