package configuration

import play.api.Play
import play.api.Play.current

object Config {
  val hostAndPort = s"${Play.configuration.getString("xuc.host").get}:${Play.configuration.getString("xuc.port").get}"
  val useSsl = Play.configuration.getBoolean("xuc.useSsl").getOrElse(false)
  def showRecordingControls = Play.configuration.getBoolean("agent.showRecordingControls").getOrElse(true)
  def showCallbacks = Play.configuration.getBoolean("agent.showCallbacks").getOrElse(false)
  def showQueueControls = Play.configuration.getBoolean("agent.showQueueControls").getOrElse(false)
}
