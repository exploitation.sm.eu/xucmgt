import configuration.Config
import play.api.Application
import play.api.GlobalSettings
import play.api.Logger

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    Logger.info("Starting xucmgt")
    Logger.info(s" host and port : ${Config.hostAndPort}")
  }

  override def onStop(app: Application) {
    Logger.debug("Stopping xucmgt")
  }
}
