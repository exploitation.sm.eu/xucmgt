#!/bin/sh
#
### BEGIN INIT INFO
# Provides:          xuc 
# Required-Start:    $local_fs $remote_fs $network $syslog
# Required-Stop:     $local_fs $remote_fs $network $syslog
# Should-Start:
# Should-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Startup daemon script for xuc
### END INIT INFO
#
set -e
USERNAME=xucmgt
PATH=/sbin:/bin:/usr/sbin:/usr/bin
DAEMONNAME=xucmgt
DAEMON=/usr/share/$DAEMONNAME/bin/$DAEMONNAME
PIDFILE=/var/run/$DAEMONNAME/$DAEMONNAME.pid
CONFIG_FILE=${CONFIG_FILE:-"/usr/share/$DAEMONNAME/conf/$DAEMONNAME.conf"}
VERSION_FILE="/usr/share/$DAEMONNAME/conf/$DAEMONNAME.version"
LOGGER_CONF=${LOGGER_CONF:-"/usr/share/$DAEMONNAME/conf/"$DAEMONNAME"_logger.xml"}
PORT=${PORT:-"9010"}
JMX="-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9066 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.local.only=false"
RMI="-Djava.rmi.server.hostname=<changetoipaddress> -Djava.rmi.activation.port=9067"

. /lib/lsb/init-functions


test -x $DAEMON || exit 0

START_DAEMON="--background"
if [ ! -z $NO_DAEMON ]
then
  START_DAEMON=""
fi

# Remove RUNNING_PID if the contained pid is not in fact running
test -e "$PIDFILE" \
  && pid=`cat "$PIDFILE"` \
  && ! ps -p "$pid" 1>/dev/null \
  && rm "$PIDFILE" \
  && echo "Removed zombie RUNNING_PID of previous server with pid $pid."

# export version variable
if [ -r $VERSION_FILE ]
then
  export XUCMGT_VERSION=$(cat $VERSION_FILE)
else
  export XUCMGT_VERSION="VersionFileNotFound"
fi

case "$1" in
  start)
    log_daemon_msg "Starting" "$DAEMONNAME"
    START_CMD="-Dconfig.file=$CONFIG_FILE -Dlogger.file=$LOGGER_CONF -Dhttp.port=$PORT -Dpidfile.path=$PIDFILE"
    if start-stop-daemon --start $START_DAEMON --user $USERNAME --chuid $USERNAME --pidfile $PIDFILE --startas $DAEMON -- ${START_CMD}
    then
        log_end_msg 0
    else
        log_end_msg 1
    fi
    ;;
  stop)
    log_daemon_msg "Stopping" "$DAEMONNAME"
    if start-stop-daemon --stop --quiet --oknodo --retry 5 --user $USERNAME --chuid $USERNAME --pidfile $PIDFILE;
    then
        log_end_msg 0
    else
        log_end_msg 1
    fi
    rm -f $PIDFILE
    ;;
  restart)
    $0 stop
    $0 start
    ;;
  status)
    status_of_proc -p $PIDFILE $DAEMON "$DAEMONNAME" && exit 0 || exit $?
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|status}"
    exit 1
    ;;
esac

exit 0
