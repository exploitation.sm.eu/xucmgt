'use strict';
describe('Xuc service voicemail', function() {
    var xucVoiceMail;
    var $rootScope;

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
        spyOn(Cti,'setHandler');
    });

    beforeEach(module('xuc.services'));

    beforeEach(inject(function(_XucVoiceMail_,_$rootScope_) {
        $rootScope = _$rootScope_;
        xucVoiceMail = _XucVoiceMail_;
        spyOn($rootScope, '$broadcast');
        })
    );


    it('setup handlers when starting', function() {
        var initVoiceMail = {};
        initVoiceMail.newMessages = 0;
        initVoiceMail.waitingMessages =0;
        initVoiceMail.oldMessages = 0;

        expect(Cti.setHandler).toHaveBeenCalled();

        expect(xucVoiceMail.getVoiceMail()).toEqualData(initVoiceMail);
    });

    it('update voicemail on voicemail updatereceived and broadcast voicemailUpdated event', function() {

        var updateVoiceMail = {};
        updateVoiceMail.newMessages = 34;
        updateVoiceMail.waitingMessages =56;
        updateVoiceMail.oldMessages = 12;


        xucVoiceMail.onVoiceMailUpdate(updateVoiceMail);

        expect(xucVoiceMail.getVoiceMail()).toEqualData(updateVoiceMail);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('voicemailUpdated');
    });


});