'use strict';
describe('Xuc service agent total', function() {
    var xucAgentTotal;
    var xucGroup;

    beforeEach(module('xuc.services'));

    beforeEach(inject(function(_XucAgentTotal_, _XucGroup_) {
        xucAgentTotal = _XucAgentTotal_;
        xucGroup = _XucGroup_;
    }));



    it('should calculate stats', function(){
        spyOn(xucGroup,'getGroup').and.returnValue({id:1,name:'agent_group'});

        var noemie = new MockAgentBuilder(58,'Noemie','Ange').inGroup(1).withStat('AgentPausedTotalTime',60).build();
        var john = new MockAgentBuilder(68,'John','Moon').inGroup(1).withStat('AgentPausedTotalTime',40).build();
        var agents=[noemie, john];

        var expected = {};
        expected['agent_group'] = {'AgentPausedTotalTime':100};

        var stats = xucAgentTotal.calculate(agents);

        expect(stats).toEqual(expected);
    });

    it('should only calculate stats for numbers', function() {
        spyOn(xucGroup,'getGroup').and.returnValue({id:1,name:'agent_group'});

        var noemie = new MockAgentBuilder(58,'Noemie','Ange').inGroup(1).withStat('LoginDateTime','2015-05-06 10:10').build();
        var john = new MockAgentBuilder(68,'John','Moon').inGroup(1).withStat('LoginDateTime','2015-05-06 09:00').build();
        var agents=[noemie, john];

        var expected = {};
        expected['agent_group'] = {};

        var stats = xucAgentTotal.calculate(agents);

        expect(stats).toEqual(expected);
    });


});