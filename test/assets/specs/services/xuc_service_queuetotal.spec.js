'use strict';
describe('Xuc service queue total', function() {
    var xucQueueTotal;
    var $rootScope;

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });

    beforeEach(module('xuc.services'));

    beforeEach(inject(function(_XucQueueTotal_) {
        xucQueueTotal = _XucQueueTotal_;
    }));


    it('should return initialzed totals', function() {

        var stats = xucQueueTotal.getCalcStats();

        expect(stats.sum.TotalNumberCallsEntered).toBe(0);
        expect(stats.sum.TotalNumberCallsAnswered).toBe(0);
        expect(stats.sum.TotalNumberCallsAbandonned).toBe(0);
        expect(stats.sum.TotalNumberCallsClosed).toBe(0);
        expect(stats.sum.TotalNumberCallsTimeout).toBe(0);
        expect(stats.sum.WaitingCalls).toBe(0);
        expect(stats.sum.TotalNumberCallsAnsweredBefore15).toBe(0);
        expect(stats.sum.TotalNumberCallsAbandonnedAfter15).toBe(0);
        expect(stats.global.PercentageAnsweredBefore15).toBe(0);
        expect(stats.global.PercentageAbandonnedAfter15).toBe(0);
        expect(stats.max.LongestWaitTime).toBe(0);
        expect(stats.max.EWT).toBe(0);
    });


    it('should accumulate totals for queues', function(){
        var q1 = {"id":1,"WaitingCalls":5,"TotalNumberCallsClosed":2,"TotalNumberCallsEntered":2,"TotalNumberCallsAbandonned":7,"TotalNumberCallsAnswered":4}
        var q2 = {"id":2,"WaitingCalls":4,"TotalNumberCallsClosed":0,"TotalNumberCallsEntered":1,"TotalNumberCallsAbandonned":4,"TotalNumberCallsAnswered":8}

        var queues = [q1,q2];
        xucQueueTotal.calculate(queues);

        expect(xucQueueTotal.getCalcStats().sum.TotalNumberCallsEntered).toBe(3);
    });

    it('should calculate max longestWaitTime for queues ', function(){
      var q1 = {"id":1,"WaitingCalls":5,"LongestWaitTime":2}
      var q2 = {"id":2,"WaitingCalls":4,"LongestWaitTime":'-'}
      var q3 = {"id":1,"WaitingCalls":5,"LongestWaitTime":20}

      var queues = [q1,q2,q3];

      xucQueueTotal.calculate(queues);

      expect(xucQueueTotal.getCalcStats().max.LongestWaitTime).toBe(20);

    });

    it('should reset max longestWaitTime on no longest wait time', function(){
        var q1 = {"id":3,"WaitingCalls":5,"LongestWaitTime":2}
        var q2 = {"id":4,"WaitingCalls":5,"LongestWaitTime":15,}
        var queues = [q1,q2];
        xucQueueTotal.calculate(queues);

        var q1 = {"id":3,"WaitingCalls":5,"LongestWaitTime":'-'}
        var q2 = {"id":4,"WaitingCalls":5,"LongestWaitTime":'-',}

        var queues = [q1,q2];
        xucQueueTotal.calculate(queues);

        expect(xucQueueTotal.getCalcStats().max.LongestWaitTime).toBe(0);

    });
    it('should calculate max EWT for queues ', function(){
      var q1 = {"id":1,"WaitingCalls":5,"EWT":25}
      var q2 = {"id":2,"WaitingCalls":4,"EWT":'0'}
      var q3 = {"id":1,"WaitingCalls":5,"EWT":10}

      var queues = [q1,q2,q3];

      xucQueueTotal.calculate(queues);

      expect(xucQueueTotal.getCalcStats().max.EWT).toBe(25);

    });

    it('should reset totals between updateds', function() {
        var q1 = {"id":1,"WaitingCalls":5,"TotalNumberCallsClosed":2,"TotalNumberCallsEntered":2,"TotalNumberCallsAbandonned":7,"TotalNumberCallsAnswered":4}
        var queues = [q1];
        xucQueueTotal.calculate(queues);

        q1 = {"id":1,"WaitingCalls":2,"TotalNumberCallsClosed":2,"TotalNumberCallsEntered":2,"TotalNumberCallsAbandonned":7,"TotalNumberCallsAnswered":4}
        var queues = [q1];
        xucQueueTotal.calculate(queues);

        expect(xucQueueTotal.getCalcStats().sum['TotalNumberCallsEntered']).toBe(2);
    });

    it('should accumulate totals when only one queue changes', function(){
        var q1 = {"id":11,"WaitingCalls":5,"TotalNumberCallsClosed":2,"TotalNumberCallsEntered":2,"TotalNumberCallsAbandonned":7,"TotalNumberCallsAnswered":4}
        var q2 = {"id":22,"WaitingCalls":4,"TotalNumberCallsClosed":0,"TotalNumberCallsEntered":7,"TotalNumberCallsAbandonned":4,"TotalNumberCallsAnswered":8}
        var queues = [q1,q2];
        xucQueueTotal.calculate(queues);

        queues[0].TotalNumberCallsEntered = 5;
        xucQueueTotal.calculate(queues);

        expect(xucQueueTotal.getCalcStats().sum['TotalNumberCallsEntered']).toBe(12);

    });

    it('should calculate a global percentage of calls answered before 15s', function(){
        var q1 = {"id":54,"TotalNumberCallsEntered":40,"TotalNumberCallsAnsweredBefore15":5}
        var q2 = {"id":55,"TotalNumberCallsEntered":20,"TotalNumberCallsAnsweredBefore15":10}

        var queues = [q1,q2];
        xucQueueTotal.calculate(queues);

        expect(xucQueueTotal.getCalcStats().global.PercentageAnsweredBefore15).toBe(25.0);

    });
    it('should calculate a global percentage of zero if no denomitaor ', function(){
        var q1 = {"id":54,"WaitingCalls":5}

        var queues = [q1];
        xucQueueTotal.calculate(queues);

        expect(xucQueueTotal.getCalcStats().global.PercentageAnsweredBefore15).toBe(0);
        expect(xucQueueTotal.getCalcStats().global.PercentageAbandonnedAfter15).toBe(0);

    });
    it('should max the percentage to 100 if more than 100', function(){
        var q1 = {"id":54,"TotalNumberCallsEntered":2,"TotalNumberCallsAnsweredBefore15":4}
        var queues = [q1];
        xucQueueTotal.calculate(queues);

        expect(xucQueueTotal.getCalcStats().global.PercentageAnsweredBefore15).toBe(100.0);
    });


});