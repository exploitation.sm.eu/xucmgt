'use strict';
describe('Xuc service callback', function() {
    var xucQueue;
    var $translate;
    var $rootScope;

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });

    beforeEach(module('xuc.services'));

    beforeEach(inject(function(_XucQueue_,_$rootScope_,_XucCallback_,_$translate_) {
        xucQueue = _XucQueue_;
        $rootScope = _$rootScope_;
        $translate = _$translate_;
        xucCallback = _XucCallback_;
        spyOn($rootScope, '$broadcast');

    }));

    it('should ask for callback lists when queues are loaded', function() {
        spyOn(Callback, 'init');
        spyOn(Callback, 'getCallbackLists');

        xucCallback._onQueuesLoaded('');

        expect(Callback.init).toHaveBeenCalledWith(Cti);
        expect(Callback.getCallbackLists).toHaveBeenCalled();
    });

    it('should mix callbacks with their queue when they are loaded', function() {
        var queue = QueueBuilder('test', 'Test').build();
        var l1 = CallbackListBuilder('Test', queue.id, []).build();
        var l2 = CallbackListBuilder('Test 2', queue.id, []).build();
        spyOn(xucQueue, 'getQueue').and.returnValue(queue);

        xucCallback._onCallbackLists([l1, l2]);

        expect(xucCallback.getCallbackLists()).toEqual([l1, l2]);
        expect(l1.queue).toEqual(queue);
        expect(l2.queue).toEqual(queue);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbacksLoaded');
    });

    it('should take a callback', function() {
        spyOn(Callback, 'takeCallback');
        var uuid = '12345-abcd';

        xucCallback.takeCallback(uuid);

        expect(Callback.takeCallback).toHaveBeenCalledWith(uuid);
    });

    it('should set the agentId when a callback is taken and broadcast the event', function() {
        var cb1 = CallbackRequestBuilder('1000').build();
        cb1.uuid = '123';
        var cb2 = CallbackRequestBuilder('1001').build();
        cb2.uuid = '456';
        var list = CallbackListBuilder('Test', queue.id, [cb1, cb2]).build();

        xucCallback._onCallbackLists([list]);
        xucCallback._onCallbackTaken({uuid: cb1.uuid, agentId: 12});

        expect(cb1.agentId).toEqual(12);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbackTaken', {uuid: cb1.uuid, agentId: 12});
    });

    it('should release a callback', function() {
        spyOn(Callback, 'releaseCallback');
        var uuid = '12456';

        xucCallback.releaseCallback(uuid);

        expect(Callback.releaseCallback).toHaveBeenCalledWith(uuid);
    });

    it('should unset the agentId when a callback is released and broadcast the event', function() {
        var cb1 = CallbackRequestBuilder('1000').build();
        cb1.uuid = '123';
        cb1.agentId = 12;
        var cb2 = CallbackRequestBuilder('1001').build();
        cb2.uuid = '456';
        var list = CallbackListBuilder('Test', queue.id, [cb1, cb2]).build();

        xucCallback._onCallbackLists([list]);
        xucCallback._onCallbackReleased({uuid: cb1.uuid});

        expect(cb1.agentId).toEqual(null);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbackReleased', {uuid: cb1.uuid});
    });

    it('should start a callback', function() {
        spyOn(Callback, 'startCallback');
        var uuid = '12456';
        var number = '3000';

        xucCallback.startCallback(uuid, number);

        expect(Callback.startCallback).toHaveBeenCalledWith(uuid,number);
    });

    it('should broadcast the event when a callback is started', function() {
        var cbStarted = {uuid: '123456'}
        xucCallback._onCallbackStarted(cbStarted);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbackStarted', cbStarted);
    });

    it('should update a callback', function() {
        spyOn(Callback, 'updateCallbackTicket');
        var uuid = '123456', status = 'Fax', comment = 'The comment';
        var ticket = {
            uuid: uuid,
            status: status,
            comment: comment
        };

        xucCallback.updateCallbackTicket(ticket);

        expect(Callback.updateCallbackTicket).toHaveBeenCalledWith(uuid,status, comment);
    });

    it('should remove a callback when clotured and braodcast the event', function() {
        var cb1 = CallbackRequestBuilder('1000').build();
        cb1.uuid = '123';
        var cb2 = CallbackRequestBuilder('1001').build();
        cb2.uuid = '456';
        var list = CallbackListBuilder('Test', queue.id, [cb1, cb2]).build();

        xucCallback._onCallbackLists([list]);
        xucCallback._onCallbackClotured({uuid: cb1.uuid});

        expect(list.callbacks).toEqual([cb2]);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbackClotured', {uuid: cb1.uuid});
    });
});