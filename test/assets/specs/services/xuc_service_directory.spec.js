'use strict';
describe('Xuc directory', function() {
    var xucDirectory;
    var $rootScope;
    var $filter;

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });

    beforeEach(module('xuc.services'));

    beforeEach(inject(function(_XucDirectory_,_$rootScope_,_$filter_) {
        xucDirectory = _XucDirectory_;
        $rootScope = _$rootScope_;
        $filter = _$filter_;
        spyOn($rootScope, '$broadcast');
    }));

    it('should remove Favoris and Personal from header list', function() {
        var result =
                {"headers": ["Nom","Numéro","Mobile","Autre numéro","Favoris", "Perso", "Email"],
                "entries":[]};
        xucDirectory.onSearchResult(result);
        expect(xucDirectory.getHeaders()).toEqualData(["Nom","Numéro","Mobile","Autre numéro","Email"]);
    });
    it('should not modify header list if Favoris is not in the list', function() {
        var result =
                {"headers": ["Nom","Numéro","Mobile","Autre numéro","Email"],
                "entries":[]};
        xucDirectory.onSearchResult(result);
        expect(xucDirectory.getHeaders()).toEqualData(["Nom","Numéro","Mobile","Autre numéro","Email"]);
    });

    it('should broadcast notification on search result', function() {
        var entries = [{"status":4,"entry":["EXTERNE2","568778","","",false,""],"contact_id":"24","source":"internal","favorite":false},
                       {"status":4,"entry":["EXTERNE1","45654","","",false,""],"contact_id":"25","source":"internal","favorite":false}];
        var result = {"headers":[], "entries": entries};
        xucDirectory.onSearchResult(result);
        expect(xucDirectory.getSearchResult()).toEqualData(entries);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('searchResultUpdated');
    });
    it('should broadcast notification on favorites result', function() {
        var entries = [{"status":4,"entry":["Fav2","568778","","",true,""],"contact_id":"24","source":"internal","favorite":false},
                       {"status":4,"entry":["Fav1","45654","","",true,""],"contact_id":"25","source":"internal","favorite":false}];
        var result = {"headers":[], "entries": entries};
        xucDirectory.onFavorites(result);
        expect(xucDirectory.getFavorites()).toEqualData(entries);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('favoritesUpdated');
    });
    it('should update favorites and broadcast notification on favorites add', function() {
        var entries = [{"status":4,"entry":["EXTERNE2","568778","","",false,""],"contact_id":"13","source":"internal","favorite":false},
                       {"status":4,"entry":["autre","23423","","",false,""],"contact_id":"41","source":"internal","favorite":false} ];
        var result = {"headers":[], "entries": entries};
        var successfulAdd = {"action":"Added","contact_id":"13","source":"internal"};
        xucDirectory.onSearchResult(result);
        expect(xucDirectory.getFavorites()).toEqualData([]);
        xucDirectory.onFavoriteUpdated(successfulAdd);
        entries[0].favorite = true;
        expect(xucDirectory.getFavorites()).toEqualData([entries[0]]);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('favoritesUpdated');
    });
    it('should update favorites and  broadcast notification on favorites remove', function() {
        var entries = [{"status":4,"entry":["fav2","568778","","",true,""],"contact_id":"13","source":"internal","favorite":true},
                       {"status":4,"entry":["fav1","4564","","",true,""],"contact_id":"32","source":"internal","favorite":true}];
        var result = {"headers":[], "entries": entries};
        var successfulAdd = {"action":"Removed","contact_id":"13","source":"internal"};
        xucDirectory.onFavorites(result);
        expect(xucDirectory.getFavorites()).toEqualData(entries);
        xucDirectory.onFavoriteUpdated(successfulAdd);
        expect(xucDirectory.getFavorites()).toEqualData([entries[1]]);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('favoritesUpdated');
    });

});
