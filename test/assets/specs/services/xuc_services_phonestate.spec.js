'use strict';
describe('Xuc phoneState', function() {
    var phoneState;
    var $rootScope;
    var $filter;

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });

    beforeEach(module('xuc.services'));

    beforeEach(inject(function(_XucPhoneState_,_$rootScope_,_$filter_) {
        phoneState = _XucPhoneState_;
        $rootScope = _$rootScope_;
        $filter = _$filter_;
        spyOn($rootScope, '$broadcast');
    }));

    it('should save state on phoneStateUpdate', function() {
        var event = {event: Cti.MessageType.PHONESTATUSUPDATE, status: 'NEW_STATE'};
        phoneState._statusHandler(event);
        expect(phoneState.getState()).toEqualData('NEW_STATE');
    });

    it('should broadcast notification on phoneStateUpdate', function() {
        var event = {event: Cti.MessageType.PHONESTATUSUPDATE, status: 'RINGING'};
        phoneState._statusHandler(event);
        expect($rootScope.$broadcast).toHaveBeenCalledWith('phoneStateUpdated', 'RINGING');
    });
    it('function isPhoneAvailable should return true if called with phone state "AVAILABLE"', function() {
        expect(phoneState.isPhoneAvailable('AVAILABLE')).toEqualData(true);
        expect(phoneState.isPhoneAvailable('RINGING')).toEqualData(false);
    });
    it('function isPhoneOffHook should return true if called with one of phone states off hook', function() {
        expect(phoneState.isPhoneOffHook('AVAILABLE')).toEqualData(false);
        expect(phoneState.isPhoneOffHook('ONHOLD')).toEqualData(true);
        expect(phoneState.isPhoneOffHook('BUSY_AND_RINGING')).toEqualData(true);
        expect(phoneState.isPhoneOffHook('CALLING')).toEqualData(true);
        expect(phoneState.isPhoneOffHook('BUSY')).toEqualData(true);
        expect(phoneState.isPhoneOffHook('QWERTY')).toEqualData(false);
    });

});
