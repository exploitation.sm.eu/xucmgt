describe('filter', function() {

  beforeEach(module('xuc.services'));

  describe('totalTime filter', function() {

    it('should convers seconds to a totalTime display',
        inject(function(totalTimeFilter) {

          expect(totalTimeFilter(0)).toBe('00:00:00');
          expect(totalTimeFilter(10)).toBe('00:00:10');
          expect(totalTimeFilter(130)).toBe('00:02:10');
          expect(totalTimeFilter(3730)).toBe('01:02:10');
          expect(totalTimeFilter(99*3600)).toBe('99:00:00');
          expect(totalTimeFilter(100*3600)).toBe('100:00:00');
          expect(totalTimeFilter(100*3600 + 59*60 + 59)).toBe('100:59:59');
          expect(totalTimeFilter(1000*3600)).toBe('###:##:##');

        }));
  });
});