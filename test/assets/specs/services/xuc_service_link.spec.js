'use strict';
describe('Xuc service link', function() {
    var xucLink;
    var rootScope;

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });

    beforeEach(module('xuc.services'));

    beforeEach(inject(function(_XucLink_, _$rootScope_) {
        xucLink = _XucLink_;
        rootScope = _$rootScope_;
        spyOn(rootScope, '$broadcast');
    }));


    it('should be able to start cti', function() {
        spyOn(Cti.WebSocket,'init');

        xucLink.setHostAndPort('host:8090');

        xucLink.initCti("john","doe",44500);

        var wsurl = "ws://host:8090/xuc/ctichannel?username=john&amp;agentNumber=44500&amp;password=doe";

        expect(Cti.WebSocket.init).toHaveBeenCalledWith(wsurl, "john", 44500);
    });

    it('should broadcast ctiLoggedOn and set logged to true on user login', function() {
       Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
       expect(xucLink.isLogged()).toEqual(true);
       expect(rootScope.$broadcast).toHaveBeenCalledWith('ctiLoggedOn', {});
    });

});
