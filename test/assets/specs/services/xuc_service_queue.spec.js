'use strict';
describe('Xuc service', function() {
    var xucQueue;
    var $rootScope;

    var QueueBuilder = function(id,name) {
        this.queue = {};
        this.queue.name = name;
        this.queue.displayName = '';
        this.queue.id = id;
        this.stats = {};
        this.stats.queueId = id;
        this.stats.counters = [];

        this.withStat = function(statName,value) {
            var stat = {};
            stat.statName = statName;
            stat.value = value;
            this.stats.counters.push(stat);
            return this;
        };
        this.withDisplayName = function(displayName) {
            this.queue.displayName = displayName;
            return this;
        };
        this.build = function() {
            xucQueue.onQueueConfig(this.queue);
            xucQueue.onQueueStatistics(this.stats);
            return this.queue;
        };
        return this;
    };

    var storedQueue = function(queueId) {
        for (var j = 0; j < xucQueue.getQueues().length; j++) {
            if (xucQueue.getQueues()[j].id === queueId) {
                return xucQueue.getQueues()[j];
            }
        }
    };
    beforeEach(module('xuc.services'));

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData,
            statEqual: customMatchers.statEqual
        });
    });

    beforeEach(inject(function(_XucQueue_,_$rootScope_) {
        xucQueue = _XucQueue_;
        $rootScope = _$rootScope_;
        spyOn($rootScope, '$broadcast');
    }));

    it('should have empty queues at init', function() {

        var queueConfig = {};

        expect(xucQueue.getQueues()).toEqualData([]);
    });

    it('should init queue with longestWaittime and waiting calls on configuration on received', function(){
        var queueConfig = {'id':3};
        xucQueue.onQueueConfig(queueConfig);

        expect(storedQueue(3)).statEqual('LongestWaitTime','-');
        expect(storedQueue(3)).statEqual('WaitingCalls',0);
    });

    it('should update queue config', function() {
        var queueConfig = {'id':7, 'name' : 'Hello'};
        var updatedQueueConfig = {'id':7, 'name' : 'Bonjour'};
        var expectedStoredConfig = {'id':7,'name':'Bonjour','LongestWaitTime': '-', 'WaitingCalls':0};

        xucQueue.onQueueConfig(queueConfig);
        xucQueue.onQueueConfig(updatedQueueConfig);

        expect(storedQueue(7).name).toBe('Bonjour');
    });
    it('should update queue config on queue list received and broadcast a loaded event', function() {
        var queueList = [{'id':17, 'name' : 'Hello'}];
        var expectedStoredConfig = {'id':7,'name':'Bonjour','LongestWaitTime': '-', 'WaitingCalls':0};

        xucQueue.onQueueList(queueList);

        expect(storedQueue(17).name).toBe('Hello');
        expect($rootScope.$broadcast).toHaveBeenCalledWith('QueuesLoaded');

    });

    it('should update queue statistics by id', function() {
        new QueueBuilder(78,'BlueIsland')
            .build();

        var statistics = {'queueId':78, 'counters':[{'statName':'TalkingAgents', 'value' : 8}]};

        xucQueue.onQueueStatistics(statistics);

        expect(storedQueue(78)).statEqual('TalkingAgents',8);
    });
    it('should update queue statistics by reference (name)', function() {
        new QueueBuilder(32,'Yellow')
            .build();

        var statistics = {'queueRef':'Yellow', 'counters':[{'statName':'TotalNumberCallsAbandonned', 'value' : 4}]};

        xucQueue.onQueueStatistics(statistics);

        expect(storedQueue(32)).statEqual('TotalNumberCallsAbandonned',4);
    });
    it('should reset LongestWaitTime when WaitingCalls goes back to 0', function(){

        new QueueBuilder(55,'Orange')
            .withStat('WaitingCalls',10)
            .withStat('LongestWaitTime' , 520)
            .build();

        var statistics = {'queueId':55, 'counters':[{'statName':'WaitingCalls', 'value' : 0}]};

        xucQueue.onQueueStatistics(statistics);

        expect(storedQueue(55)).statEqual('LongestWaitTime','-');
    });
    it('should return a queue by id', function(){
        new QueueBuilder(55,'Orange')
        .build();

        var queue = xucQueue.getQueue(55);
        expect(queue.name).toBe('Orange');
    });
    it('should return a list of queues excluding a list of queues', function(){
        new QueueBuilder(55,'Orange')
        .build();
        new QueueBuilder(44,'Blue')
        .build();
        new QueueBuilder(37,'Yellow')
        .build();

        var queue = {'id':44};
        var excludeQueues = [queue];

        var queues = xucQueue.getQueuesExcept(excludeQueues);

        expect(queues.length).toBe(2);
    });
    it('should return a list of queues by ids', function() {
        var q1 = new QueueBuilder(101,'Sky').build();
        var q2 = new QueueBuilder(102,'Earth').build();
        var q3 = new QueueBuilder(103,'Sea').build();

        var queueIds = [101,103];

        var queues = xucQueue.getQueueByIds(queueIds);

        expect(queues.length).toBe(2);
        expect(queues).toContain(q1);
        expect(queues).toContain(q3);
    });
});

