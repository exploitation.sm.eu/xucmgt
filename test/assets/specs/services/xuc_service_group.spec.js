'use strict';
describe('Xuc service group', function() {
    var xucGroup;
    var xucAgent;
    var $rootScope;

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });

    beforeEach(module('xuc.services'));

    beforeEach(inject(function(_XucGroup_,_XucAgent_) {
        xucGroup = _XucGroup_;
        xucAgent = _XucAgent_;
    }));

    it('should have empty groups at init', function() {

        expect(xucGroup.getGroups()).toEqualData([]);
    });

    it('should init agent groups on agent group list received', function() {
        var agentGroupList = [{'id':8, 'name' : 'one'},{'id':17, 'name' : 'two'}];
        xucGroup.onAgentGroupList(agentGroupList);

        expect(xucGroup.getGroups().length).toBe(2);
    });

    it('should request groups from server on start', function() {
        spyOn(Cti,'getList');

        xucGroup.start();

        expect(Cti.getList).toHaveBeenCalledWith("agentgroup");

    });

    it('should send back an agent group on id', function(){
        var agentGroupList = [{'id':8, 'name' : 'one'},{'id':17, 'name' : 'two'}];
        xucGroup.onAgentGroupList(agentGroupList);

        expect(xucGroup.getGroup(17)).toEqualData({'id':17, 'name' : 'two'});

    });

    it('should send request move group of agents to xuc server', function() {
        spyOn(Cti,'moveAgentsInGroup');
        xucGroup.moveAgentsInGroup(54,523,4,895,8);

        expect(Cti.moveAgentsInGroup).toHaveBeenCalledWith(54,523,4,895,8);
    });
    it('should send request add group of agents to xuc server', function() {
        spyOn(Cti,'addAgentsInGroup');
        xucGroup.addAgentsInGroup(44,423,3,795,7);

        expect(Cti.addAgentsInGroup).toHaveBeenCalledWith(44,423,3,795,7);
    });

    it('should send request remove agents from queue group to xuc server', function(){
        var groupId=27, queueId = 234, penalty = 1;
        spyOn(Cti,'removeAgentGroupFromQueueGroup');

        xucGroup.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);

        expect(Cti.removeAgentGroupFromQueueGroup).toHaveBeenCalledWith(groupId, queueId, penalty);
    });

    it('should send request add agent not in queue from group to queue penalty to xuc server', function(){
        var groupId=98, queueId = 543, penalty = 8;
        spyOn(Cti,'addAgentsNotInQueueFromGroupTo');

        xucGroup.addAgentsNotInQueueFromGroupTo(groupId, queueId, penalty);

        expect(Cti.addAgentsNotInQueueFromGroupTo).toHaveBeenCalledWith(groupId, queueId, penalty);
    });
    it('should be able to get groups with agents available to be added in a queue penalty', function() {
        var agentGroupList = [{'id':8, 'name' : 'one'},{'id':7, 'name' : 'two'},{'id':6, 'name' : 'three'}];
        xucGroup.onAgentGroupList(agentGroupList);
        var queueId = 50, penalty=2;

        var noemie = new MockAgentBuilder(7,'Noemie','Ange').inGroup(8).build();
        var jean = new MockAgentBuilder(9,'Jean','Bonne').inGroup(7).build();
        var kem = new MockAgentBuilder(9,'Kem','Chown').inGroup(7).build();

        spyOn(xucAgent,'getAgentsNotInQueue').and.returnValue([jean,noemie,kem])

        var group = xucGroup.getAvailableGroups(queueId, penalty)

        expect(group).toContain({'id':8, 'name' : 'one'});//,{'id':7, 'name' : 'two'}]);
        expect(group).toContain({'id':7, 'name' : 'two'});
        expect(group).not.toContain({'id':6, 'name' : 'three'});
        expect(group.length).toBe(2);
    });
    it('return one empty list of groups for penalty 0 if no agents for a queue ', function(){
        var agents = [];
        var queueId = 895;

        spyOn(xucAgent,'getAgentsInQueue').and.returnValue([]);

        var expectedGroups = [{"penalty":0,"groups":[]}];
        var groups =  xucGroup.getGroupsForAQueue(queueId);

        expect(groups).toEqualData(expectedGroups);
    });
    it('do not process agents not in a group ', function(){
        var agents = [];
        var queueId = 895;

        var agentNotInGroup = new MockAgentBuilder(74,'Kim','Gunard').inQueue(queueId,0).build();
        spyOn(xucAgent,'getAgentsInQueue').and.returnValue([agentNotInGroup]);

        var expectedGroups = [{"penalty":0,"groups":[]}];
        var groups =  xucGroup.getGroupsForAQueue(queueId);

        expect(groups).toEqualData(expectedGroups);
    });

    it('return groups for penalty 0 and an empty group for penalty 1', function(){
        var queueId = 3;
        var groupId = 78;
        var agent1 = new MockAgentBuilder(74,'Kim','Gunard').inQueue(queueId,0).inGroup(groupId).build();
        var agents = [agent1];
        spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);

        var group = new MockGroupBuilder(groupId,'blue').build();
        xucGroup.onAgentGroupList([group]);

        var vgroup = new MockGroupBuilder(groupId,'blue').build();
        vgroup.nbOfAgents=1;
        vgroup.agents=agents;
        var expectedGroups = [{"penalty":0,"groups":[vgroup]},
                              {"penalty":1,"groups":[]}
                             ];
        var groups =  xucGroup.getGroupsForAQueue(queueId);

        expect(groups).toEqualData(expectedGroups);
        expect(group.nbOfAgents).toBeUndefined();
    });
    it('return empty group if agent not in queue', function(){
        var queueId = 3;
        var groupId = 9;
        var agent1 = new MockAgentBuilder(74,'Kim','Gunard').inGroup(groupId).build();
        var agents = [agent1];
        spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);
        var group = new MockGroupBuilder(groupId,'blue');
        xucGroup.onAgentGroupList([group]);

        var expectedGroups = [{"penalty":0,"groups":[]}];

        var groups =  xucGroup.getGroupsForAQueue(queueId);

        expect(groups).toEqualData(expectedGroups);
    });
    it('return only one group when agents are in the same group', function(){
        var queueId = 7;
        var groupId = 54;
        var agent1 = new MockAgentBuilder(86,'Louis','Hantou').inQueue(queueId,0).inGroup(groupId).build();
        var agent2 = new MockAgentBuilder(91,'Mike','Birch').inQueue(queueId,0).inGroup(groupId).build();
        var agents = [agent1, agent2];
        spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);
        var group = new MockGroupBuilder(groupId,'blue').build();
        xucGroup.onAgentGroupList([group]);

        var vGroup = angular.copy(group);
        vGroup.nbOfAgents = 2;
        vGroup.agents = agents;

        var expectedGroups = [{"penalty":0,"groups":[vGroup]},{"penalty":1,"groups":[]}];

        var groups =  xucGroup.getGroupsForAQueue(queueId);

        expect(groups).toEqualData(expectedGroups);
    });

    it('should return empty group when penalty is missing', function() {
        var queueId = 1
        var group1Id = 67;
        var group2Id = 71;
        var group1 = new MockGroupBuilder(group1Id,'red').build();
        var group2 = new MockGroupBuilder(group2Id,'yellow').build();
        var agent1 = new MockAgentBuilder(102,'Noe','Carlos').inQueue(queueId,0).inGroup(group1Id).build();
        var agent2 = new MockAgentBuilder(41,'Oscar','Dante').inQueue(queueId,3).inGroup(group2Id).build();
        var agents = [agent1, agent2];
        spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);
        var groups = [group1, group2];
        xucGroup.onAgentGroupList(groups);
        var getGroup = function() {
            return groups.splice(0,1)[0];
        };
        spyOn(xucGroup, 'getGroup').and.callFake(getGroup);

        var vGroup1 = angular.copy(group1);
        vGroup1.nbOfAgents = 1;
        vGroup1.agents = [agent1];
        var vGroup2 = angular.copy(group2);
        vGroup2.nbOfAgents = 1;
        vGroup2.agents = [agent2];
        var expectedGroupsArrayForQueue = [{"penalty":0,"groups":[vGroup1]},
                                                        {"penalty":1,"groups":[]},
                                                        {"penalty":2,"groups":[]},
                                                        {"penalty":3,"groups":[vGroup2]},
                                                        {"penalty":4,"groups":[]}
                                                     ];

        var groups =  xucGroup.getGroupsForAQueue(queueId);

        expect(groups).toEqualData(expectedGroupsArrayForQueue);
    });

    it ('max penalty is 20 no more group returned if max agent penalty is 20', function(){
        var queueId = 1
        var groupId = 54;
        var agent1 = new MockAgentBuilder(66,'Joe','Foxtrot').inQueue(queueId,20).build();
        var agents = [agent1];
        spyOn(xucAgent,'getAgentsInQueue').and.returnValue(agents);

        var group = new MockGroupBuilder(groupId,'blue');
        xucGroup.onAgentGroupList([group]);

        var groups =  xucGroup.getGroupsForAQueue(queueId);

        expect(groups[21]).toBeUndefined();
    });

});