'use strict';

describe('Service: agent\'s preferences', function () {
    var preferences;
    var localStorageService;
    var $rootScope;

    beforeEach(module('Agent'));

    beforeEach(inject(function(_AgentPreferences_,_localStorageService_,_$rootScope_) {
        preferences = _AgentPreferences_;
        localStorageService = _localStorageService_;
        $rootScope = _$rootScope_;
        spyOn(localStorageService, 'set');
        spyOn($rootScope, '$broadcast').and.callThrough();
    }));

    var init = function(queues) {
        spyOn(localStorageService, 'get').and.returnValue(queues);
        preferences._init();
    };

    it('loads and returns favorite queues', function() {
        var queues = [4, 2];
        init(queues);
        result = preferences.getFavoriteQueues();

        expect(localStorageService.get).toHaveBeenCalledWith('favoriteQueues');
        expect(result).toEqual(queues);
    });

    it('saves favoriteQueues', function() {
        init([]);
        var queues = [3, 5];
        preferences.setFavoriteQueues(queues);
        expect(localStorageService.set).toHaveBeenCalledWith('favoriteQueues', queues);
    });

    it('adds queue to favorites', function() {
        init([3, 5]);
        preferences.addFavoriteQueue(4);
        expect(localStorageService.set).toHaveBeenCalledWith('favoriteQueues', [3,5,4]);
    });

    it('should not double queues in favorites', function() {
        init([3, 5]);
        preferences.addFavoriteQueue(3);
        expect(localStorageService.set.calls.any()).toEqual(false);
    });

    it('removes queue from favorites', function() {
        init([1, 3, 5]);
        preferences.removeFavoriteQueue(3);
        expect(localStorageService.set).toHaveBeenCalledWith('favoriteQueues', [1, 5]);
    });

    it('should not fail when removing unknown queue from favorites', function() {
        init([3]);
        preferences.removeFavoriteQueue(5);
        expect(localStorageService.set.calls.any()).toEqual(false);
    });

    it('should check favorite queues', function() {
        init([3, 5]);
        expect(preferences.isFavoriteQueue(3)).toEqual(true);
        expect(preferences.isFavoriteQueue(1)).toEqual(false);
    });

    it('should broadcast preferences.favoriteQueues event on favoriteQueues changed', function() {
        $rootScope.$broadcast('LocalStorageModule.notification.setitem',{key: 'favoriteQueues', newvalue: [4]});
        expect($rootScope.$broadcast.calls.mostRecent().args).toEqual(['preferences.set.favoriteQueues', [4]]);
    });

});
