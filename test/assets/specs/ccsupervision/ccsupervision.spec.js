'use strict';

describe('CCSupervision Queues Controllers', function() {
    var $scope, ctrl;
    var xucQueue;
    var $rootScope;
    var cookieStore;

    beforeEach(module('xuc.services'));
    beforeEach(module('ccSupervision'));

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });

    beforeEach(inject(function(_XucQueue_,$rootScope, $controller, $cookieStore) {
        $scope = $rootScope.$new();
        xucQueue = _XucQueue_;
        rootScope = $rootScope;
        cookieStore = $cookieStore;
        ctrl = $controller('ccQueuesController', {
            'XucQueue' : xucQueue,
            '$scope' : $scope,
            '$rootScope' : rootScope,
            '$cookieStore' :$cookieStore
        });
    }));

    it('should have empty queues at initialisation ', function() {
        expect($scope.queues).toEqualData([]);
    });

    it('should be able start queue service on cti logged on ', function() {
        spyOn(xucQueue, 'start');
        rootScope.$broadcast('ctiLoggedOn');
        expect(xucQueue.start).toHaveBeenCalled();
     });

    it ('should keep empty filter queues if cookie is not set', function(){
        spyOn(cookieStore, 'get').and.callFake(function(cookiename) {return });

        $scope.readCookie();

        expect($scope.filterQueues).toEqualData([]);
    });
});

describe('CCSupervision Agents Controllers', function() {

    var $rootScope, $scope, ctrl, xucAgent;

    beforeEach(module('ccSupervision'));

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });

        noAgent = { 'state' : 'NoAgent', 'name' : '-', 'phoneNb' : '-', 'timeInState' : '-', 'status' : '-' }
        agentUn = {'id' : '1','state' : 'AgentReady','name' : 'Sartre Jean-Paul','lastName' : 'Sartre','firstName' : 'Jean-Paul', 'displayed' :true, 'queues' : [1,2]};
        agentDeux = {'id' : '2','state' : 'AgentReady','name' : 'Locke John','lastName' : 'Locke','firstName' : 'John', 'displayed' :true, 'queues' : [2]};
        agentTrois = {'id' : '3','state' : 'AgentReady','name' : 'Kant Emmanuel','lastName' : 'zammy','firstName' : 'Bob', 'displayed' :true, 'queues' : [2,3]};
        agentQuatre = {'id' : '4','state' : 'AgentReady','name' : 'Descartes René','lastName' : 'Descartes','firstName' : 'René', 'displayed' :true};
        agentCinq = {'id' : '5','state' : 'AgentReady','name' : 'Freud Sigmund','lastName' : 'Freud','firstName' : 'Sigmund', 'displayed' :true};
        agentSix = {'id' : '6','state' : 'AgentReady','name' : 'Rousseau Jean-Jacques','lastName' : 'Rousseau','firstName' : 'Jean-Jaques', 'displayed' :true};
        agentSept = {'id' : '7','state' : 'AgentReady','name' : 'Stote Harry','lastName' : 'Stote','firstName' : 'Harry', 'displayed' :true};
        agentHuit = {'id' : '8','state' : 'AgentReady','name' : 'Schopenhauer Arthur','lastName' : 'Schopenhauer','firstName' : 'Arthhur', 'displayed' :true};
        agentNeuf = {'id' : '9','state' : 'AgentReady','name' : 'Pascal Blaise','lastName' : 'Pascal','firstName' : 'Blaise', 'displayed' :true};
        agentDix = {'id' : '10','state' : 'AgentReady','name' : 'Nietzsche Friedrich','lastName' : 'Nietzsche','firstName' : 'Friedrich', 'displayed' :true};


    });

    beforeEach(inject(function(_$rootScope_, $controller, $filter, _XucAgent_) {
        $rootScope = _$rootScope_
        $scope = $rootScope.$new();
        xucAgent = _XucAgent_;
        ctrl = $controller('ccAgentsController', {
            '$scope' : $scope,
            $filter : $filter,
            'XucAgent' : xucAgent
        });
    }));

   it('should have no agents at initialisation ', function() {
        expect($scope.agents).toEqualData([]);
   });

    it('should request agents from xuc agent on agent loaded and dispatch agents', function(){
        var agents = ['ag1...','ag2...','ag3...'];
        spyOn(xucAgent, 'getAgents').and.callFake(function(queueId) {return agents;});
        spyOn($scope,'dispatchAgents');

        $rootScope.$broadcast('AgentsLoaded');

        expect($scope.agents).toEqualData(agents);
        expect($scope.dispatchAgents).toHaveBeenCalled();
    });
    it('should dispatch agents on queue member updated', function(){
        spyOn($scope,'dispatchAgents');

        $rootScope.$broadcast('QueueMemberUpdated');

        expect($scope.dispatchAgents).toHaveBeenCalled();
    });
    it('should dispatch agents on agent state updated', function(){
        spyOn($scope,'dispatchAgents');

        $rootScope.$broadcast('AgentStateUpdated');

        expect($scope.dispatchAgents).toHaveBeenCalled();
    });

    it('should not display logged out agents ',function(){
       $scope.agents = [{
           'id' : '1',
           'state' : 'AgentLoggedOut',
           'lastName' : 'Ltn',
           'firstName' : 'Charles',
           'status' : '-'
       },{
           'id' : '2',
           'state' : 'AgentReady',
           'lastName' : 'Willy',
           'firstName' : 'John',
           'status' : '-'
       }];

       $scope.dispatchAgents();

       expect($scope.agentsArray).toEqualData([[{
            'id' : '2',
            'state' : 'AgentReady',
            'name' : 'WILLY John',
            'lastName' : 'Willy',
            'firstName' : 'John',
            'status' : '-',
        }], [noAgent], [noAgent]])
    });

    it ('should order agents by name',function() {
        var willyJohn = {'id' : '1','state' : 'AgentReady','name' : 'Willy John','lastName' : 'Willy','firstName' : 'John'};
        var ammyBob = {'id' : '2','state' : 'AgentReady','name' : 'ammy Bob','lastName' : 'ammy','firstName' : 'Bob'};

        $scope.agents = [willyJohn,ammyBob];
        $scope.cookieOrder = {'name' : 'cookieOrderAgents', 'value' : 'name'}
        $scope.createCookie($scope.cookieOrder)
        $scope.orderAgents();

        expect($scope.agents).toEqualData([ammyBob, willyJohn])
    });

    it ('should order agents by state',function() {
        var willyJohn = {'id' : '1','state' : 'AgentReady','name' : 'Willy John','lastName' : 'Willy','firstName' : 'John'};
        var zammyBob = {'id' : '2','state' : 'AgentPaused','name' : 'zammy Bob','lastName' : 'zammy','firstName' : 'Bob'};

        $scope.agents = [willyJohn,zammyBob];
        $scope.cookieOrder = {'name' : 'cookieOrderAgents', 'value' : 'state'}
        $scope.createCookie($scope.cookieOrder)
        $scope.orderAgents();

        expect($scope.agents).toEqualData([zammyBob, willyJohn])
    });
    it ('should order agents by state in reverse order',function() {
        var willyJohn = {'id' : '1','state' : 'AgentPaused','name' : 'Willy John','lastName' : 'Willy','firstName' : 'John'};
        var zammyBob = {'id' : '2','state' : 'AgentReady','name' : 'zammy Bob','lastName' : 'zammy','firstName' : 'Bob'};

        $scope.agents = [willyJohn,zammyBob];
        $scope.cookieOrder = {'name' : 'cookieOrderAgents', 'value' : '-state'}
        $scope.createCookie($scope.cookieOrder)
        $scope.orderAgents();


        expect($scope.agents).toEqualData([zammyBob, willyJohn])
    });


     it ('should order agents by status',function() {
        var willyJohn = {'id' : '1','state' : 'AgentReady','name' : 'Willy John','lastName' : 'Willy','firstName' : 'John', 'status' : 'partiManger'};
        var zammyBob = {'id' : '2','state' : 'AgentPaused','name' : 'zammy Bob','lastName' : 'zammy','firstName' : 'Bob', 'status' : 'disponible'};

        $scope.agents = [willyJohn,zammyBob];
        $scope.cookieOrder = {'name' : 'cookieOrderAgents', 'value' : 'status'}
        $scope.createCookie($scope.cookieOrder)
        $scope.orderAgents();

        expect($scope.agents).toEqualData([zammyBob, willyJohn])
    });

    it('should limit length of agent display names',function(){
        var ben = {'id' : '2','state' : 'AgentReady','lastName' : 'Franklin','firstName' : 'Benjamin', 'displayed' : true};
        $scope.nameMaxLen = 10;
        $scope.agents = [ben];
        $scope.dispatchAgents();

        expect($scope.agentsArray[0][0].name).toBe("FRANKLIN B");

    });

    it('should order 8 agents by name and in 3 columns', function() {

        $scope.agents = [agentUn, agentDeux, agentTrois, agentQuatre, agentCinq, agentSix, agentSept, agentHuit]
        $scope.dispatchAgents();
         for(i=0;i < $scope.agentsArray.length;i++)
                    for(j=0;j<$scope.agentsArray[i].length;j++)
                        if($scope.agentsArray[i][j].state == "NoAgent")
                            $scope.agentsArray[i].splice(j,$scope.agentsArray[i].length);
        expect($scope.agentsArray.length).toBe(3)
        expect($scope.agentsArray[0].length).toBe(3)
        expect($scope.agentsArray[1].length).toBe(3)
        expect($scope.agentsArray[2].length).toBe(2)
    });


    it('should order 10 agents by name and in 4 columns', function() {

        $scope.agents = [agentUn, agentDeux, agentTrois, agentQuatre, agentCinq, agentSix, agentSept, agentHuit, agentNeuf, agentDix]
        $scope.dispatchAgents();

        for(i=0;i < $scope.agentsArray.length;i++)
            for(j=0;j<$scope.agentsArray[i].length;j++)
                if($scope.agentsArray[i][j].state == "NoAgent")
                    $scope.agentsArray[i].splice(j,$scope.agentsArray[i].length);
        expect($scope.agentsArray.length).toBe(4)
        expect($scope.agentsArray[0].length).toBe(3)
        expect($scope.agentsArray[1].length).toBe(3)
        expect($scope.agentsArray[2].length).toBe(3)
        expect($scope.agentsArray[3].length).toBe(1)

    });

    it('should order 6 agents by name and in only 3 columns', function() {

            $scope.agents = [agentUn, agentDeux, agentTrois, agentQuatre, agentCinq, agentSix]
            $scope.dispatchAgents();

            for(i=0;i < $scope.agentsArray.length;i++)
                for(j=0;j<$scope.agentsArray[i].length;j++)
                    if($scope.agentsArray[i][j].state == "NoAgent")
                        $scope.agentsArray[i].splice(j,$scope.agentsArray[i].length);

            expect($scope.agentsArray.length).toBe(3)
            expect($scope.agentsArray[0].length).toBe(2)
            expect($scope.agentsArray[1].length).toBe(2)
            expect($scope.agentsArray[2].length).toBe(2)
        });


    it('should filter queues on demand', function() {
            $scope.agents = [agentUn, agentDeux, agentTrois];
            $scope.$broadcast('filterQueues', [1,3]);
    });


});
