describe("Directory display prepare display", function() {
    it("display field as a link if it is a number", function () {
        var fieldBeDisplayed = DirectoryDisplay.formatField("12345");

        expect(fieldBeDisplayed).toEqual('<a style="cursor:pointer">12345</a>');
    });

    it("display normal field if it is not a number", function () {
        var fieldBeDisplayed = DirectoryDisplay.formatField("John");

        expect(fieldBeDisplayed).toEqual('John');
    });
});
