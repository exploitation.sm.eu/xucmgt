describe('qeueue select controllers', function() {
    var preferences;
    var xucQueue;
    var $scope;
    var ctrl;

    beforeEach(module('ccManager'));

    beforeEach(inject(function(_XucQueue_,_Preferences_, $controller, $rootScope) {
        $scope = $rootScope.$new();
        xucQueue = _XucQueue_;
        preferences = _Preferences_;
        ctrl = $controller('queueSelectCtrl', {
            '$scope' : $scope,
            'XucQueue' : xucQueue,
            'Preferences' : preferences
        });
    }));

    it('should select a queue', function(){
        spyOn(preferences, 'toggleQueueSelection');

        $scope.toggleQueue(28);

        expect(preferences.toggleQueueSelection).toHaveBeenCalledWith(28);
    });

    it('should unselect a queue', function(){
        spyOn(preferences, 'isQueueSelected');

        $scope.isSelected(54);

        expect(preferences.isQueueSelected).toHaveBeenCalledWith(54);
    });

});
