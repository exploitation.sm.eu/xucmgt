describe('agent dyn view controllers', function() {
    var preferences;
    var xucAgent;
    var $scope;
    var ctrl;
    var $filter;
    var ngTableParams;
    var $rootScope;
    var agentViewColBuilder;
    var xucAgentTotal;

    var updateFn = jasmine.createSpy('update');

    beforeEach(module('ccManager'));

    beforeEach(inject(function(_ngTableParams_, _$filter_, _XucAgent_,_Preferences_, $controller, _$rootScope_, _AgentViewColBuilder_,_XucAgentTotal_, _CCMUtils_) {
        $scope = _$rootScope_.$new();
        $rootScope = _$rootScope_;
        xucAgent = _XucAgent_;
        preferences = _Preferences_;
        $filter = _$filter_;
        ngTableParams = _ngTableParams_;
        agentViewColBuilder = _AgentViewColBuilder_;
        xucAgentTotal = _XucAgentTotal_;
        ccmUtils = _CCMUtils_;

        var selectedColumns = [{id:'col1', show: false},{id:'col2', show: true}];
        spyOn(preferences, 'getViewColumns').and.returnValue(selectedColumns);
        spyOn(agentViewColBuilder, 'buildTable').and.callFake(function() {});
        spyOn(ccmUtils,'doDeffered').and.returnValue(updateFn);

        ctrl = $controller('ccAgentDynViewController', {
            '$scope' : $scope,
            'ngTableParams': ngTableParams,
            '$filter': $filter,
            'XucAgent' : xucAgent,
            'Preferences' : preferences,
            'AgentViewColBuilder': agentViewColBuilder,
            'XucAgentTotal' : xucAgentTotal,

        });
    }));


    it('should select agents from queue selected', function() {

         $rootScope.$broadcast('preferences.queueSelected');

         expect(updateFn).toHaveBeenCalled();

    });

    it('on agent loaded should select agent from queues', function() {

        $rootScope.$broadcast('AgentsLoaded');

        expect(updateFn).toHaveBeenCalled();

    });
    it('should reload cols on preferences saved', function(){

        $rootScope.$broadcast('preferences.agentViewColumns');

        expect(preferences.getViewColumns.calls.count()).toEqual(2);

    });

    it('should update totals on agent statistic updated', function(){

        $rootScope.$broadcast('AgentStatisticsUpdated');

        expect(updateFn).toHaveBeenCalled();

    });
/*
*/

});
