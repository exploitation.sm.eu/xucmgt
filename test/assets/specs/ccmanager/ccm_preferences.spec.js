'use strict';

describe('Service: ccmanager preferences', function () {
    var preferences;
    var localStorageService;
    var $rootScope;

    beforeEach(module('ccManager'));

    beforeEach(inject(function(_Preferences_,_localStorageService_,_$rootScope_) {
        preferences = _Preferences_;
        localStorageService = _localStorageService_;
        $rootScope = _$rootScope_;
        spyOn(localStorageService, 'set');
    }));

    it('should load queue selection on init', function(){
        var queueIds = [4,7,11];
        spyOn(localStorageService, 'get').and.returnValue(queueIds);

        preferences.init();

        expect(localStorageService.get).toHaveBeenCalledWith('queueSelected');
    });

    it('should be able to get empty selection', function(){
        var queueIds = [4,7,11];
        spyOn(localStorageService, 'get').and.returnValue(queueIds);

        expect(preferences.getSelectedQueues()).toEqual(queueIds);

        expect(localStorageService.get).toHaveBeenCalledWith('queueSelected');

    });

    it('should be able to remove a selected queue', function(){
        var queueIds = [8,11];
        loadQueueIds(queueIds);

        preferences.toggleQueueSelection(11);

        var queueIdsToSave = [8];
        expect(localStorageService.set).toHaveBeenCalledWith('queueSelected',queueIdsToSave);
    });

    it('should be able to add a selected queue', function(){
        var queueIds = [5,27];
        loadQueueIds(queueIds);

        preferences.toggleQueueSelection(21);

        var queueIdsToSave = [5,27,21];
        expect(localStorageService.set).toHaveBeenCalledWith('queueSelected',queueIdsToSave);
    });


    it('should be able to check if a queue is selected', function() {
        var queueIds = [54,270];
        loadQueueIds(queueIds);

        expect(preferences.isQueueSelected(54)).toBe(true);
    });

    it('should check if any queue is selected', function() {
        loadQueueIds([]);
        expect(preferences.noQueueSelected()).toBe(true);
    });

    it('should broadcast preferences.queueselected event on localstore changed', function() {
        $rootScope.$broadcast('LocalStorageModule.notification.setitem',{key:'queueSelected'});
    });

    var loadQueueIds = function(queueIds) {
        spyOn(localStorageService, 'get').and.returnValue(queueIds);
        preferences.init();
    }


    it('on load colums if not saved should return available columns', function() {
        spyOn(localStorageService, 'get').and.returnValue(null);
        var availCols = [{id:'one'}];
        var key = 'saveKey';

        var savedCols = preferences.getViewColumns(availCols,key);

        expect(localStorageService.get).toHaveBeenCalledWith(key);
        expect(savedCols).toEqual(availCols);
    });

    it('on load columns set show on available columns from saved', function() {
        var availCols = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'}];
        var savedCols = [{id:'one',show:true},{id:'two', show: false}];
        var key = 'saveKey';

        var loadedCols = [{id:'one',show:true, title:'col1'},{id:'two', show: false, title:'col2'}];

        spyOn(localStorageService, 'get').and.returnValue(savedCols);

        var prefcols = preferences.getViewColumns(availCols,key);

        expect(prefcols).toEqual(loadedCols);

    });

    it('on load columns keep order from saved columns and keep initial colums', function() {
        var availCols = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'},{id:'three', show: true, title:'col3'}];
        var savedCols = [{id:'two',show:true, title:'col2'},{id:'one', show: true, title:'col1'}];
        var key = 'saveKey';

        var loadedCols = [{id:'two',show:true, title:'col2'},{id:'one', show: true, title:'col1'},{id:'three', show: true, title:'col3'}];

        spyOn(localStorageService, 'get').and.returnValue(savedCols);

        var prefcols = preferences.getViewColumns(availCols,key);

        expect(prefcols).toEqual(loadedCols);
        expect(availCols).toEqual([{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'},{id:'three', show: true, title:'col3'}]);

    });
    it('saved columns preferences', function() {
        var colToSave = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'}];
        var key = 'saveKey';

        preferences.saveViewColumns(colToSave, key);

        expect(localStorageService.set).toHaveBeenCalledWith(key,colToSave);

    });

    it('should broadcast preferences.agentViewColumns event on agent view cols changed', function() {
        $rootScope.$broadcast('LocalStorageModule.notification.setitem',{key:'agentViewColumns'});
    });

    it('should move columns from right to left and save configuration', function(){
        var tableCols = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'},{id:'three', show: true, title:'col3'}];
        var colToSave = [{id:'one',show:true, title:'col1'},{id:'three', show: true, title:'col3'},{id:'two', show: true, title:'col2'}];

        preferences.moveCol('three', 'two', tableCols, 'agentViewColumns');

        expect(localStorageService.set).toHaveBeenCalledWith('agentViewColumns',colToSave);

    });
    it('should move columns from left to right and save configuration', function(){
        var tableCols = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'},{id:'three', show: true, title:'col3'},{id:'four', show: true, title:'col3'}];
        var colToSave = [{id:'two',show:true, title:'col2'},{id:'three', show: true, title:'col3'},{id:'one', show: true, title:'col1'},{id:'four', show: true, title:'col3'}];

        preferences.moveCol('one', 'three', tableCols, 'agentViewColumns');

        expect(localStorageService.set).toHaveBeenCalledWith('agentViewColumns',colToSave);

    });
    it('should move first column and save configuration', function(){
        var tableCols = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'},{id:'three', show: true, title:'col3'}];
        var colToSave = [{id:'two',show:true, title:'col2'},{id:'three', show: true, title:'col3'},{id:'one', show: true, title:'col1'}];

        preferences.moveCol('one', 'three', tableCols, 'agentViewColumns');

        expect(localStorageService.set).toHaveBeenCalledWith('agentViewColumns',colToSave);

    });
    it('should move second column to first and save configuration', function(){
        var tableCols = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'},{id:'three', show: true, title:'col3'}];
        var colToSave = [{id:'two',show:true, title:'col2'},{id:'one', show: true, title:'col1'},{id:'three', show: true, title:'col3'}];

        preferences.moveCol('two', 'one', tableCols, 'agentViewColumns');

        expect(localStorageService.set).toHaveBeenCalledWith('agentViewColumns',colToSave);

    });

    it('should do nothing if target column does not exists', function() {
        var tableCols = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'},{id:'three', show: true, title:'col3'}];

        preferences.moveCol('three', 'four', tableCols,  'agentViewColumns');

        expect(localStorageService.set.calls.count()).toEqual(0);
    });
    it('should do nothing if source column does not exists', function() {
        var tableCols = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'},{id:'three', show: true, title:'col3'}];

        preferences.moveCol('zero', 'two', tableCols,  'agentViewColumns');

        expect(localStorageService.set.calls.count()).toEqual(0);
    });

});
