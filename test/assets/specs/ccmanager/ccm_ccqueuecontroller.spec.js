describe('ccm ccqueuecontroller', function() {
    var localStorageService;
    var xucQueue;
    var xucAgent;
    var xucGroup;
    var $rootScope;
    var $scope;
    var ctrl;
    var preferences;
    var ccmUtils;

    beforeEach(module('ccManager'));

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });
    var updateFn = jasmine.createSpy('update');

    beforeEach(inject(function(_XucQueue_,_XucAgent_,_XucGroup_,_localStorageService_, _Preferences_, $controller, _$rootScope_, _CCMUtils_) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        xucQueue = _XucQueue_;
        xucAgent = _XucAgent_;
        xucGroup = _XucGroup_;
        localStorageService = _localStorageService_;
        preferences = _Preferences_;
        ccmUtils = _CCMUtils_;
        spyOn(ccmUtils,'doDeffered').and.returnValue(updateFn);
        $attrs = {showcallbacks: 'false'};

        ctrl = $controller('ccQueuesController', {
            '$scope' : $scope,
            'XucQueue' : xucQueue,
            'XucAgent' : xucAgent,
            'XucGroup' : xucGroup,
            'localStorageService' : localStorageService,
            'Preferences': preferences,
            '$attrs': $attrs
        });
        spyOn(localStorageService, 'set');
    }));

    it('should get queues on queue loaded', function() {
        var qsales = QueueBuilder('sales','sales channel').build();
        var qsalesexpert = QueueBuilder('salesexpert','sales expert').build();
        var qsupport = QueueBuilder('support','support channel').build();

        spyOn(preferences, 'isQueueSelected').and.returnValue(true);
        spyOn(xucQueue,'getQueues').and.returnValue([qsales,qsalesexpert,qsupport]);

        $rootScope.$broadcast('QueuesLoaded');

        expect(xucQueue.getQueues).toHaveBeenCalled();
        expect($scope.queues).toEqual([qsales,qsalesexpert,qsupport]);
    });

    it('should reload queues on queue selected', function() {
        var qsales = QueueBuilder('sales','sales channel').build();

        spyOn(preferences, 'isQueueSelected').and.returnValue(true);
        spyOn(xucQueue,'getQueues').and.returnValue([qsales]);

        $rootScope.$broadcast('preferences.queueSelected');

        expect(updateFn).toHaveBeenCalled();

    });

    it('should build group member on queue member updated', function() {
        var agentsInQueue = ['ag1...','ag2...','ag3...'];

        spyOn(xucAgent, 'getAgentsInQueue').and.callFake(function(queueId) {return agentsInQueue;});
        spyOn($scope,'buildGroupMember');
        spyOn($scope,'buildGroupOfGroups');
        spyOn($scope,'$digest');
        $rootScope.$broadcast('QueueMemberUpdated', 32);

        expect(xucAgent.getAgentsInQueue).toHaveBeenCalledWith(32);
        expect($scope.buildGroupMember).toHaveBeenCalledWith(32,agentsInQueue);
        expect($scope.buildGroupOfGroups).toHaveBeenCalledWith(32,agentsInQueue);
        expect(updateFn).toHaveBeenCalled();

    });

    it('build one group members for penalty 0 if no agents for a queue ', function(){

        var agents = [];
        var queueId = 895;
        $scope.groupsArray = [];

        var expectedGroupsArrayForQueue = {"idQueue":895,"groups":[{"penalty":0,"agents":[]}]}

        $scope.buildGroupMember(queueId,agents);

        expect($scope.groupsArray[queueId]).toEqualData(expectedGroupsArrayForQueue);
    });

    it('build a group member for penalty + empty group member for penalty +1', function(){
        var queueId = 1
        var agent = new MockAgentBuilder(25,'Franc','Barns')
            .inQueue(queueId,0)
            .build();
        var agents = [agent];
        var expectedGroupsArrayForQueue = {"idQueue":queueId,
                                                     "groups":[
                                                        {"penalty":0,"agents":[agent]},
                                                        {"penalty":1,"agents":[]}
                                                     ]}
        $scope.buildGroupMember(queueId,agents);

        expect($scope.groupsArray[queueId]).toEqualData(expectedGroupsArrayForQueue);

    });
    it('should create empty group when penalty is missing', function() {
        var queueId = 1
        var agent1 = new MockAgentBuilder(32,'Gerald','Choung')
            .inQueue(queueId,0)
            .build();
        var agent2 = new MockAgentBuilder(41,'Hector','Douma')
            .inQueue(queueId,3)
            .build();
        var agents = [agent1, agent2];
        var expectedGroupsArrayForQueue = {"idQueue":queueId,
                                                     "groups":[
                                                        {"penalty":0,"agents":[agent1]},
                                                        {"penalty":1,"agents":[]},
                                                        {"penalty":2,"agents":[]},
                                                        {"penalty":3,"agents":[agent2]},
                                                        {"penalty":4,"agents":[]}
                                                     ]}
        $scope.buildGroupMember(queueId,agents);

        expect($scope.groupsArray[queueId]).toEqualData(expectedGroupsArrayForQueue);
    });
    it('should create a group for each penalty', function(){
        var queueId = 1
        var agent1 = new MockAgentBuilder(32,'Gerald','Choung')
            .inQueue(queueId,1)
            .build();
        var agent2 = new MockAgentBuilder(41,'Hector','Douma')
            .inQueue(queueId,0)
            .build();
        var agent3 = new MockAgentBuilder(57,'Innes','Emma')
            .inQueue(queueId,1)
            .build();
        var agents = [agent1, agent2, agent3];
        var expectedGroupsArrayForQueue = {"idQueue":queueId,
                                                     "groups":[
                                                        {"penalty":0,"agents":[agent2]},
                                                        {"penalty":1,"agents":[agent1,agent3]},
                                                        {"penalty":2,"agents":[]}
                                                     ]}
        $scope.buildGroupMember(queueId,agents);

        expect($scope.groupsArray[queueId]).toEqualData(expectedGroupsArrayForQueue);
    });
    it ('max penalty is 20 no more group created if agent penalty is 20', function(){
        var queueId = 1
        var agent1 = new MockAgentBuilder(66,'Joe','Foxtrot')
            .inQueue(queueId,20)
            .build();
        var agents = [agent1];

        $scope.buildGroupMember(queueId,agents);

        expect($scope.groupsArray[queueId].groups[21]).toBeUndefined();
    });

    it('build one empty group of groups for penalty 0 if no agents for a queue ', function(){
        var agents = [];
        var queueId = 895;

        var expectedGroups = {"idQueue":queueId,"groups":[
                                                {"penalty":0,"groups":[]}
                                                ]};
        $scope.buildGroupOfGroups(queueId,agents);

        expect($scope.groupGroups[queueId]).toEqualData(expectedGroups);
    });

    it('remove logged out agents if show all agent is false', function() {
        var agents = [];

        $scope.agentFilter.showAllAgents = false;

        var queueId = 1
        var agent1 = new MockAgentBuilder(32,'Gerald','Choung')
            .onState('AgentLoggedOut')
            .inQueue(queueId,1)
            .build();
        var agent2 = new MockAgentBuilder(41,'Hector','Douma')
            .inQueue(queueId,0)
            .build();
        var agent3 = new MockAgentBuilder(57,'Innes','Emma')
            .inQueue(queueId,1)
            .build();
        var agents = [agent1, agent2, agent3];
        var expectedGroupsArrayForQueue = {"idQueue":queueId,
                                                     "groups":[
                                                        {"penalty":0,"agents":[agent2]},
                                                        {"penalty":1,"agents":[agent3]},
                                                        {"penalty":2,"agents":[]}
                                                     ]}
        $scope.buildGroupMember(queueId,agents);

        expect($scope.groupsArray[queueId]).toEqualData(expectedGroupsArrayForQueue);

    });
    it('build a group of groups', function() {
        var groupId = 56;
        var queueId = 789;

        var group = new MockGroupBuilder(groupId,'blue').build();

        var queueGroups = [{"penalty":0,"groups":[group]},{"penalty":1,"groups":[]}];

        spyOn(xucGroup, 'getGroupsForAQueue').and.returnValue(queueGroups);

        var expectedGroups = {"idQueue":queueId,"groups":queueGroups};

        $scope.buildGroupOfGroups(queueId,[]);

        expect($scope.groupGroups[queueId]).toEqualData(expectedGroups);

    });

});
