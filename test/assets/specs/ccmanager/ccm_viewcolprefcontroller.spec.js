describe('agent view col pref controllers', function() {
    var preferences;
    var $scope;
    var ctrl;


    beforeEach(module('ccManager'));

    beforeEach(inject(function(_Preferences_, $controller, _$rootScope_) {
        $scope = _$rootScope_.$new();
        preferences = _Preferences_;
        ctrl = $controller('ccViewColPrefController', {
            '$scope' : $scope,
            'Preferences' : preferences
        });
    }));


    it('should save selected columns', function() {
        $scope.cols = [{id:'one',show:true, title:'col1'},{id:'two', show: true, title:'col2'}];
        $scope.selectedCols = [{id:'two', show: true, title:'col2'}];

        var colToSave = [{id:'one',show:false},{id:'two', show: true}];

        spyOn(preferences,'saveViewColumns');

        $scope.selectColChanged('agentViewColumns');

        expect(preferences.saveViewColumns).toHaveBeenCalledWith(colToSave,'agentViewColumns');

    });

});
