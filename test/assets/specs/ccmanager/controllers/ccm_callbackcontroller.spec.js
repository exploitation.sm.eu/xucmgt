describe('ccm cccallbackcontroller', function() {
    var xucCallback;
    var $rootScope;
    var $scope;
    var ctrl;

    beforeEach(module('ccManager'));

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });
    var updateFn = jasmine.createSpy('update');

    beforeEach(inject(function(_XucCallback_, _$rootScope_, $controller) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        xucCallback = _XucCallback_;

        ctrl = $controller('ccCallbackController', {
            '$scope' : $scope,
            'XucCallback' : xucCallback
        });
    }));

    it('should get callbacks on callbacks loaded', function() {
        var l1 = CallbackListBuilder('First List', 1, []).build();
        var l2 = CallbackListBuilder('Second List', 2, []).build();

        spyOn(xucCallback,'getCallbackLists').and.returnValue([l1, l2]);

        $rootScope.$broadcast('CallbacksLoaded');

        expect(xucCallback.getCallbackLists).toHaveBeenCalled();
        expect($scope.callbackLists).toEqual([l1, l2]);
    });
});