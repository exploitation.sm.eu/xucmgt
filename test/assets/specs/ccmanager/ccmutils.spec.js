'use strict';

describe('Service: ccmanager utils', function () {
    var $timeout;
    var ccmutils;
    var update = {
        f:function() {
        }
    };

    beforeEach(module('ccManager'));

    beforeEach(inject(function(_CCMUtils_,_$timeout_) {
        ccmutils = _CCMUtils_;
        $timeout = _$timeout_;
        spyOn(update,'f');
    }));

    it('should execute f immediatly on first call', function(){

        var df = ccmutils.doDeffered(update.f);

        df();

        expect(update.f).toHaveBeenCalled();
    });
    it('should defer f on second call', function(){

        var df = ccmutils.doDeffered(update.f);

        df();
        df();

        expect(update.f.calls.count()).toEqual(1);
    });
    if('should defer f on next calls execute on timeout', function(){
        var df = ccmutils.doDeffered(update.f);

        df();
        df();
        df();
        $timeout.flush();

        expect(update.f.calls.count()).toEqual(2);
    });
});
