describe('agent edit controller', function() {
    var $scope;
    var xucAgent;
    var xucQueue;
    var ctrl;
    var modal;

    beforeEach(function() {
        jasmine.addMatchers({
            toEqualData : customMatchers.toEqualData
        });
    });

    beforeEach(module('ccManager'));

    beforeEach(inject(function(_XucAgent_,_XucQueue_, $controller, $rootScope, $modal) {
        $scope = $rootScope.$new();
        xucAgent = _XucAgent_;
        xucQueue = _XucQueue_;
        modal = $modal;
        ctrl = $controller('agentEditController', {
            '$scope' : $scope,
            'XucAgent' : xucAgent,
            'XucQueue' : xucQueue,
            '$modal' : modal
        });
    }));


    it('can instanciate controller', function() {
    });

    it('loads agent on edit agent', function(){
        var agent = {};
        agent.id = 32;
        agent.firstName = 'john';
        agent.queueMembers = {};
        agent.queueMembers['1']= 7;
        var queues=[{'id':1}];
        spyOn(xucAgent, 'getAgent').and.callFake(function(agentId) {return agent;});
        spyOn($scope,'loadAgentQueues').and.callFake(function(queueMembers) {return queues;});


        $scope.editAgent(32);

        expect(xucAgent.getAgent).toHaveBeenCalledWith(32);
        expect($scope.loadAgentQueues).toHaveBeenCalledWith(agent.queueMembers);
        expect($scope.agent).toEqualData(agent);
        expect($scope.agent.queues).toEqualData(queues);

    });
    it('loads queues for an agent', function(){
        var queueMembers = [];
        queueMembers[1]= 7;
        var queue1= {'id':1,'displayName':'First Queue'};
        var queueMember = {};
        queueMember.id = queue1.id;
        queueMember.displayName = queue1.displayName;
        queueMember.penalty = 7;
        var allQueues=[queueMember];

        spyOn(xucQueue,'getQueue').and.callFake(function(queueId){return queue1;});

        var queues = $scope.loadAgentQueues(queueMembers);

        expect(xucQueue.getQueue).toHaveBeenCalledWith(1);
        expect(queues).toEqualData(allQueues);

    });
});
