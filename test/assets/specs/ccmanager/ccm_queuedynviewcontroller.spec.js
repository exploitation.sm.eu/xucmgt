describe('queue dyn view controllers', function() {
    var xucQueueTotal;
    var $scope;
    var ctrl;
    var $filter;
    var ngTableParams;
    var $rootScope;
    var viewColBuilder;
    var preferences;

    beforeEach(module('ccManager'));
/*
ccManagerApp.controller('ccQueueDynViewController',
    ['$scope','ngTableParams','$filter','XucQueueTotal','ViewColBuilder', 'Preferences',
*/
    beforeEach(inject(function(_ngTableParams_, _$filter_, _XucQueueTotal_, $controller, _$rootScope_, _ViewColBuilder_, _Preferences_) {
        $scope = _$rootScope_.$new();
        $rootScope = _$rootScope_;
        xucQueueTotal = _XucQueueTotal_;
        $filter = _$filter_;
        ngTableParams = _ngTableParams_;
        viewColBuilder = _ViewColBuilder_;
        preferences = _Preferences_;

        $scope.queues = [];

        var selectedColumns = [{id:'col1', show: false},{id:'col2', show: true}];
        spyOn(preferences, 'getViewColumns').and.returnValue(selectedColumns);

        ctrl = $controller('ccQueueDynViewController', {
            '$scope' : $scope,
            'ngTableParams': ngTableParams,
            '$filter': $filter,
            'xucQueueTotal': xucQueueTotal,
            'viewColBuilder': viewColBuilder,
            'preferences' : preferences,
        });
    }));



    it('should get queue columns to be displayed from preferences', function() {
        var availColumns = [{id:'col1', show: true},{id:'col2', show: true}];

        expect(preferences.getViewColumns).toHaveBeenCalled();

    });

    it('should reload cols on preferences saved', function(){


        $rootScope.$broadcast('preferences.queueViewColumns');


        expect(preferences.getViewColumns.calls.count()).toEqual(2);

    });

});
