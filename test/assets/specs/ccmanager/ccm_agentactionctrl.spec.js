describe('agent action controller', function() {
    var $scope;
    var xucAgent;
    var ctrl;

    beforeEach(module('ccManager'));

    beforeEach(inject(function(_XucAgent_, $controller, $rootScope) {
        $scope = $rootScope.$new();
        xucAgent = _XucAgent_;
        ctrl = $controller('agentActionController', {
            '$scope' : $scope,
            'XucAgent' : xucAgent
        });
    }));


    it('can instanciate controller', function() {
    });

    it('can check if agent is able to log in', function(){
        spyOn(xucAgent,'canLogIn').and.callFake(function(agentId){return true});
        var agentId = 45;

        expect($scope.canLogIn(45)).toBe(true);

        expect(xucAgent.canLogIn).toHaveBeenCalledWith(agentId);
    });

    it('can log in an agent', function(){
        spyOn(xucAgent,'login');

        $scope.login(88);

        expect(xucAgent.login).toHaveBeenCalledWith(88);
    });

    it('can check if agent is able to log out', function() {
        spyOn(xucAgent,'canLogOut').and.callFake(function(agentId){return true});

        expect($scope.canLogOut(67)).toBe(true);

        expect(xucAgent.canLogOut).toHaveBeenCalledWith(67);

    });
    it('can log out an agent', function() {
        spyOn(xucAgent,'logout');

        $scope.logout(66);

        expect(xucAgent.logout).toHaveBeenCalledWith(66);
    });
    it('can check if agent is able to pause', function(){
        spyOn(xucAgent,'canPause').and.callFake(function(agentId){return true});

        expect($scope.canPause(75)).toBe(true);

        expect(xucAgent.canPause).toHaveBeenCalledWith(75);
    });
    it('can pause an agent', function(){
        spyOn(xucAgent,'pause');

        $scope.pause(115);

        expect(xucAgent.pause).toHaveBeenCalledWith(115);

    });
    it('can check if an agent is able to unpause', function() {
        spyOn(xucAgent,'canUnPause').and.callFake(function(agentId){return true});

        expect($scope.canUnPause(123)).toBe(true);

        expect(xucAgent.canUnPause).toHaveBeenCalledWith(123);
    });
    it('can un pause an agent', function(){
        spyOn(xucAgent,'unpause');

        $scope.unpause(628);

        expect(xucAgent.unpause).toHaveBeenCalledWith(628);
    });
    it('can check if an agent can be listened', function() {
        spyOn(xucAgent,'canListen').and.returnValue(true);

        expect($scope.canListen(879)).toBe(true);

        expect(xucAgent.canListen).toHaveBeenCalledWith(879);
    });
    it('can listen to an agent', function(){
        spyOn(xucAgent,'listen');

        $scope.listen(852);

        expect(xucAgent.listen).toHaveBeenCalledWith(852);
    })

});
