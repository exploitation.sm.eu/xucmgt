package test

import org.specs2.mutable._

import play.api.test._

import play.i18n._
import play.api.i18n.Lang
import org.openqa.selenium.By._
import org.openqa.selenium.{WebDriver, WebElement}
import org.openqa.selenium.support.ui.{ExpectedCondition, WebDriverWait}


/**
 * add your integration spec here.
 * An integration test will fire up a whole play application in a real (or headless) browser
 */
class IntegrationSpec extends Specification {

}