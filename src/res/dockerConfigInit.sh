#!/bin/sh
mkdir -p /etc/docker/xucmgt/conf
cd /etc/docker/xucmgt

mkdir /var/log/xucmgt
chown daemon:daemon /var∕log/xucmgt

wget https://gitlab.com/xuc/xucmgt/raw/master/src/res/update_start.sh
chmod +x update_start.sh

wget https://gitlab.com/xuc/xucmgt/raw/master/conf/application.conf
mv application.conf conf/xucmgt.conf

wget https://gitlab.com/xuc/xucmgt/raw/master/conf/logger.xml
mv logger.xml conf/xucmgt_logger.xml

cat > docker-run.conf << EOF
-t
-d
--restart=always
--name=xucmgt
-p XXXX:9000
-v /etc/docker/xucmgt/conf:/conf/
-v /var/log/xucmgt:/var/log/xucmgt/
-v /etc/timezone:/etc/timezone:ro
-v /etc/localtime:/etc/localtime:ro
xivoxc/xucmgt
-Dconfig.file=/conf/xucmgt.conf
-Dlogger.file=/conf/xucmgt_logger.xml
-Dxuc.port=XXXX
-Dxuc.host=X.X.X.X
EOF

echo "XUCMgt docker config init done."
echo "Now you have to update the configuration in the /etc/docker/xucmgt/docker-run.conf and /etc/docker/xucmgt/conf/xucmgt.conf."
echo "Than you can start XUCMgt by running 'docker run \$(cat /etc/docker/xucmgt/docker-run.conf)'"