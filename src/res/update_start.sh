#!/bin/sh

docker pull xivo/xucmgt:latest;
docker stop xucmgt;
docker rm xucmgt;
docker run $(cat /etc/docker/xucmgt/docker-run.conf)