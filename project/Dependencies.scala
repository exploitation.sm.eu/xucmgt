import sbt._
import play.Play.autoImport._


object Version {
  val selenium = "2.31.0"
  val htmlcompressor = "1.4"
  val yuicompressor = "2.4.6"
  val closurecompiler = "r1043"
  val scalatestplay = "1.2.0"
}


object Library {
  val selenium        =  "org.seleniumhq.selenium"        %   "selenium-java"     % Version.selenium
  val htmlcompressor  =  "com.googlecode.htmlcompressor"  %   "htmlcompressor"    % Version.htmlcompressor
  val yuicompressor   =  "com.yahoo.platform.yui"         % "yuicompressor"       % Version.yuicompressor
  val scalatestplay      = "org.scalatestplus"            %% "play"               % Version.scalatestplay

}

object Dependencies {

  import Library._

  val scalaVersion = "2.11.6"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("theatr.us" at "http://repo.theatr.us")
  )

  val runDep = run(
    htmlcompressor,
    yuicompressor
  )

  val testDep = test(
    selenium,
    scalatestplay
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
