import com.joescii.SbtJasminePlugin._
import com.typesafe.sbt.SbtSite.site
import com.typesafe.sbt.packager.debian.DebianPlugin
import com.typesafe.sbt.packager.debian.DebianPlugin.autoImport._
import com.typesafe.sbt.packager.docker.DockerPlugin
import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport._
import com.typesafe.sbt.site.SphinxSupport._
import com.typesafe.sbt.packager.linux.LinuxPlugin.autoImport._
import com.typesafe.sbt.packager.linux.LinuxPackageMapping
import org.clapper.sbt.editsource.EditSourcePlugin
import sbt._
import sbt.Keys._
import com.typesafe.sbt.packager.archetypes.ServerLoader.SystemV
import org.clapper.sbt.editsource.EditSourcePlugin.autoImport._
import com.typesafe.sbt.SbtNativePackager.autoImport._


object ApplicationBuild extends Build with com.typesafe.sbt.packager.linux.LinuxMappingDSL {

  val appName = "xucmgt"
  val appVersion = "1.33.0"
  val appOrganisation = "xivo"

  val main = Project(appName, file("."))
    .enablePlugins(play.PlayScala)
    .enablePlugins(DebianPlugin)
    .enablePlugins(DockerPlugin)
    .settings(
      name := appName,
      version := appVersion,
      scalaVersion := Dependencies.scalaVersion,
      organization := appOrganisation,
      resolvers ++= Dependencies.resolutionRepos,
      libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
      publishArtifact in(Compile, packageDoc) := false,
      publishArtifact in packageDoc := false
    )
    .settings(Jasmine.settings: _*)
    .settings(CustomDebianSettings.settings: _*)
    .settings(CustomDebianSettings.debianMappings: _*)
    .settings(
      testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u","target/test-reports"),
      testOptions in Test += Tests.Setup(() => { System.setProperty("XUCMGT_VERSION", appVersion) })

    )
    .settings(
      Site.settings: _*
    )
    .settings(
      DockerSettings.settings: _*
    )
    .settings(
      Editsources.settings: _*
    )
    .settings(
      setVersionVarTask := { System.setProperty("XUCMGT_VERSION", appVersion) },
      edit in EditSource <<= (edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource),
      packageBin in Compile <<= (packageBin in Compile) dependsOn (edit in EditSource),
      run in Compile <<= (run in Compile) dependsOn setVersionVarTask
    )

  lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")

  object Site {
    val settings = site.settings ++ site.sphinxSupport() ++ Seq(
      enableOutput in generatePdf in Sphinx := true
    )
  }

  object Jasmine {
    val settings = jasmineSettings ++ Seq(
      appJsDir <+= baseDirectory / "app/assets/javascripts",
      appJsLibDir <+= baseDirectory / "public/javascripts/",
      jasmineTestDir <+= baseDirectory / "test/assets/",
      jasmineConfFile <+= baseDirectory / "test/assets/test.dependencies.js",
      (test in Test) <<= (test in Test) dependsOn (jasmine),
      jasmineEdition := 2
    )
  }

  object DockerSettings {
    val settings = Seq(
      maintainer in Docker := "Jean-Yves LEBLEU <jylebleu@avencall.com>",
      dockerBaseImage := "java:openjdk-8u66-jdk",
      dockerExposedPorts := Seq(9000),
      dockerExposedVolumes := Seq("/conf","/logs"),
      dockerRepository := Some("xivoxc"),
      dockerEntrypoint := Seq("bin/xucmgt_docker")
    )
  }

  object Editsources {
    val settings = Seq(
      flatten in EditSource := true,
      targetDirectory in EditSource <<= baseDirectory / "conf",
      variables in EditSource <+= version {version => ("SBT_EDIT_APP_VERSION", appVersion)},
      (sources in EditSource) <++= baseDirectory map { bd =>
        (bd / "src/res" * "xucmgt.version").get
      }
    )
  }

  object CustomDebianSettings {
    val settings = Seq(
      daemonUser in Linux := "xucmgt",
      serverLoading in Debian := SystemV,
      maintainer in Debian := "Jean-Yves LEBLEU <jylebleu@avencall.com>",
      packageSummary := "Xuc Applications",
      packageDescription := """supervision, agent application and samples for Xivo""",
      debianChangelog := Some(file("debian/changelog"))
    )

    val debianMappings = Seq (
      linuxPackageMappings in Debian <+= (baseDirectory) map { bd =>
      (packageMapping((bd / "logs") -> "/var/log/xucmgt/")
         withUser "xucmgt" withGroup "xucmgt" withPerms "0755")
      },

      linuxPackageMappings in Debian <+= (baseDirectory) map { bd =>
      (packageMapping((bd / "etc/init.d/xucmgt_init") -> "/etc/init.d/xucmgt")
         withUser "root" withGroup "root" withPerms "0755")
      },

      linuxPackageMappings in Debian  <+= (baseDirectory) map { bd =>
      (packageMapping((bd / "conf/application.conf") -> "/usr/share/xucmgt/conf/xucmgt.conf")
         withUser "xucmgt" withGroup "xucmgt" withPerms "0755")
      },

      linuxPackageMappings in Debian  <+= (baseDirectory) map { bd =>
      (packageMapping((bd / "conf/logger.xml") -> "/usr/share/xucmgt/conf/xucmgt_logger.xml")
         withUser "xucmgt" withGroup "xucmgt" withPerms "0755")
      },

      linuxPackageMappings <<= (linuxPackageMappings) map { mappings =>
        for(LinuxPackageMapping(filesAndNames, meta, zipped) <- mappings) yield {
          val newFilesAndNames = for {
            (file, installPath) <- filesAndNames
            if !installPath.contains("/application.conf")
            if !installPath.contains("/logger.xml")
            if !installPath.contains("/messages")
            if !installPath.contains("/messages.fr")
            if !installPath.contains("/play.plugins")
          } yield file -> installPath
          LinuxPackageMapping(newFilesAndNames, meta, zipped)
        }
      }

    )
  }
}

