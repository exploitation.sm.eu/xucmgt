.. _installation:

#######################
Xuc server installation
#######################

============
Prerequisite
============

Necessary Compoments
--------------------

Xuc server embed its own web server, and need to be installed on a wheezy debian.

* Install java::

   apt-get install openjdk-7-jre

XiVO Server Configuration
-------------------------

XiVO configuration
^^^^^^^^^^^^^^^^^^

* Create a telephone user with XiVO Client account, 
      login = xuc / password = 0000. See /usr/share/xuc/conf/xuc.conf xivocti section for changes in user credentials if needed.
      profile can be emtpy

* Create a Webservices user (Configuration/Accès aux services web) with all rights, login = xivows / password = xivows. 
      See /usr/share/xuc/conf/xuc.conf XivoWs section for changes in user credentials if needed.

* Postgresql
   - /etc/postgresql/9.1/main/postgresql.conf Add/Change "listen_addresses" to match your configuration
   - /etc/postgresql/9.1/main/pg_hba.conf Add/Change configuration to match your configuration

::

    ex: "host    asterisk             asterisk             127.0.0.1/32            md5"  to authorize connections on the same server

Apply new configuration::

   xivo-service restart all

Xuc Installation
----------------

* Install Xuc server

From the repository::

   apt-get install xuc

Or you may install generated package builded from the sources::

   dpkg -i xuc-<version>.deb

Xuc Configuration
-----------------
You have to restart xuc server after configuration modifications

Changing listening port number or memory size
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 - /etc/init.d/xuc
    Change "PORT" variable, check with netstat first if port is available
    Add JVM_MEM="-mem 64" below port definition
    Add $JVM_MEM in START_CMD variable, ex: "START_CMD="$JVM_MEM -Dconfig.file=..."

XiVO server
^^^^^^^^^^^

 - /usr/share/xuc/conf/xuc.conf
    Change "xivohost" variable

=================
Post installation
=================

Change logger configuration
 
 - /usr/share/xuc/conf/xuc_logger.xml
 
    Modify the pathnames to add "/" in front of "var/log/xuc/"

Enable automatic restart::

   update-rc.d xuc defaults
   
========
Starting
========

Xuc can be started as a service::

   service xuc [start | stop | restart]

=====
Check
=====

tail -f /var/log/xuc/xuc.log
