########################################
Xuc Xivo Unified Communication Framework
########################################

Xuc is an application suite developed by Avencall_ Group, based on several free existing components
including XiVO_, and our own developments to provide communication services api and application to businesses.
Xuc is build on Play_ using intensively Akka_ and written in Scala_

.. _Avencall: http://www.avencall.com/
.. _XiVO: http://documentation.xivo.io/
.. _Play: http://www.playframework.com/
.. _Akka: http://akka.io/
.. _Scala: http://www.scala-lang.org/


XiVO is `free software`_. Most of its distinctive components, and Xuc as a whole, are distributed
under the *LGPLv3 license*.

.. _free software: http://www.gnu.org/philosophy/free-sw.html

Xuc is providing

* Javascript API
* Web services
* Sample application
* Simple agent application
* Simple unified communication application pratix
* Contact center supervision
* Contact center statistics

Xuc is composed of 3 modules

* The server module
* The core module
* The statistic module.

Table of Contents
=================

.. toctree::
   :maxdepth: 2

   installation/installation
   developers/developers
   api/javascriptapi
   api/rest

Release Notes
=============

2.0.8
-----
* new cti commands setAgentQueue, removeAgentFromQueue, getList
* Added ccmanager application

2.0.4
-----
* Play upgraded to 2.3.3, Scala to 2.11.1

1.9.0
-----

* Removed javascript events : AgentLogin, AgentReady, AgentPaused replaced by agent state events.
* Removed javascript toggleLogin function
* Can supervise agents without having all users connected or started
